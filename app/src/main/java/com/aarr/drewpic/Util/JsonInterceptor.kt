package com.aarr.drewpic.Util

import android.util.Log
import com.aarr.drewpic.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody
import java.io.IOException


/**
 * Created by andresrodriguez on 2/28/18.
 */
class JsonInterceptor : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        val response = chain.proceed(request)
        val rawJson = response.body()?.string()

        Log.d(BuildConfig.APPLICATION_ID, String.format("raw JSON response is: %s", rawJson))

        // Re-create the response before returning it because body can be read only once
        return response.newBuilder()
                .body(ResponseBody.create(response.body()?.contentType(), rawJson)).build()
    }
}