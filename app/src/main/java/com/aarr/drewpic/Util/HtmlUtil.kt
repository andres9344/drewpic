package com.aarr.drewpic.Util

/**
 * Created by andresrodriguez on 4/2/18.
 */
class HtmlUtil{

    fun getHtmlString(text:String?,link:String,size:String):String{

        if (!text.isNullOrEmpty()){
            val finalText =
                    "<html>" +
                        "<head>" +
                                "<style>" +
                                    " a { color:black; text-decoration:none; } "+
                                    " a:hover { color:black; text-decoration:none; } "+
                                    " a:visited { color:black; text-decoration:none; } "+
                                    " #lblDescription { font-size:"+size+"px; }"+
                                    " #lblRead { border-radius: 25px; " +
                                        " border: 2px solid #60AFE5; " +
                                        " font-size:15px; " +
                                        " color:gray; " +
                                        " padding: 3px; " +
                                        " width: 200px; " +
                                        " height: 150px; } "+
                                "</style>"+
                        "</head>" +
                        "<body>" +
                                "<label id='lblDescription'>"+text+"</label></br></br>"+
                                "<a href='#'>" +
                                "<label id='lblRead'>" +
                                "<img src='globe2.png' width='15' height='15'>"+link+"</label>" +
                                "</a>"
                        "</body>" +
                    "</html>"

            return finalText
        }

        return ""
    }

}