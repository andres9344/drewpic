package com.aarr.drewpic.Util

import android.util.Log
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by andresrodriguez on 12/30/17.
 */
class DateManage{

    //        Sat, 24 Feb 2018 15:12:19 GMT
    fun parseDateFeedToMillis(datetime:String):Long{
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val gmt = SimpleDateFormat("EEE, d MMM yyyy kk:mm:ss zzz",Locale.US)
        gmt.timeZone = TimeZone.getTimeZone("GMT")
        try{
            val d = gmt.parse(datetime)
            Log.v("DateParse","Time: "+d.time)
            return d.time
        }catch (e:Exception){
            Log.e("DateParse",e.toString())
        }
        return -1
    }

    fun parseMillistoDateString(millis:Long): String {
        val date = Date(millis)
        val output = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val d = output.format(date)
        return d
    }

    fun parseMillistoTimeString(millis:Long): String {
        val myMillis = Date().time
        val timeMillis = myMillis - millis
        Log.v("TimeParse","MyMillis: "+myMillis+" - "+millis+" = "+timeMillis)
        val diff = TimeUnit.HOURS.convert(timeMillis, TimeUnit.MILLISECONDS).toInt()
        val date = Date(timeMillis)
        val output = SimpleDateFormat("HH")
        val diffString = diff.toString()
        try{
            val d = output.format(date)
            Log.e("TimeParse","Time: "+d)
            return d
        }catch (e:Exception){
            Log.e("TimeParse",e.toString())
        }
        return diffString
    }

    fun parseMillistoTimeInt(millis:Long): Int {
        val myMillis = Date().time
        val timeMillis = myMillis - millis
        Log.v("TimeParse","MyMillis: "+myMillis+" - "+millis+" = "+timeMillis)
        val diff = TimeUnit.HOURS.convert(timeMillis, TimeUnit.MILLISECONDS).toInt()
        return diff
    }

    fun getMillisFromServerTime(date:String): Long{
        val gmt = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
        try{
            val d = gmt.parse(date)
            Log.v("DateParse","Time: "+d.time)
            return d.time
        }catch (e:Exception){
            Log.e("DateParse",e.toString())
        }
        return -1
    }

    fun parseMillistoDate(millis:Long): Date = Date(millis)

}