package com.aarr.drewpic.Util

import java.util.*

/**
 * Created by andresrodriguez on 3/2/18.
 */
class RandomValues{
    var reached:Int=0
    var agree:Int=0
    var disagree:Int=0

    val min=15
    val max=50

    init {
        generateRandomReached()
        generateRandomAgree()
        generateRandomDisagree()
    }

    fun generateRandomReached():Int{
        reached = getRandom(min,max)
        return reached
    }

    fun generateRandomAgree():Int{
        agree = getRandom(min,max)
        if (agree>reached)
            agree -= reached
        return agree
    }
    fun generateRandomDisagree():Int{
        disagree = getRandom(min,max)
        return disagree
    }

    fun getRandom(from: Int, to: Int) : Int {
        return Random().nextInt(to - from) + from
    }
}