package com.aarr.drewpic.Notification

/**
 * Created by andresrodriguez on 9/14/17.
 */

import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.support.v4.content.WakefulBroadcastReceiver

class NotificationBroadcast : WakefulBroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {

        val componentName = ComponentName(context.packageName, NotificationIntent::class.java.name)
        WakefulBroadcastReceiver.startWakefulService(context, intent.setComponent(componentName))
        resultCode = Activity.RESULT_OK

    }
}