package com.aarr.drewpic.Notification

import android.app.AlarmManager
import android.app.IntentService
import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.media.RingtoneManager
import android.net.Uri
import android.os.SystemClock
import android.support.v4.app.NotificationCompat
import android.util.Log

import com.aarr.drewpic.R
import com.aarr.drewpic.Util.OrderIDGenerator
import com.aarr.drewpic.Views.SignupActivity
import java.util.Random

/**
 * Created by andresrodriguez on 9/14/17.
 */

class NotificationIntent : IntentService("NotificationIntent") {
    private val notificationTime = 172800 //48 Hours in seconds
    //    private val bubbleTime = 14400 //4 Hours in seconds
    private val bubbleTime = 6 //4 Hours in seconds

    override fun onHandleIntent(intent: Intent?) {
//        Log.v("Notification", "Notification intent: " + intent!!.getIntExtra(NOTIFICATION_ID, 0))
        if (intent!!.getIntExtra(NOTIFICATION_ID, 0) == 100) {
//            Log.v("Notification", "Bubble notification receive")
            val value = Random().nextInt(20 - 10 + 1) + 10
//            Log.v("Notification", "Bubble notification receive")
            setBadge(this, value)
            scheduleNotificationBubble(getNotification("Test Notification 2 Sec"), bubbleTime * 1000)
        } else {
//            Log.v("Notification", "Normal notification receive")
            val notificationManager = this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val builder = NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.icon_square)
                    .setContentTitle(this.resources.getString(R.string.app_name))
                    .setAutoCancel(true)
                    .setStyle(NotificationCompat.BigTextStyle().bigText("We miss you, come back!"))
                    .setContentText("We miss you, come back!")

            //        Notification notification = intent.getParcelableExtra(NOTIFICATION);
            //        int id = intent.getIntExtra(NOTIFICATION_ID, 0);
            //        notificationManager.notify(id, notification);

            val newIntent = Intent(this, SignupActivity::class.java)

            val pendingIntent = PendingIntent.getActivity(this, 0, newIntent, PendingIntent.FLAG_UPDATE_CURRENT)

            builder.setContentIntent(pendingIntent)
            val pattern = longArrayOf(500, 500, 500, 500, 500, 500, 500, 500, 500)
            builder.setVibrate(pattern)

            val alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            builder.setSound(alarmSound)
            NOTIF_ALERTA_ID = OrderIDGenerator().randomIDInt
            notificationManager.notify(NOTIF_ALERTA_ID, builder.build())

            scheduleNotification(getNotification("Test Notification 2 Sec"), notificationTime * 1000)
        }
    }

    private fun scheduleNotification(notification: Notification, delay: Int) {

        val notificationIntent = Intent(this, NotificationBroadcast::class.java)
        notificationIntent.putExtra(NOTIFICATION_ID, 1)
        notificationIntent.putExtra(NOTIFICATION, notification)
        val pendingIntent = PendingIntent.getBroadcast(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val futureInMillis = SystemClock.elapsedRealtime() + delay
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent)

        val editor = getSharedPreferences("NotificationCreated", Context.MODE_PRIVATE).edit()
        editor.putBoolean("isCreated", true)
        editor.commit()
    }

    private fun scheduleNotificationBubble(notification: Notification, delay: Int) {

        val notificationIntent = Intent(this, NotificationBroadcast::class.java)
        notificationIntent.putExtra(NOTIFICATION_ID, 100)
        notificationIntent.putExtra(NOTIFICATION, notification)
        val pendingIntent = PendingIntent.getBroadcast(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val futureInMillis = SystemClock.elapsedRealtime() + delay
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent)

        val editor = getSharedPreferences("NotificationBubbleCreated", Context.MODE_PRIVATE).edit()
        editor.putBoolean("isCreated", true)
        editor.commit()
    }

    private fun getNotification(content: String): Notification {
        val builder = Notification.Builder(this)
        builder.setContentTitle("Scheduled Notification")
        builder.setContentText(content)
        builder.setSmallIcon(R.mipmap.ic_launcher)
        return builder.build()
    }

    companion object {

        var NOTIFICATION_ID = "notification-id"
        var NOTIFICATION = "notification"
        private var NOTIF_ALERTA_ID = 1

        fun setBadge(context: Context, count: Int) {
            val launcherClassName = getLauncherClassName(context) ?: return
            val intent = Intent("android.intent.action.BADGE_COUNT_UPDATE")
            intent.putExtra("badge_count", count)
            intent.putExtra("badge_count_package_name", context.packageName)
            intent.putExtra("badge_count_class_name", launcherClassName)
            context.sendBroadcast(intent)
        }

        fun getLauncherClassName(context: Context): String? {

            val pm = context.packageManager

            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_LAUNCHER)

            val resolveInfos = pm.queryIntentActivities(intent, 0)
            for (resolveInfo in resolveInfos) {
                val pkgName = resolveInfo.activityInfo.applicationInfo.packageName
                if (pkgName.equals(context.packageName, ignoreCase = true)) {
                    val className = resolveInfo.activityInfo.name
                    return className
                }
            }
            return null
        }
    }
}
