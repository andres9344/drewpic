package com.aarr.drewpic.Views

import android.accounts.AccountManager
import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.os.SystemClock
import android.support.constraint.ConstraintLayout
import android.support.v4.app.ActivityCompat
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import android.telephony.TelephonyManager
import android.util.Log
import android.util.Patterns
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.*
import cn.pedant.SweetAlert.SweetAlertDialog
import com.aarr.drewpic.Database.Dao.CategoriesDao
import com.aarr.drewpic.Database.Dao.SettingsDao
import com.aarr.drewpic.Database.Dao.UserDao
import com.aarr.drewpic.Database.Dao.UserFriendsDao
import com.aarr.drewpic.Database.Helper.DatabaseHelper
import com.aarr.drewpic.Database.Model.*
import com.aarr.drewpic.Notification.NotificationBroadcast
import com.aarr.drewpic.Notification.NotificationIntent

import com.aarr.drewpic.R
import com.aarr.drewpic.Services.FloatingViewService
import com.aarr.drewpic.Services.GetAllCategoriesService
import com.aarr.drewpic.Services.RegisterUserCategories
import com.aarr.drewpic.Services.Response.Categories.CategoriesResponse
import com.aarr.drewpic.Services.Response.SignUp.SignUpResponse
import com.aarr.drewpic.Services.Response.UserFBLikes.UserLikesResponse
import com.aarr.drewpic.Services.Response.UserFriends.GetUserFriends
import com.aarr.drewpic.Services.SignUpService
import com.aarr.drewpic.Util.CountryUtil
import com.aarr.drewpic.Views.Fragments.CategoriesFragments
import com.aarr.drewpic.Views.Fragments.EmailFragment
import com.aarr.drewpic.Views.Fragments.WebFragments
import com.aigestudio.wheelpicker.WheelPicker
import com.daasuu.bl.BubbleLayout
import com.facebook.*
import com.facebook.appevents.AppEventsLogger
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.gson.Gson
import com.j256.ormlite.support.DatabaseConnection
import com.j256.ormlite.table.TableUtils
import org.json.JSONObject
import java.io.Serializable
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*
import kotlin.collections.ArrayList

class SignupActivity : AppCompatActivity() {

    var userData: SignUpResponse? = null
    var lstCategories: CategoriesResponse? = null
    var lstSelected: MutableList<SubCategoryModel> = ArrayList<SubCategoryModel>()
    var fragment: CategoriesFragments? = null
    var fragmentWeb: WebFragments? = null
    val FRAGMENT_SIGNUP = 1
    val FRAGMENT_CATEGORIES = 2
    val FRAGMENT_WEB = 3
    var currentFragment: Int? = -1
    var dialog: Dialog? = null
    var NOTIFICATION_ID = "notification-id"
    var NOTIFICATION_BUBBLE_ID = "100"
    var NOTIFICATION_BUBBLE = "bubble"
    var NOTIFICATION = "notification"
    private val notificationTime = 172800 //48 Hours in seconds
//    private val bubbleTime = 14400 //4 Hours in seconds
    private val bubbleTime = 6 //4 Hours in seconds
    var progressDialog: ProgressDialog? = null
    var mCallbackManager: CallbackManager? = null
    var showTutorial: Boolean = true
    var thumbUpUnicode: Int = 0x1F44D
    var thumbDownUnicode: Int = 0x1F44E
    var currentUser: User?=null
    var pDialog: SweetAlertDialog? = null
    var languages = arrayOf("English","Español","Italiano","Français","Deutsche","Português","Română")
    var selectedLanguage: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        getHashKey()
        initComponents()
        initFacebookClient()
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            accountsPermission()
        }else{
            scheduleNotifications()
        }
//        startService(Intent(this@SignupActivity, FloatingViewService::class.java))
    }

    fun getHashKey(){
        try {
            val info = getPackageManager().getPackageInfo(
                    "com.aarr.drewpic",
                    PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray());
                Log.d("KeyHash:", android.util.Base64.encodeToString(md.digest(), android.util.Base64.DEFAULT));
                }
        } catch (e: PackageManager.NameNotFoundException) {
            Log.e("KeyHash",e.toString())
        } catch (e: NoSuchAlgorithmException) {
            Log.e("KeyHash",e.toString())
        }
    }

    fun initComponents(){
        loadEmailFragment()

//        var accounts = AccountManager.get(this).getAccountsByType("com.google")
//        if (accounts!=null && accounts.isNotEmpty()) {
//            val emailPattern = Patterns.EMAIL_ADDRESS
//            for (item in accounts) {
//                if (emailPattern.matcher(item.name).matches()) {
//                    Log.v("Account", item.name)
//                }
//            }
//        }else{
//            Log.v("Account", "")
//        }
    }

    fun scheduleNotifications(){
        if (!isNotificationCreated()) {
            scheduleNotification(getNotification("Test Notification 2 Sec"), notificationTime * 1000)
        }
        if (!isNotificationBubbleCreated()){
            val value = Random().nextInt(30 - 10 + 1) + 10
//            Log.v("Notification", "Bubble notification receive")
            setBadge(this, value)
            scheduleNotificationBubble(getNotification("Test Notification 2 Sec"), bubbleTime * 1000)
        }else{
            setBadge(this,0)
        }
    }

    fun loadEmailFragment(){
        val fragment: android.support.v4.app.Fragment
        fragment = EmailFragment()

        currentFragment = FRAGMENT_SIGNUP

        val fragmentTransaction = supportFragmentManager!!.beginTransaction()

        showFirstTutorial()

//        if (getUserData()!=null){
//            if (getUserData()?.showTutorial!!){
//                showFirstTutorial()
//            }
////            val bundle = Bundle()
////            bundle.putBoolean("UserExist",true)
////            fragment.setArguments(bundle)
//        }else{
//
//        }

//        fragmentTransaction.replace(R.id.fragmentContainer, fragment)
//        fragmentTransaction.addToBackStack("EmailFragment")
//        fragmentTransaction.commit()
    }

    fun SignUpSuccessResponse(response: SignUpResponse){
        dismissProgressDialog()
        if (response.status.equals("success")){
            TableUtils.clearTable(DatabaseHelper.helper.connectionSource,User::class.java)
            TableUtils.clearTable(DatabaseHelper.helper.connectionSource,Categories::class.java)
            TableUtils.clearTable(DatabaseHelper.helper.connectionSource,SubCategories::class.java)
            userData = response
            var newUser: User? = User(-1,userData!!.data!!.userId,userData!!.data!!.email,"","",userData!!.data!!.subCategory,true,"",showTutorial)
            User().getRepository().create(newUser)
            showProgressDialog("Downloading categories...")
            GetAllCategoriesService(this).getCategories()
        }
    }

    fun CategoriesSuccessResponse(response: CategoriesResponse, isWeb: Boolean){
        dismissProgressDialog()
        if (response.status.equals("success")){
            lstCategories = response
            if (lstCategories!!.data!=null && lstCategories!!.data!!.isNotEmpty()){
                for (item in lstCategories!!.data!!){
                    var newCategory: Categories? = Categories(-1,item.cat,item.image)
                    var idCat = CategoriesDao().updateOrCreate(newCategory)
                    if (idCat>0){
                        for (item2 in item!!.subCat!!){
                            var newSubCategory: SubCategories? = SubCategories(-1,idCat,item2)
                            SubCategories().getRepository().create(newSubCategory)
                        }
                    }else{
                        Log.v("CreateCat","Id is less than 0")
                    }

                }
            }
            currentFragment = FRAGMENT_CATEGORIES
            fragment = CategoriesFragments()
            val fragmentTransaction = supportFragmentManager!!.beginTransaction()

            val bundle = Bundle()
//            bundle.putSerializable("DataList",response.data as Serializable)
//            bundle.putSerializable("UserData",userData as Serializable)
//            if (isWeb){
//                bundle.putBoolean("ToWeb", true)
//            }
//            fragment!!.setArguments(bundle)

            fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right)
            fragmentTransaction.replace(R.id.fragmentContainer, fragment)
            fragmentTransaction.addToBackStack("CategoriesFragments")
            fragmentTransaction.commit()
        }
    }

    fun goToCategories(){
        dismissProgressDialog()
            currentFragment = FRAGMENT_CATEGORIES
            fragment = CategoriesFragments()
            val fragmentTransaction = supportFragmentManager!!.beginTransaction()

            val bundle = Bundle()
//            bundle.putSerializable("DataList",response.data as Serializable)
//            bundle.putSerializable("UserData",userData as Serializable)
//            if (isWeb){
//                bundle.putBoolean("ToWeb", true)
//            }
//            fragment!!.setArguments(bundle)

            fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right)
            fragmentTransaction.replace(R.id.fragmentContainer, fragment)
            fragmentTransaction.addToBackStack("CategoriesFragments")
            fragmentTransaction.commit()
    }

    fun subCategoryIsSelected(newItem:SubCategoryModel):Boolean{
        var isSelected = false

        for (item in lstSelected) {
            Log.v("ItemClick", "Item For: "+item.toString())
            Log.v("ItemClick", "Checking Item For: "+item.toString()+" == "+newItem.toString())
            if (item.id == newItem.id){
                isSelected = true
            }
        }
        Log.v("ItemClick", "Is Selected: "+isSelected.toString())
        return isSelected
    }

    fun addSubCategory(newItem:SubCategoryModel){
        var exist = false
        for (item in lstSelected) {
            Log.v("ItemClick", "Item For: "+item.toString())
            Log.v("ItemClick", "Checking Item For: "+item.toString()+" == "+newItem.toString())
            if (item.id == newItem.id){
                exist = true
            }
        }
        Log.v("ItemClick", "Exist: "+exist.toString())
        if (!exist && lstSelected.size<3){
            Log.v("ItemClick", "Adding: "+newItem.toString())
            lstSelected.add(newItem)
        }
        Log.v("ItemClick", "New Count: "+lstSelected.size)
        if (fragment!=null){
            fragment!!.updateSelectedCount(lstSelected!!.size)
        }
        if (lstSelected!!.size==3){
            if (dialog==null || !dialog!!.isShowing){
                dialog = Dialog(this)
                dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialog!!.getWindow().setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                dialog!!.setContentView(R.layout.categories_dialog)

                var lstCategoriesText: TextView? = dialog!!.findViewById<TextView>(R.id.lstCategories) as TextView
                var catsName : String? = ""
                for (item in lstSelected){
                    catsName += "\n"+item.name+"\n"
                }
                lstCategoriesText!!.setText(catsName)
                var btnSave : Button? = dialog!!.findViewById<Button>(R.id.btnSave) as Button
                var btnReset : Button? = dialog!!.findViewById<Button>(R.id.btnReset) as Button
                btnSave!!.setOnClickListener(object: View.OnClickListener{
                    override fun onClick(v: View?) {
                        dialog!!.dismiss()
                        var cats: String? = ""
                        for (item in lstSelected){
                            if (cats!!.equals("")){
                                cats += item.id.toString()
                            }else{
                                cats += ","+item.id.toString()
                            }
                        }

                        showProgressDialog("Registering categories...")
                        RegisterUserCategories(this@SignupActivity).RegisterCategories(getUserData()!!.email!!,cats!!)
                        Log.v("CatsReview",cats)
                    }
                })
                btnReset!!.setText("Reset")
                btnReset!!.setOnClickListener(object: View.OnClickListener{
                    override fun onClick(v: View?) {
                        dialog!!.dismiss()
                        resetSelectedList()
                        fragment!!.fillAndResetRecycler()
                    }

                })
                dialog!!.setCancelable(false)
                dialog!!.show()
            }
        }
    }

    fun registerCategorySuccess(cats:String?){
        dismissProgressDialog()
        if (cats!=null){
            var currentUser = UserDao().findCurrentUser()
            if (currentUser!=null){
                currentUser.subCategory = cats
                User().getRepository().update(currentUser)
            }
            fragmentWeb = WebFragments()
            currentFragment = FRAGMENT_WEB
            val fragmentTransaction = supportFragmentManager!!.beginTransaction()

            val bundle = Bundle()
            bundle.putString("ToWeb",cats)
//            bundle.putInt("SincronizarConfiguracion", config)
            fragmentWeb!!.setArguments(bundle)

            fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right)
            fragmentTransaction.replace(R.id.fragmentContainer, fragmentWeb)
            fragmentTransaction.addToBackStack("WebFragments")
            fragmentTransaction.commit()
        }
    }

    fun goToWeb(){
        dismissProgressDialog()

        fragmentWeb = WebFragments()
        currentFragment = FRAGMENT_WEB
        val fragmentTransaction = supportFragmentManager!!.beginTransaction()

        fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right)
        fragmentTransaction.replace(R.id.fragmentContainer, fragmentWeb)
        fragmentTransaction.addToBackStack("WebFragments")
        fragmentTransaction.commit()
    }



    fun removeSubCategory(newItem:SubCategoryModel){
        var exist = false
        for (item in lstSelected) {
            if (item.id == newItem.id){
                exist = true
            }
        }
        if (exist){
            lstSelected.remove(newItem)
        }
        if (fragment!=null){
            fragment!!.updateSelectedCount(lstSelected!!.size)
        }
    }

    fun resetSelectedList(){
        lstSelected = ArrayList<SubCategoryModel>()
        if (fragment!=null){
            fragment!!.updateSelectedCount(0)
        }
    }

    fun keepOrResetSubCategories(){
        var dialog: Dialog? = Dialog(this)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.getWindow().setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog!!.setContentView(R.layout.categories_dialog)

        var lstCategoriesText: TextView? = dialog!!.findViewById<TextView>(R.id.lstCategories) as TextView
        var catsName : String? = ""
        for (item in lstSelected){
            catsName += "\n"+item.name+"\n"
        }
        lstCategoriesText!!.setText(catsName)
        var dialogTitle : TextView? = dialog!!.findViewById<TextView>(R.id.dialogTitle) as TextView
        dialogTitle!!.setText("Current sub categories selected")
        var btnSave : Button? = dialog!!.findViewById<Button>(R.id.btnSave) as Button
        var btnReset : Button? = dialog!!.findViewById<Button>(R.id.btnReset) as Button
        btnSave!!.setText("Keep")
        btnSave!!.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?) {
                dialog!!.dismiss()
                var cats: String? = ""
                for (item in lstSelected){
                    if (cats!!.equals("")){
                        cats += item.id.toString()
                    }else{
                        cats += ","+item.id.toString()
                    }
                }
                registerCategorySuccess(cats!!)
                Log.v("CatsReview",cats)
            }
        })
        btnReset!!.setText("Reset")
        btnReset!!.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?) {
                dialog!!.dismiss()
                resetSelectedList()
            }

        })
        dialog!!.setCancelable(false)
        dialog!!.show()
    }

    fun returnWebFragment(isSignOut:Boolean){
        if (!isSignOut){
            Log.v("SharedPrefference","Creating return categories")
            var sp = getSharedPreferences("ReturnCategories",Context.MODE_PRIVATE).edit()
            sp.putBoolean("Return",true)
            sp.apply()
            supportFragmentManager!!.popBackStackImmediate()
            currentFragment = FRAGMENT_CATEGORIES
        }else{
            supportFragmentManager!!.popBackStackImmediate("EmailFragment",0)
            currentFragment = FRAGMENT_SIGNUP
        }

    }

    fun returnSignIn(){
        supportFragmentManager!!.popBackStackImmediate()
        currentFragment = FRAGMENT_SIGNUP
    }

    override fun onBackPressed() {
        if (currentFragment == FRAGMENT_WEB) {
            Log.v("CurrentFragment", currentFragment.toString())
            if (!fragmentWeb!!.goBack()) {
                finish()
            }
        }else if (currentFragment == FRAGMENT_CATEGORIES) {
            Log.v("CurrentFragment", currentFragment.toString())
            finish()
        }else{
            finish()
        }
    }

    private fun scheduleNotification(notification: Notification, delay: Int) {

        val notificationIntent = Intent(this, NotificationBroadcast::class.java)
        notificationIntent.putExtra(NOTIFICATION_ID, 1)
        notificationIntent.putExtra(NOTIFICATION, notification)
        val pendingIntent = PendingIntent.getBroadcast(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val futureInMillis = SystemClock.elapsedRealtime() + delay
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent)

        val editor = getSharedPreferences("NotificationCreated", Context.MODE_PRIVATE).edit()
        editor.putBoolean("isCreated", true)
        editor.commit()
    }

    private fun scheduleNotificationBubble(notification: Notification, delay: Int) {
        Log.v("Notification", "Creating notification bubble schedule")
        val notificationIntent = Intent(this, NotificationBroadcast::class.java)
        notificationIntent.putExtra(NOTIFICATION_ID, 100)
        notificationIntent.putExtra(NOTIFICATION, notification)
        val pendingIntent = PendingIntent.getBroadcast(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val futureInMillis = SystemClock.elapsedRealtime() + delay
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent)

        val editor = getSharedPreferences("NotificationBubbleCreated", Context.MODE_PRIVATE).edit()
        editor.putBoolean("isCreated", true)
        editor.commit()
    }

    private fun getNotification(content: String): Notification {
        val builder = Notification.Builder(this)
        builder.setContentTitle("Scheduled Notification")
        builder.setContentText(content)
        builder.setSmallIcon(R.mipmap.ic_launcher)
        return builder.build()
    }

    private fun isNotificationCreated(): Boolean {
        var isCreated = false
        val editor = getSharedPreferences("NotificationCreated", Context.MODE_PRIVATE)
        isCreated = editor.getBoolean("isCreated", false)
        return isCreated
    }

    private fun isNotificationBubbleCreated(): Boolean {
        var isCreated = false
        val editor = getSharedPreferences("NotificationBubbleCreated", Context.MODE_PRIVATE)
        isCreated = editor.getBoolean("isCreated", false)
        Log.v("Notification","Is schedule bubble created: "+isCreated.toString())
        return isCreated
    }

    fun setBadge(context: Context, count: Int) {
        Log.v("NotificationBubble", "Count: "+count)
        val launcherClassName = getLauncherClassName(context) ?: return
        val intent = Intent("android.intent.action.BADGE_COUNT_UPDATE")
        intent.putExtra("badge_count", count)
        intent.putExtra("badge_count_package_name", context.packageName)
        intent.putExtra("badge_count_class_name", launcherClassName)
        context.sendBroadcast(intent)
    }

    fun getLauncherClassName(context: Context): String? {

        val pm = context.packageManager

        val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_LAUNCHER)

        val resolveInfos = pm.queryIntentActivities(intent, 0)
        for (resolveInfo in resolveInfos) {
            val pkgName = resolveInfo.activityInfo.applicationInfo.packageName
            if (pkgName.equals(context.packageName, ignoreCase = true)) {
                val className = resolveInfo.activityInfo.name
                return className
            }
        }
        return null
    }

//    fun showProgressDialog(msg:String?) {
//        progressDialog = ProgressDialog.show(this, "Please wait ...", msg, true)
//        progressDialog!!.setCancelable(true)
//    }
//
//    fun dismissProgressDialog(){
//        if (progressDialog!=null && progressDialog!!.isShowing){
//            progressDialog!!.dismiss()
//        }
//    }

    private val MY_PERMISSIONS_REQUEST_STORAGE = 10004
    private fun accountsPermission() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.GET_ACCOUNTS), MY_PERMISSIONS_REQUEST_STORAGE)
        }else if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.GET_ACCOUNTS_PRIVILEGED) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.GET_ACCOUNTS_PRIVILEGED), MY_PERMISSIONS_REQUEST_STORAGE)
        }else{
            notificationPermission()
        }
    }

    private val MY_PERMISSIONS_REQUEST_NOTIFICATION = 10005
    private fun notificationPermission() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_NOTIFICATION_POLICY) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_NOTIFICATION_POLICY), MY_PERMISSIONS_REQUEST_NOTIFICATION)
        }else{
            scheduleNotifications()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_STORAGE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                notificationPermission()
            }
            MY_PERMISSIONS_REQUEST_NOTIFICATION -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                scheduleNotifications()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (!isNotificationCreated()) {
            scheduleNotification(getNotification("Test Notification 2 Sec"), notificationTime * 1000)
        }
        if (!isNotificationBubbleCreated()){
            scheduleNotificationBubble(getNotification("Test Notification 2 Sec"), 6 * 1000)
        }
//        setBadge(this,0)
    }

    fun getUserData():User?{
        return UserDao().findCurrentUser()
    }

    fun signOut(){
        var dialog = AlertDialog.Builder(this)
        dialog.setTitle("Sign Out")
        dialog.setMessage("Are you sure you want to sign out?")
        dialog.setPositiveButton("Yes", object: DialogInterface.OnClickListener{
            override fun onClick(dialog: DialogInterface?, which: Int) {
                dialog!!.dismiss()
                var sp = getSharedPreferences("SignOut",Context.MODE_PRIVATE).edit()
                sp.putBoolean("isSignOut",true)
                sp.apply()

                returnWebFragment(true)
            }
        })
        dialog.setNegativeButton("No", object: DialogInterface.OnClickListener{
            override fun onClick(dialog: DialogInterface?, which: Int) {
                dialog!!.dismiss()
            }
        })
        dialog.setCancelable(false)
        dialog.show()
    }

    fun facebookLogin(){
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile","email", "user_friends","user_likes","user_posts"))
    }

    fun initFacebookClient(){
        FacebookSdk.sdkInitialize(getApplicationContext())
        AppEventsLogger.activateApp(this)

        mCallbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(mCallbackManager,object: FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {
                showProgressDialog("Loading...")
                Log.v("RegistroActivity", "Login")
                var request = GraphRequest
                        .newMeRequest(
                                result!!.accessToken,
                                object: GraphRequest.GraphJSONObjectCallback {
                                    override fun onCompleted(jsonObject: JSONObject?, response: GraphResponse?) {
                                        Log.v("RegistroActivity", response.toString())
                                        Log.v("RegistroActivity", jsonObject.toString())
                                        var idFacebook = jsonObject!!.get("id") as String
                                        var name = ""
                                        try{
                                            name = jsonObject.get("name") as String
                                            Log.v("UserName",name)
                                        }catch (e:Exception){
                                            Log.e("UserName",e.toString())
                                        }
                                        var email = jsonObject.get("email") as String
                                        var profilePicture = "https://graph.facebook.com/" + idFacebook + "/picture?type=large"
                                        Log.e("LoginToken",AccessToken.getCurrentAccessToken().token)
                                        currentUser = User(-1,
                                                -1,
                                                email,
                                                name,
                                                profilePicture,
                                                "",
                                                true,
                                                "",
                                                showTutorial,
                                                idFacebook,
                                                AccessToken.getCurrentAccessToken().token)
                                        Log.v("CurrentUser", "Login: "+currentUser.toString())
                                        dismissProgressDialog()
                                        getUsersFriends(idFacebook)
//                                        LoginService(this@LoginActivity).handleLogin(
//                                                email,
//                                                "",
//                                                3)

                                        Log.v("RegistroActivity", jsonObject.toString())
                                    }
                                })
                val parameters = Bundle()
                parameters.putString("fields", "id,name,email,gender, birthday")
                request.parameters = parameters
                request.executeAsync()

                //            Log.v("FacebookConnect","Email: "+socialUser.email)
                //            Log.v("FacebookConnect","Full name:"+socialUser.fullName)
                //            Log.v("FacebookConnect","Profile Picture: "+socialUser.profilePictureUrl)
                //            saveUser(socialUser,3)
            }

            override fun onCancel() {
                Toast.makeText(this@SignupActivity, "Login Cancel", Toast.LENGTH_LONG).show()
            }

            override fun onError(error: FacebookException?) {
                Toast.makeText(this@SignupActivity, error!!.message, Toast.LENGTH_LONG).show()
            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(mCallbackManager!!.onActivityResult(requestCode, resultCode, data)) {
            return
        }
    }

    fun getUsersFriends(userId:String){
        showProgressDialog("Getting Friends")
        var request = GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/"+userId+"/friends",
                null,
                HttpMethod.GET,
                object: GraphRequest.Callback {
                    override fun onCompleted(response: GraphResponse?) {
                        dismissProgressDialog()
                        if (response!=null){
                            try{
                                Log.v("FriendsUserObjects",response.rawResponse.toString())
                                var userFriends = Gson().fromJson(response.rawResponse, GetUserFriends::class.java)
                                if (userFriends!=null){
                                    if (userFriends.data!=null && userFriends.data!!.isNotEmpty()){
                                        for (i in userFriends.data!!){
                                            var profilePicture = "https://graph.facebook.com/"+i.id+"/picture?type=large"
                                            var friend = UserFriendsDao().getFriendByFacebookId(i.id)
                                            if (friend==null){
                                                friend = UserFriends(-1,i.email,i.name,i.gender,profilePicture,i.id)
                                            }
                                            UserFriendsDao().updateOrCreate(friend)
                                            Log.v("FriendsUserItem",friend.toString())
                                        }
                                    }
                                }
                            }catch (e:Exception){
                                Log.e("FriendsUserObjects",e.toString())
                            }
                        }
                        dismissProgressDialog()
                        getUsersLikes(userId)
                    }
                }
        )
        request.executeAsync()
    }

    fun getUsersLikes(userId:String){
        showProgressDialog("Getting user likes")
        var request = GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/"+userId+"/likes",
                null,
                HttpMethod.GET,
                object: GraphRequest.Callback {
                    override fun onCompleted(response: GraphResponse?) {
                        dismissProgressDialog()
                        if (response!=null){
                            Log.v("RegistroActivityLikes", response.toString())
                            Log.v("RegistroActivityLikes", response.rawResponse.toString())
                            Log.v("RegistroActivityLikes", response.jsonObject.toString())
                            var userLikes = Gson().fromJson(response.rawResponse,UserLikesResponse::class.java)
                            if (userLikes!=null){
                                Log.v("UserLikes", userLikes.toString())
                                if (currentUser!=null){
                                    var likes = ""
                                    for (item in userLikes.data!!){
                                        var value = item.name!!.replace("'","")
                                        likes+= value+","
                                    }
                                    currentUser?.likes = likes
                                    User().getRepository().create(currentUser)
                                    Log.v("CurrentUser",currentUser.toString())
                                    dismissProgressDialog()
                                    finish()
//                                    startActivity(Intent(this@SignupActivity,Main2Activity::class.java))
                                    startService(Intent(this@SignupActivity, FloatingViewService::class.java))
                                }
                            }
                        }
                    }
                }
        )
        request.executeAsync()
    }

    @SuppressLint("ResourceAsColor")
    fun showFirstTutorial(){
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow().setBackgroundDrawable(ColorDrawable(R.color.dialogBgColor))
        dialog.setContentView(R.layout.tutorial_first)
        val lp = WindowManager.LayoutParams()
        val window = dialog.getWindow()
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        window.setAttributes(lp)

        val picker:NumberPicker = dialog!!.findViewById(R.id.spLanguage)
        picker.setMinValue(0)
        picker.setMaxValue(languages.size-1)
        picker.setDisplayedValues(languages)

        picker.setOnValueChangedListener { numberPicker, i, j ->
            Log.v("ValueChanged","I: "+i+", J: "+j+", Value: "+languages[j])

        }

        val btnNext = dialog.findViewById<RelativeLayout>(R.id.btnNext) as RelativeLayout
        btnNext.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?) {
                Log.v("ValueChanged","PickerValue: "+languages[picker.value])
                selectedLanguage = languages[picker.value]
                var settings = SettingsDao().getSettings()
                if (settings==null)
                    settings = Settings()
                settings.language = selectedLanguage
                settings.languageShort = CountryUtil().getLanguageCodeFromLanguage(selectedLanguage)
                SettingsDao().updateOrCreate(settings)
                dialog.dismiss()

                this@SignupActivity.runOnUiThread {
                    showCountryTutorial()
                }
            }
        })
        dialog.setCancelable(false)
        dialog.show()
    }

    @SuppressLint("ResourceAsColor")
    fun showCountryTutorial(){
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow().setBackgroundDrawable(ColorDrawable(R.color.dialogBgColor))
        dialog.setContentView(R.layout.tutorial_first)
        val lp = WindowManager.LayoutParams()
        val window = dialog.getWindow()
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        window.setAttributes(lp)

        val countries = CountryUtil().countriesAux

        val picker:NumberPicker = dialog!!.findViewById(R.id.spLanguage)
        picker.setMinValue(0)
        picker.setMaxValue(countries.size-1)
        picker.setDisplayedValues(countries)

        val tm = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        val countryCode = tm.simCountryIso
        val country = CountryUtil().getCountryByCode(countryCode!!.replace(" ",""))
        if (country!=null && country.imgFlag>0){
            Log.v("CountryData",country.toString())
            picker.value = country.imgFlag
        }else{
            picker.value = 1
            Log.v("CountryCode","Country data is null")
        }
        Log.v("CountryCode",countryCode)

        picker.setOnValueChangedListener { numberPicker, i, j ->
            Log.v("ValueChanged","I: "+i+", J: "+j+", Value: "+countries[j])

        }

        val btnNext = dialog.findViewById<RelativeLayout>(R.id.btnNext) as RelativeLayout
        btnNext.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?) {
                Log.v("ValueChanged","PickerValue: "+countries[picker.value])
                val selectedCountry = countries[picker.value]
                var settings = SettingsDao().getSettings()
                if (settings==null)
                    settings = Settings()
                settings.country = selectedCountry
                SettingsDao().updateOrCreate(settings)
                dialog.dismiss()

                this@SignupActivity.runOnUiThread {
                    showSecondTutorial()
                }
            }
        })
        dialog.setCancelable(false)
        dialog.show()
    }

    @SuppressLint("ResourceAsColor")
    fun showSecondTutorial(){
        var dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow().setBackgroundDrawable(ColorDrawable(R.color.dialogBgColor))
        dialog.setContentView(R.layout.tutorial_three)
        var lp = WindowManager.LayoutParams()
        var window = dialog.getWindow()
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        window.setAttributes(lp)

        var btnNext = dialog!!.findViewById<RelativeLayout>(R.id.btnNext) as RelativeLayout
        btnNext.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?) {
                dialog.dismiss()

                this@SignupActivity.runOnUiThread {
                    showThirdTutorial()
                }
            }
        })
        dialog.setCancelable(false)
        dialog.show()
    }

    @SuppressLint("ResourceAsColor")
    fun showThirdTutorial(){
        var dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow().setBackgroundDrawable(ColorDrawable(R.color.dialogBgColor))
        dialog.setContentView(R.layout.tutorial_four)
        var lp = WindowManager.LayoutParams()
        var window = dialog.getWindow()
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        window.setAttributes(lp)

        var btnNext = dialog!!.findViewById<RelativeLayout>(R.id.btnNext) as RelativeLayout
        btnNext.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?) {
                dialog.dismiss()

                this@SignupActivity.runOnUiThread {
                    showSixthTutorial()
                }
            }
        })
        dialog.setCancelable(false)
        dialog.show()
    }

    @SuppressLint("ResourceAsColor")
    fun showFourthTutorial(){
        var dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow().setBackgroundDrawable(ColorDrawable(R.color.dialogBgColor))
        dialog.setContentView(R.layout.tutorial_fourth)
        var lp = WindowManager.LayoutParams()
        var window = dialog.getWindow()
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        window.setAttributes(lp)

        var txtComment = dialog!!.findViewById<TextView>(R.id.txtComment) as TextView
        txtComment!!.setText(getEmojiByUnicode(thumbDownUnicode)+" Lucky shot Drake")

        var btnFinal = dialog!!.findViewById<RelativeLayout>(R.id.btnNext) as RelativeLayout
        btnFinal.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?) {
                dialog.dismiss()
                this@SignupActivity.runOnUiThread{
//                    showFifthTutorial()
                }
            }
        })
        dialog.setCancelable(false)
        dialog.show()
    }

//    @SuppressLint("ResourceAsColor")
//    fun showFifthTutorial(){
//        var dialog = Dialog(this)
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        dialog.getWindow().setBackgroundDrawable(ColorDrawable(R.color.dialogBgColor))
//        dialog.setContentView(R.layout.tutorial_five)
//        var lp = WindowManager.LayoutParams()
//        var window = dialog.getWindow()
//        lp.copyFrom(window.getAttributes());
//        //This makes the dialog take up the full width
//        lp.width = WindowManager.LayoutParams.MATCH_PARENT
//        lp.height = WindowManager.LayoutParams.MATCH_PARENT
//        window.setAttributes(lp)
//
//        val bubbleContainer: ConstraintLayout = dialog!!.findViewById(R.id.bubbleContainer)
//        val bubbleLayout: BubbleLayout = dialog!!.findViewById(R.id.bubbleLayout)
//        val txtBubble: TextView = dialog!!.findViewById(R.id.txtBubble)
//
//        val settings = SettingsDao().getSettings()
//        if (settings?.showDislike!!){
//            bubbleContainer?.visibility = View.VISIBLE
//            txtBubble?.setText("Click here to dislike")
//            bubbleLayout?.arrowPosition = 100f
//            bubbleContainer!!.setOnClickListener {
//                settings?.showDislike = false
//                SettingsDao().updateOrCreate(settings)
//                if (settings?.showLike!!){
//                    bubbleLayout?.arrowPosition = 400f
//                    txtBubble?.setText("Click here to like")
//                    bubbleContainer!!.setOnClickListener {
//                        settings?.showLike = false
//                        SettingsDao().updateOrCreate(settings)
//                        dialog.dismiss()
//                    }
//                }else{
//                    dialog.dismiss()
//                }
//            }
//        }else if(settings?.showLike){
//            bubbleContainer?.visibility = View.VISIBLE
//            bubbleLayout?.arrowPosition = 400f
//            txtBubble?.setText("Click here to like")
//            bubbleContainer!!.setOnClickListener {
//                settings?.showLike = false
//                SettingsDao().updateOrCreate(settings)
//                dialog.dismiss()
//            }
//        }
//
//        dialog.setCancelable(false)
//        dialog.show()
//    }

    @SuppressLint("ResourceAsColor")
    fun showSixthTutorial(){
        var dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow().setBackgroundDrawable(ColorDrawable(R.color.dialogBgColor))
        dialog.setContentView(R.layout.tutorial_seventh)
        var lp = WindowManager.LayoutParams()
        var window = dialog.getWindow()
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        window.setAttributes(lp)

        var btnFb = dialog!!.findViewById<RelativeLayout>(R.id.btnFacebook) as RelativeLayout
        btnFb.setOnClickListener(object: View.OnClickListener {
            override fun onClick(v: View?) {
                dialog.dismiss()
                facebookLogin()
                showTutorial = false
            }
        })

        dialog.setCancelable(false)
        dialog.show()
    }

    fun showProgressDialog(msg:String?){
        if (msg!=null){
            pDialog = SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
            pDialog?.getProgressHelper()?.setBarColor(R.color.drewpicColor)
            pDialog?.setTitleText(msg)
            pDialog?.setCancelable(false)
            pDialog?.show()
        }
    }

    fun dismissProgressDialog(){
        if (pDialog!=null && pDialog!!.isShowing){
            pDialog!!.dismiss()
        }
    }

    fun getEmojiByUnicode(unicode: Int): String {
        return String(Character.toChars(unicode))
    }

}
