package com.aarr.drewpic.Views

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.util.TypedValue
import android.view.*
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.*
import cn.pedant.SweetAlert.SweetAlertDialog
import com.aarr.drewpic.Adapters.FeedsAdapter
import com.aarr.drewpic.Adapters.RepliesAdapter
import com.aarr.drewpic.Adapters.RssFeedAdapter
import com.aarr.drewpic.Database.Dao.*
import com.aarr.drewpic.Database.Helper.DatabaseHelper
import com.aarr.drewpic.Database.Model.FeedReplies
import com.aarr.drewpic.Database.Model.PostImage
import com.aarr.drewpic.Database.Model.RssFeedModel
import com.aarr.drewpic.Database.Model.UserFriends
import com.aarr.drewpic.R
import com.aarr.drewpic.Services.*
import com.aarr.drewpic.Services.Asyncs.AsyncLoadFeed
import com.aarr.drewpic.Services.Asyncs.AsyncLoadFriendFeed
import com.aarr.drewpic.Services.Asyncs.AsyncLoadFriendReplies
import com.aarr.drewpic.Services.Response.GetFriendsPosts.Posts
import com.aarr.drewpic.Services.Response.GetFriendsPosts.Replies
import com.aarr.drewpic.Services.Response.UserFriends.GetUserFriends
import com.aarr.drewpic.Util.DateManage
import com.aarr.drewpic.Util.RandomValues
import com.bumptech.glide.Glide
import com.daasuu.bl.BubbleLayout
import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.facebook.GraphResponse
import com.facebook.HttpMethod
import com.google.gson.Gson
import com.j256.ormlite.table.TableUtils
import com.jackandphantom.circularimageview.CircleImage
import com.lsjwzh.widget.materialloadingprogressbar.CircleProgressBar
import com.yuyakaido.android.cardstackview.CardStackView
import com.yuyakaido.android.cardstackview.SwipeDirection
import id.co.flipbox.sosoito.LoadingLayout
import kotlinx.android.synthetic.main.activity_main2.*
import kotlinx.android.synthetic.main.feed_item.*
import me.toptas.rssconverter.RssFeed
import me.toptas.rssconverter.RssItem
import retrofit2.Call
import java.io.ByteArrayOutputStream
import java.util.*

class Main2Activity : AppCompatActivity(), FeedsAdapter.IconClickListener, RssFeedAdapter.ShareButtonListener, RssFeedAdapter.FeedButtonListener {

    private var feedLst: MutableList<RssFeedModel>? = ArrayList<RssFeedModel>()
    private var repliesLst: MutableList<FeedReplies>? = ArrayList<FeedReplies>()
    var pDialog: SweetAlertDialog? = null
    var currentPosition: Int = 0
    private var currentFeedPosition: Int = -1
    var currentRequestPosition: Int = 0
    private var adapterCard: RssFeedAdapter? = null
    private var adapter: FeedsAdapter? = null
    private var userLikes: List<String> ? = null
    private var userFriendsLst: MutableList<UserFriends> ? = null
    var searchValue = ""
    var thumbUpUnicode: Int = 0x1F44D
    var thumbDownUnicode: Int = 0x1F44E
    var thumbUpUnicodeString = "0x1F44D"
    var thumbDownUnicodeString = "0x1F44E"
    var thumbUpUnicodeParsed = "128077"
    var thumbDownUnicodeParsed = "128078"
    var isServiceRunning = false
    private val likeStatus = "Like"
    private val replyStatus = "Reply"
    private var currentSection = likeStatus
    private val SECTION_HOME = 100
    private val SECTION_SEARCH = 200
    private val SECTION_NOTIFICATION = 300
    private var currentPage = SECTION_HOME
    var feedCounter = 1
    private var swipedFeed:RssFeedModel?=null
    private var isReversed = false
    private var currentReply: FeedReplies?=null
    private var friendPostCaption:String?=null
    private var loadingLayout: LoadingLayout?=null
    val ACTION_REVERSE = 101
    val ACTION_DISLIKE = 202
    val ACTION_LIKE = 303
    var shareToFacebook = false
    var shareToFacebookSwiped = false
    var encodedImg: String = ""
    var currentFeedCall : Call<RssFeed>? = null
    var apiCallSearch = false
    var resetCards = false
    var addTrend=false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        val user = UserDao().findCurrentUser()
        Log.v("CurrentUser", "Login: "+user.toString())


        if (currentPage==SECTION_HOME){
            fillAndResetRecycler(null,-1)
            fillAndResetCardStack(null)
            refillCardStack(null,null,false)
        }

        btnClose.setOnClickListener{
            finish()
        }

        btnHome.setOnClickListener {
            if (currentPage!=SECTION_HOME){
                searchValue = ""
                if (searchView!=null){
                    when (searchView?.visibility){
                        View.VISIBLE -> searchView?.visibility = View.GONE
                    }
                }
                fillAndResetRecycler(null,-1)
                fillAndResetCardStack(null)
                refillCardStack(null,null,false)
                cardStackView?.setCardEventListener(getCardSwipeListener())
                notificationContainer.visibility = View.GONE
                currentPage = SECTION_HOME
//                hideKeyboard()
                currentPosition= 0
                currentFeedPosition = -1
                currentRequestPosition = 0
                doSearch()
            }
        }

        btnSearch.setOnClickListener {
            currentPage = SECTION_SEARCH
            if (searchView!=null){
                when (searchView?.visibility){
                    View.VISIBLE -> searchView?.visibility = View.GONE
                    View.GONE -> searchView?.visibility = View.VISIBLE
                }
            }
            rssDetails.visibility = View.GONE
            notificationContainer.visibility = View.GONE
            hideKeyboard()
        }

        btnNotification.setOnClickListener {
            currentPage = SECTION_NOTIFICATION
            searchValue = ""
            if (searchView!=null){
                searchView?.visibility = View.GONE
            }
            if (notificationContainer.visibility != View.VISIBLE){
                rssDetails.visibility = View.GONE
                fillAndResetNotificationRecycler(false,likeStatus)
                notificationContainer.visibility = View.VISIBLE
                getFriendsReplies()
            }
            hideKeyboard()
        }

        lblLike.setOnClickListener {
            currentSection = likeStatus
            rssDetails.visibility = View.GONE
            fillAndResetNotificationRecycler(false,likeStatus)
            lblNoReplies.setText("You have no likes :(")
            hideKeyboard()
        }

        lblReplies.setOnClickListener {
            currentSection = replyStatus
            rssDetails.visibility = View.GONE
            fillAndResetNotificationRecycler(false,replyStatus)
            lblNoReplies.setText("You have no replies :(")
            hideKeyboard()
        }

        txtSearch?.threshold = 1
        txtSearch?.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                val txtValue = p0.toString()
                if (txtValue.isNullOrEmpty())
                    resetAutocomplete(emptyList())
                else
                    getAutocomplete(txtValue)

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

        })

        txtSearch?.setOnEditorActionListener(object: TextView.OnEditorActionListener{
            override fun onEditorAction(p0: TextView?, p1: Int, p2: KeyEvent?): Boolean {
                if (p1 == EditorInfo.IME_ACTION_DONE) {
                    val keyword = p0?.text.toString()
                    if (!keyword.isNullOrEmpty()){
                        Log.v("DetailsStatus","Keyword: "+keyword)
                        fillAndResetRecycler(keyword,-1)
                        refillCardStack(null,null,true)
                        hideKeyboard()
                    }else{
                        fillAndResetRecycler(null,-1)
                        refillCardStack(null,null,false)
                        hideKeyboard()
                    }
                    return true
                }
                return false
            }
        })

        btnSendSearch?.setOnClickListener {
            if (currentFeedCall!=null && currentFeedCall!!.isExecuted){
                currentFeedCall!!.cancel()
                currentFeedCall = null
            }
            apiCallSearch = true
            val keyword = txtSearch.text.toString()
            if (!keyword.isNullOrEmpty()){
                Log.v("DetailsStatus","Keyword: "+keyword)
                requestNewRss(keyword)
//                fillAndResetRecycler(keyword,-1)
//                refillCardStack(null,null,true)
            }else{
                fillAndResetRecycler(null,-1)
                refillCardStack(null,null,false)
            }
            hideKeyboard()
        }

        try{
//            Glide.with(this).load(R.mipmap.widget_home).into(btnHome)
//            Glide.with(this).load(R.mipmap.widget_search).into(btnSearch)
//            Glide.with(this).load(R.mipmap.widget_notification).into(btnNotification)
//            Glide.with(this).load(R.mipmap.widget_collapse).into(btnClose)
        }catch (e:Exception){
            Log.e("LoadImageException",e.toString())
        }

        currentPosition = 0
        doSearch()

        val settings = SettingsDao().getSettings()
        if (settings?.showDislike!! || settings.showLike){
            showTutorial()
        }
    }

    fun resetAutocomplete(values:List<String>){
        val adapter = ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,values)
        txtSearch.setAdapter(adapter)
    }

    fun getAutocomplete(keyword:String){
        CallSearchAutocomplete(this).getAutocomplete(keyword)
    }

    fun hideKeyboard(){
        val imm = this.getSystemService(android.app.Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        if (imm.isActive){
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
        }
    }

    fun manageLoadingLayout(show:Boolean,msg:String){
        pgrCardStackMsg.showLoading(show,msg)
    }

    fun showRepliedCard(card:RssFeedModel,reply:FeedReplies){
        notificationContainer.visibility = View.GONE
        val auxLst: MutableList<RssFeedModel> = ArrayList<RssFeedModel>()
        auxLst.add(card)
        rssDetails?.visibility = View.VISIBLE
        cardStackView?.visibility = View.VISIBLE
        cardStackView?.setCardEventListener(object: CardStackView.CardEventListener{
            override fun onCardDragging(percentX: Float, percentY: Float) {}

            override fun onCardSwiped(direction: SwipeDirection?) {
                cardStackView?.visibility = View.GONE
                rssDetails?.visibility = View.GONE
                notificationContainer.visibility = View.VISIBLE
            }

            override fun onCardReversed() {}

            override fun onCardMovedToOrigin() {}

            override fun onCardClicked(index: Int) {
//
            }

        })
        Log.v("ShowingCard","Reply: "+reply.toString())
        refillCardStack(auxLst,reply,false)
    }

    private fun getFriendsReplies(){
        showProgressBar()
        val user = UserDao().findCurrentUser()
        GetPostsHistoryService(this).getHistory(user!!.idFacebook!!)
    }

    private fun getFacebookToken():String?{
        val token = AccessToken.getCurrentAccessToken()
        var tokenValue = ""
        if (token!=null){
            tokenValue = token.token
            Log.e("FacebookToken",token.token)
        }else{
            tokenValue = UserDao().findCurrentUser()?.token!!
        }
        return tokenValue
    }

    private fun updateIconsAdapter(feed:RssFeedModel?,activePos:Int,reload:Boolean,showDialog:Boolean, reset:Boolean){
        Log.e("UpdatingIcons","Initing")
        var feedValue = feed
        if (adapter!=null){
            Log.e("UpdatingIcons","Adapter is not null")
            if (feedValue==null){
                feedValue = RssFeedModel(
                        -12345,
                        -1,
                        searchValue,
                        "",
                        "",
                        "",
                        0,
                        "",
                        "",
                        false,
                        "",
                        "",
                        0,
                        0,
                        false,
                        "",
                        true,
                        false)
            }
            Log.e("UpdatingIcons","Item to Remove: "+feedValue.toString())
            if (feedLst!=null && feedLst!!.isNotEmpty()){
                var itemPosition = -1
                for (item in feedLst!!){
                    if (item.idMobile == feedValue.idMobile){
                        Log.e("UpdatingIcons","Removing: "+feedValue.toString())
                        itemPosition = feedLst!!.indexOf(item)
                    }
                }
                if (itemPosition>-1)
                    feedLst!!.removeAt(itemPosition)
            }
            if (reload){
                Log.e("UpdatingIcons","Reloading icons")
                val newItems = if (searchValue.isNullOrEmpty()){
                    Log.e("ResetRecycler","Reloading icons with no search")
                    RssFeedDao().findAllUnviewedFeedsWithFriends(null,addTrend)
                }else{
                    Log.e("ResetRecycler","Reloading icons with search: "+searchValue)
                    RssFeedDao().findAllUnviewedFeedsWithFriends(searchValue,addTrend)
                }
                addTrend = !addTrend
                try{
                    val lastFeed = feedLst!![0]
                    newItems!!.remove(lastFeed)
                }catch (e:Exception){
                    Log.e("RemovingDuplicated","Exception: "+e.toString())
                }
                if (reset){
                    feedLst!!.clear()
                }
                feedLst!!.addAll(newItems!!)
            }
            Log.e("UpdatingIcons","Active Position: "+activePos)
            adapter!!.clear()
            adapter!!.setItems(feedLst!!)
            adapter!!.setActivePosition(activePos)
            adapter!!.notifyDataSetChanged()
        }else{
            fillAndResetRecycler(searchValue,activePos)
        }
//        if (feed!=null && showDialog)
//            showDialogCard(feed)
    }

    private fun fillAndResetRecycler(search:String?,activePos:Int){
        Log.v("FeedIconList","Reseting Icon Recycler")
        recycler?.adapter = null

        feedLst = if (search.isNullOrEmpty()){
            RssFeedDao().findAllUnviewedFeedsWithFriends(null,addTrend)
        }else{
            searchValue = search!!
            Log.v("ResetRecycler","Reset with search: "+searchValue)
            RssFeedDao().findAllUnviewedFeedsWithFriends(search,addTrend)
        }
        addTrend = !addTrend
        adapter = FeedsAdapter(this, feedLst!!,activePos)
        adapter?.setCustomItemClickListener(this)
        recycler?.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recycler?.adapter = adapter
        if (feedLst!!.isEmpty()){
            Log.v("FeedIconList","Size: Empty")
        }else{
            try{
                val size = feedLst!!.size - 1
                recycler?.scrollToPosition(0)
                recycler?.scrollToPosition(size)
                recycler?.scrollToPosition(0)
            }catch (e:Exception){
                Log.e("RecyclerScroll",e.toString())
            }
            Log.v("FeedIconList","Size: "+feedLst?.size)
        }
    }

    fun fillAndResetNotificationRecycler(ascending:Boolean,status:String){
        hideProgressBar()
        Log.v("FeedIconList","Reseting Recycler")
        repliesRecycler?.adapter = null

        repliesLst = FeedRepliesDao().getReplies(ascending,currentSection)
        if (repliesLst == null || repliesLst!!.isEmpty()){
            lblNoReplies.visibility = View.VISIBLE
        }else{
            lblNoReplies.visibility = View.GONE
        }
        val adapter = RepliesAdapter(this, repliesLst!!)
//        adapter.setCustomItemClickListener(this)


        val lManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        val mDividerItemDecoration = DividerItemDecoration(
                repliesRecycler.getContext(),
                lManager.getOrientation())

        repliesRecycler.addItemDecoration(mDividerItemDecoration)
        repliesRecycler?.layoutManager = lManager
        repliesRecycler?.adapter = adapter
    }

    private fun refillCardStack(auxLst:List<RssFeedModel>?,reply:FeedReplies?,isSearch:Boolean){
        Log.v("FeedIconList","Refilling CardStack")
        Log.v("ShowingCard","Refill: "+reply.toString())
        currentReply = reply
        fillAndResetCardStack(reply)
        adapterCard!!.setAdapterType(isSearch)
        adapterCard!!.clear()
        if (auxLst!=null && auxLst.isNotEmpty())
            adapterCard!!.addAll(auxLst)
        else
            adapterCard!!.addAll(feedLst)
        adapterCard!!.notifyDataSetChanged()

//        cardStackView?.getChildAt(adapterCard?.count!!-1)?.requestFocus()
        cardStackView?.visibility = View.VISIBLE
        rssDetails?.visibility = View.VISIBLE
        if (feedLst==null || feedLst!!.isEmpty())
            manageLoadingLayout(false,"No Feeds")
    }

    private fun fillAndResetCardStack(reply:FeedReplies?){
        currentReply = reply
        Log.v("FeedIconList","Reseting CardStack Recycler")
        adapterCard = RssFeedAdapter(this,reply)
        adapterCard?.setShareButtonListener(this)
        adapterCard?.setFeedButtonListener(this)
//        cardStackView?.reverse()
        cardStackView?.setAdapter(adapterCard)
        cardStackView?.setCardEventListener(getCardSwipeListener())
        cardStackView?.visibility = View.VISIBLE
    }

    fun showMessageDialog(msg:String?, imgUrl:String?, feed:RssFeedModel){
        if (msg!=null){
            pDialog = SweetAlertDialog(this, SweetAlertDialog.NORMAL_TYPE)
            pDialog?.setTitleText("Share feed to facebook?")
            pDialog?.window!!.setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT)
            pDialog?.setCancelable(false)
            pDialog?.confirmText = "Yes"
            pDialog?.cancelText = "No"
            pDialog?.setCancelClickListener {
//                TableUtils.clearTable(DatabaseHelper.helper.connectionSource,PostImage::class.java)
                dismissDialog()
                SavePostService(this@Main2Activity).savePost(getFacebookToken()!!,feed,false,"Reply")
            }
            pDialog?.setConfirmClickListener {
//                TableUtils.clearTable(DatabaseHelper.helper.connectionSource,PostImage::class.java)
                dismissDialog()
                PostPhotoFacebookService(this).postPhoto(getFacebookToken()!!,feed.title,imgUrl!!,feed,true,"Reply")
//                collapsedView?.visibility = View.VISIBLE
//                expandedView?.visibility = View.GONE
//
//                val dialogIntent = Intent(this, PostActivity::class.java)
//                dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                dialogIntent.putExtra("ImgUrl",imgUrl)
//                startActivity(dialogIntent)
            }
            pDialog?.show()
        }
    }

    fun showMessageDialog2(imgUrl:String?, feed:RssFeedModel, type: Int, comment: TextView,view:View){
        val userName = UserDao().findCurrentUser()?.name
        var commentValue = comment.text.toString()

        pDialog = SweetAlertDialog(this, SweetAlertDialog.NORMAL_TYPE)
        pDialog?.setTitleText("Wait!")
        pDialog?.setContentText("Before we share this to facebook did you like or dislike")
        pDialog?.window!!.setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT)
        pDialog?.setCancelable(false)
        pDialog?.confirmText = getEmojiByUnicode(thumbUpUnicode)
        pDialog?.cancelText = getEmojiByUnicode(thumbDownUnicode)
        pDialog?.setCancelClickListener {
//                TableUtils.clearTable(DatabaseHelper.helper.connectionSource,PostImage::class.java)
//                dismissDialog()
//                SavePostService(this@Main2Activity).savePost(getFacebookToken()!!,feed,false,"Reply")
            pDialog?.dismiss()
            if (commentValue.isNullOrEmpty()){
                val txtDislike = getEmojiByUnicode(thumbDownUnicode)+" "+userName+" Dislikes this"
                comment.setText(txtDislike)
                if (view!=null){
                    val bitmap = viewToBitmap(view)
                    encodedImg = encodeToBase64(bitmap, Bitmap.CompressFormat.PNG,100)
                }
                object: CountDownTimer(2000,1000){
                    override fun onTick(millisUntilFinished: Long) = Unit

                    override fun onFinish(){
                        swipeLeft()
                    }
                }.start()
            }else{
                swipeLeft()
            }
        }
        pDialog?.setConfirmClickListener {
//                TableUtils.clearTable(DatabaseHelper.helper.connectionSource,PostImage::class.java)
//                dismissDialog()
//                PostPhotoFacebookService(this).postPhoto(getFacebookToken()!!,feed.title,imgUrl!!,feed,true,"Reply")
            pDialog?.dismiss()
            if (commentValue.isNullOrEmpty()){
                val txtLike = getEmojiByUnicode(thumbUpUnicode)+" "+userName+" Likes this"
                comment.setText(txtLike)
                if (view!=null){
                    val bitmap = viewToBitmap(view)
                    encodedImg = encodeToBase64(bitmap, Bitmap.CompressFormat.PNG,100)
                }
                object: CountDownTimer(2000,1000){
                    override fun onTick(millisUntilFinished: Long) = Unit

                    override fun onFinish(){
                        swipeRight()
                    }
                }.start()
            }else{
                swipeRight()
            }
        }
        pDialog?.show()
        /*else{
            when (type){
                ACTION_DISLIKE -> {
                    Log.v("FeedButtonListener","Dislike Feed")
//                    var commentValue = comment.text.toString()
//                    if (commentValue.isNullOrEmpty()){
//                        val txtDislike = getEmojiByUnicode(thumbDownUnicode)+" "+userName+" Dislikes this"
//                        comment.setText(txtDislike)
//                        object: CountDownTimer(2000,1000){
//                            override fun onTick(millisUntilFinished: Long) = Unit
//
//                            override fun onFinish(){
//
//                            }
//                        }.start()
//                    }else{
                        swipeLeft()
//                    }
                }
                ACTION_LIKE -> {
                    Log.v("FeedButtonListener","Like Feed")
//                    var commentValue = comment.text.toString()
//                    if (commentValue.isNullOrEmpty()) {
//                        val txtLike = getEmojiByUnicode(thumbUpUnicode)+" "+userName+" Likes this"
//                        comment.setText(txtLike)
//                        object: CountDownTimer(2000,1000){
//                            override fun onTick(millisUntilFinished: Long) = Unit
//
//                            override fun onFinish(){
//                                swipeRight()
//                            }
//                        }.start()
//                    }else{
                        swipeRight()
//                    }
                }
            }
        }*/
    }

    override fun onItemClickListener(position: Int, feed: RssFeedModel) {
        /*when (rssDetails!!.visibility){
            View.GONE->{
                Log.v("DetailsStatus","SHOWING")
                rssDetails!!.visibility = View.VISIBLE
            }
            View.VISIBLE->{
                Log.v("DetailsStatus","HIDDING")
                rssDetails!!.visibility = View.GONE
            }
        }*/
        currentFeedPosition = feed.idMobile
        Log.v("DetailsStatus","Keyword: "+feed.keyword)
        Log.v("DetailsStatus","Position: "+position)
        var isSearch = false
        if (!searchValue.isNullOrEmpty()){
            feedLst = RssFeedDao().findAllUnviewedFeedsBySearch(searchValue)
            isSearch = true
        }else{
            feedLst = RssFeedDao().findAllUnviewedFeedsBySearch(null)
        }
        if (feedLst!=null && feedLst!!.isNotEmpty()){
            for (item in feedLst!!){
                Log.v("CardSelected",item.toString())
            }
        }
        refillCardStack(null,null,isSearch)
        cardStackView?.visibility = View.VISIBLE
        cardStackView?.setCardEventListener(getCardSwipeListener())
        Log.v("StartCommand","Feed List is not null: "+feedLst!!.size)
//            }else{
//                Log.e("StartCommand","Feed List is null")
        /*if (currentFeedPosition == feed.idMobile){
                Log.v("DetailsStatus","Position: "+position)
                Log.v("DetailsStatus","Keyword: "+feed.keyword)
                if (feedLst==null || feedLst!!.isEmpty()){
                    if (!searchValue.isNullOrEmpty()){
                        feedLst = RssFeedDao().findAllUnviewedFeedsBySearch(searchValue)
                    }else{
                        feedLst = RssFeedDao().findAllUnviewedFeedsBySearch(null)
                    }
                }
//                if (feedLst!=null && feedLst!!.size>0){
//                if (adapterCard==null)
//                    fillAndResetCardStack()
//                adapterCard!!.setAdapterType(false)
//                adapterCard!!.clear()
//                adapterCard!!.addAll(feedLst)
//                adapterCard!!.notifyDataSetChanged()
                refillCardStack(null,null)
                cardStackView?.visibility = View.VISIBLE
                cardStackView?.setCardEventListener(getCardSwipeListener())

                Log.v("StartCommand","Feed List is not null: "+feedLst!!.size)
//                }else{
//                    Log.e("StartCommand","Feed List is null")
//                }
                currentFeedPosition = feed.idMobile
                Log.v("DetailsStatus","Opened")
//            btnOpenDetails!!.setImageResource(R.mipmap.expand32)
                rssDetails!!.visibility = View.VISIBLE
        }else{
            currentFeedPosition = feed.idMobile
            Log.v("DetailsStatus","Opened")
            Log.v("DetailsStatus","Keyword: "+feed.keyword)
            Log.v("DetailsStatus","Position: "+position)
            if (!searchValue.isNullOrEmpty()){
                feedLst = RssFeedDao().findAllUnviewedFeedsBySearch(searchValue)
            }else{
                feedLst = RssFeedDao().findAllUnviewedFeedsBySearch(null)
            }
            if (feedLst!=null && feedLst!!.isNotEmpty()){
                for (item in feedLst!!){
                    Log.v("CardSelected",item.toString())
                }
            }
//            if (feedLst!=null && feedLst!!.size>0){
//            if (adapterCard==null)
//                fillAndResetCardStack()
//            adapterCard!!.setAdapterType(false)
//            adapterCard!!.clear()
//            adapterCard!!.addAll(feedLst)
//            adapterCard!!.notifyDataSetChanged()
            refillCardStack(null,null)
            cardStackView?.visibility = View.VISIBLE
            cardStackView?.setCardEventListener(getCardSwipeListener())
            Log.v("StartCommand","Feed List is not null: "+feedLst!!.size)
//            }else{
//                Log.e("StartCommand","Feed List is null")
//            }
            rssDetails!!.visibility = View.VISIBLE
        }*/
    }

    fun setFriendCaption(caption:String?){
        friendPostCaption = caption
    }

    override fun onFeedButtonClickListener(img: Bitmap?, position: Int, feed: RssFeedModel, type: Int, comment: TextView, view:View) {
        val userName = UserDao().findCurrentUser()?.name
        when (type){
            ACTION_DISLIKE -> {
                Log.v("FeedButtonListener","Dislike Feed")
                var commentValue = comment.text.toString()
                if (commentValue.isNullOrEmpty()){
                    shareToFacebook = false
                    shareToFacebookSwiped = true
                    val txtDislike = getEmojiByUnicode(thumbDownUnicode)+" "+userName+" Dislikes this"
                    comment.setText(txtDislike)
                    if (view!=null){
                        val bitmap = viewToBitmap(view)
                        encodedImg = encodeToBase64(bitmap, Bitmap.CompressFormat.PNG,100)
                    }
                    object: CountDownTimer(2000,1000){
                        override fun onTick(millisUntilFinished: Long) = Unit

                        override fun onFinish(){
                            swipeLeft()
                        }
                    }.start()
                }else{
                    shareToFacebook = true
                    if (view!=null){
                        val bitmap = viewToBitmap(view)
                        encodedImg = encodeToBase64(bitmap, Bitmap.CompressFormat.PNG,100)
                    }
                    swipeLeft()
                }
            }
            ACTION_LIKE -> {

                Log.v("FeedButtonListener","Like Feed")
                var commentValue = comment.text.toString()
                if (commentValue.isNullOrEmpty()) {
                    shareToFacebook = false
                    val txtLike = getEmojiByUnicode(thumbUpUnicode)+" "+userName+" Likes this"
                    comment.setText(txtLike)
                    object: CountDownTimer(2000,1000){
                        override fun onTick(millisUntilFinished: Long) = Unit

                        override fun onFinish(){
                            swipeRight()
                        }
                    }.start()
                }else{
                    shareToFacebook = true
                    if (view!=null){
                        val bitmap = viewToBitmap(view)
                        encodedImg = encodeToBase64(bitmap, Bitmap.CompressFormat.PNG,100)
                    }
                    swipeRight()
                }
            }
            ACTION_REVERSE -> {
                shareToFacebook = true
                showMessageDialog2(null,feed,type,comment,view)
//                cardStackView.reverse()
//                Log.v("FeedButtonListener","Reverse Feed")
//                isReversed = true
            }
        }
    }

    fun viewToBitmap(view:View):Bitmap {
        var bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888)
        var canvas = Canvas(bitmap)
        view.draw(canvas)
        return bitmap
    }

    fun getCardSwipeListener():CardStackView.CardEventListener{
        val event = object: CardStackView.CardEventListener{
            override fun onCardDragging(percentX: Float, percentY: Float) {
                Log.v("CardState","X: "+percentX.toString()+", Y: "+percentY)
            }

            override fun onCardSwiped(direction: SwipeDirection?) {
//                if (!isReversed) {
//                    val position = cardStackView.topIndex - 1
                    val position = 0
                    swipedFeed = feedLst!![0]
                    val oldFeed = RssFeedDao().findRssFeedModelByMobileId(swipedFeed!!.idMobile)
                    val updatedFeed = RssFeedDao().findRssFeedModelByMobileId(swipedFeed!!.idMobile)
                    var showDialog = false
                    if (updatedFeed != null) {
                        val caption = updatedFeed.caption
                        Log.v("CardSwiped", "Position: " + position)
                        Log.v("CardSwiped", "Item: " + updatedFeed.toString())
//                        Log.v("CardSwiped", direction?.name)
//                        Log.v("CardSwiped", "Before Updating: " + updatedFeed.toString())
                        updatedFeed.viewed = true
                        updatedFeed.type = direction?.name!!
                        if (updatedFeed.isFriends) {
                            Log.e("CardSwiped", "IS FRIEND")
                            var auxFeed: RssFeedModel? = null
                            var status = ""
                            if (friendPostCaption.isNullOrEmpty()) {
//                                Log.e("CardSwiped", "IF AUX VALUE: " + auxFeed.toString())
//                                Log.e("CardSwiped", "IF UPDATED VALUE: " + updatedFeed.toString())
                                status = "Like"
//                                Log.e("ShowingDialog", "No Caption")
                                showDialog = true
//                                TableUtils.clearTable(DatabaseHelper.helper.connectionSource, PostImage::class.java)
                                val userName = UserDao().findCurrentUser()?.name
                                when (direction!!.name) {
                                    "Right" -> {
                                        val txtLike = thumbUpUnicodeString + " " + userName + " Likes this"
                                        val txtToast = getEmojiByUnicode(thumbUpUnicode) + " " + userName + " Likes this"
                                        auxFeed = updatedFeed.copy(caption = txtLike)

                                    }
                                    "Left" -> {
                                        val txtDislike = thumbDownUnicodeString + " " + userName + " Dislikes this"
                                        val txtToast = getEmojiByUnicode(thumbDownUnicode) + " " + userName + " Dislikes this"
                                        auxFeed = updatedFeed.copy(caption = txtDislike)
                                    }
                                }
//                                Log.e("CardSwiped", "AFTER CAPTION AUX VALUE: " + auxFeed.toString())
//                                Log.e("CardSwiped", "AFTER CAPTION UPDATED VALUE: " + auxFeed.toString())
                                if (shareToFacebook && !encodedImg.isNullOrEmpty()){
                                    PostPhotoFacebookService(this@Main2Activity).postPhoto(getFacebookToken()!!,auxFeed!!.title,encodedImg!!,auxFeed!!,true,"Reply")
                                }else{
                                    SavePostService(this@Main2Activity).savePost(getFacebookToken()!!, auxFeed!!, false, "Like")
                                }
                                encodedImg = ""
                            } else {
                                status = "Reply"
                                auxFeed = updatedFeed.copy(caption = friendPostCaption!!)
//                                val postImage = PostImagesDao().findPostImageByPostId(auxFeed.idMobile)
                                if (shareToFacebook){
                                    if (!encodedImg.isNullOrEmpty())
                                        showMessageDialog("Post feed to facebook?", encodedImg!!, auxFeed)
                                    else
                                        Log.e("PostFacebook","Encoded Image is null or empty")
                                }else{
                                    Log.e("PostFacebook","Share to facebook is false")
                                }

                            }
                            setFriendCaption(null)
                            val user = UserDao().findCurrentUser()
                            val reply = FeedReplies(
                                    -1,
                                    -1,
                                    auxFeed!!.caption,
                                    user!!.idFacebook!!,
                                    auxFeed.idServer,
                                    Date().time,
                                    direction.name,
                                    status)
                            FeedRepliesDao().updateOrCreate(reply)
                        } else {
                            Log.e("CardSwiped", "IS MINE")
                            if (caption.isNullOrEmpty()) {
                                Log.e("ShowingDialog", "No Caption")
                                showDialog = true
//                                TableUtils.clearTable(DatabaseHelper.helper.connectionSource, PostImage::class.java)
                                val userName = UserDao().findCurrentUser()?.name
                                when (direction!!.name) {
                                    "Right" -> {
                                        val txtLike = thumbUpUnicodeString + " " + userName + " Likes this"
                                        val txtToast = getEmojiByUnicode(thumbUpUnicode) + " " + userName + " Likes this"
                                        updatedFeed.caption = txtLike

                                    }
                                    "Left" -> {
                                        val txtDislike = thumbDownUnicodeString + " " + userName + " Dislikes this"
                                        val txtToast = getEmojiByUnicode(thumbDownUnicode) + " " + userName + " Dislikes this"
                                        updatedFeed.caption = txtDislike
                                    }
                                }
                                if (shareToFacebook)
                                    Log.e("CardSwiped", "Share to Facebook")
                                if (!encodedImg.isNullOrEmpty())
                                    Log.e("CardSwiped", "Feed Image Exist")

                                if (shareToFacebook && !encodedImg.isNullOrEmpty()){
                                    PostPhotoFacebookService(this@Main2Activity).postPhoto(getFacebookToken()!!,updatedFeed.title,encodedImg!!,updatedFeed,true,"Reply")
                                }else if(shareToFacebookSwiped && !encodedImg.isNullOrEmpty()){
                                    shareToFacebookSwiped = false
                                    showMessageDialog("Post feed to facebook?", encodedImg!!, updatedFeed)
                                }else{
                                    SavePostService(this@Main2Activity).savePost(getFacebookToken()!!, updatedFeed, false, "Like")
                                }

                                encodedImg = ""
                            } else {
//                                val postImage = PostImagesDao().findPostImageByPostId(updatedFeed.idMobile)
                                if (shareToFacebook){
                                    if (!encodedImg.isNullOrEmpty())
                                        showMessageDialog("Post feed to facebook?", encodedImg!!, updatedFeed)
                                    else
                                        Log.e("PostFacebook","Encoded Image is null or empty")
                                }else{
                                    Log.e("PostFacebook","Share to facebook is false")
                                }
                            }
                        }
                        val id = RssFeedDao().updateOrCreate(updatedFeed)
                        swipedFeed = updatedFeed
                    }
                    if (feedLst!!.size > 1) {
                        val nextFeed = feedLst!![1]
                        val nextIndex = feedLst!!.indexOf(nextFeed)
                        currentFeedPosition = nextFeed.idMobile
                        Log.v("CardSwiped", "Next Feed Index: " + nextIndex)
                        Log.v("CardSwiped", "Next Feed: " + nextFeed.toString())
                        updateIconsAdapter(updatedFeed, nextFeed.idMobile, false, showDialog,false)
                    } else {
                        currentFeedPosition = -1
                        Log.v("CardSwiped", "No next index")
                        updateIconsAdapter(updatedFeed, -1, true, showDialog,false)
                        var isSeach = false
                        if (!searchValue.isNullOrEmpty())
                            isSeach = true
                        refillCardStack(null, null, isSeach)

                    }
//                }else{
//                    isReversed = false
//                }
            }

            override fun onCardReversed() {
//                isReversed = true
//                Log.v("CardState","Card Reversed")
////                val view = adapterCard!!.currentViewHolder
//                val view = cardStackView!!.getChildAt(cardStackView!!.topIndex)
//                if (view!=null){
//                    Log.e("CardView","ViewHolder is not null")
//                    Log.e("CardView","Caption: "+checkUnicodeInString(swipedFeed!!.caption))
//                    try{
//                        val txtPostComment:TextView = view.findViewWithTag(swipedFeed!!.idMobile)
//                        txtPostComment.setText(checkUnicodeInString(swipedFeed!!.caption))
////                        view.txtPostComment!!.setText(checkUnicodeInString(swipedFeed!!.caption))
//                    }catch (e:Exception){
//                        Log.e("CardView","Exception: "+e.toString())
//                    }
//                }else{
//                    Log.e("CardView","ViewHolder is null")
//                }
//                object: CountDownTimer(2000,1000){
//                    override fun onTick(millisUntilFinished: Long) = Unit
//
//                    override fun onFinish(){
//                        when (swipedFeed!!.type){
//                            "Right"->swipeRight()
//                            "Left"->swipeLeft()
//                        }
//                    }
//                }.start()
            }

            override fun onCardMovedToOrigin() {
                Log.v("CardState","Card Moved to Origin")
            }

            override fun onCardClicked(index: Int) {
            }

        }
        return event
    }

    fun swipeLeft() {
//        List<TouristSpot> spots = extractRemainingTouristSpots();
//        if (spots.isEmpty()) {
//            return;
//        }

        val target = cardStackView.getTopView();

        val rotation = ObjectAnimator.ofPropertyValuesHolder(
                target, PropertyValuesHolder.ofFloat("rotation", -10f));
        rotation.setDuration(200);
        val translateX = ObjectAnimator.ofPropertyValuesHolder(
                target, PropertyValuesHolder.ofFloat("translationX", 0f, -2000f));
        val translateY = ObjectAnimator.ofPropertyValuesHolder(
                target, PropertyValuesHolder.ofFloat("translationY", 0f, 500f));
        translateX.setStartDelay(100);
        translateY.setStartDelay(100);
        translateX.setDuration(500);
        translateY.setDuration(500);
        val set = AnimatorSet();
        set.playTogether(rotation, translateX, translateY);

        cardStackView.swipe(SwipeDirection.Left, set);
    }

    fun swipeRight() {
//        List<TouristSpot> spots = extractRemainingTouristSpots();
//        if (spots.isEmpty()) {
//            return;
//        }

        val target = cardStackView.getTopView();

        val rotation = ObjectAnimator.ofPropertyValuesHolder(
                target, PropertyValuesHolder.ofFloat("rotation", 10f));
        rotation.setDuration(200);
        val translateX = ObjectAnimator.ofPropertyValuesHolder(
                target, PropertyValuesHolder.ofFloat("translationX", 0f, 2000f));
        val translateY = ObjectAnimator.ofPropertyValuesHolder(
                target, PropertyValuesHolder.ofFloat("translationY", 0f, 500f));
        translateX.setStartDelay(100);
        translateY.setStartDelay(100);
        translateX.setDuration(500);
        translateY.setDuration(500);
        val set = AnimatorSet();
        set.playTogether(rotation, translateX, translateY);

        cardStackView.swipe(SwipeDirection.Right, set);
    }


        fun checkUnicodeInString(text:String):String{
        var finalString = text
        when {
            text.contains(thumbUpUnicodeParsed) -> finalString = text.replace(thumbUpUnicodeParsed,getEmojiByUnicode(thumbUpUnicode))
            text.contains(thumbDownUnicodeParsed) -> finalString = text.replace(thumbDownUnicodeParsed,getEmojiByUnicode(thumbDownUnicode))
            text.contains(thumbUpUnicodeString) -> finalString = text.replace(thumbUpUnicodeString,getEmojiByUnicode(thumbUpUnicode))
            text.contains(thumbDownUnicodeString) -> finalString = text.replace(thumbDownUnicodeString,getEmojiByUnicode(thumbDownUnicode))
        }
        return finalString
    }

    override fun onShareButtonClickListener(img: Bitmap, position: Int, feed: RssFeedModel) {
        if (img!=null){
            val encoded = encodeToBase64(img, Bitmap.CompressFormat.PNG,100)
            Log.v("ImageBase64",encoded)
//            val url = saveImage(img)
//            val imgFile = File(url)
//            if (imgFile.exists()){
//                Log.v("ImageFile","Exist")
//            }
            showMessageDialog("Post feed to facebook?",encoded,feed)
        }else{
            Log.v("ImageBase64","Image is null")
        }
    }

    fun encodeToBase64(image: Bitmap, compressFormat: Bitmap.CompressFormat, quality: Int): String {
        val byteArrayOS = ByteArrayOutputStream()
        image.compress(compressFormat, quality, byteArrayOS)
        return android.util.Base64.encodeToString(byteArrayOS.toByteArray(), android.util.Base64.DEFAULT)
    }

    fun isDetailsOpen():Boolean{
        if (rssDetails!!.visibility == View.VISIBLE)
            return true
        return false
    }

    fun dismissDialog(){
        if (pDialog!=null && pDialog!!.isShowing){
            pDialog!!.dismiss()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.v("ActivityState","Destroyed")

    }

    fun reStartService(){
        Log.v("RssService","Re-starting Service")
        val service = Intent(this@Main2Activity, FloatingViewService::class.java)
        val token = AccessToken.getCurrentAccessToken()
        startService(service)
        finish()
    }

    fun killService(){
        Log.v("RssService","Killing Service")
        val service = Intent(this@Main2Activity, FloatingViewService::class.java)
        stopService(service)
    }

    fun checkFeedCounter(){
        Log.v("RequestCounter","Checking Counter")
        Log.v("RequestCounter","Counter: "+feedCounter.toString()+", Likes: "+(userLikes!!.size-1).toString())
        if (apiCallSearch){
            Log.v("ResetRecycler","is api call search")
            isServiceRunning = false
            manageLoadingLayout(false,"Success")
            hideProgressBar()
            if (adapter==null)
                fillAndResetRecycler(searchValue,-1)
            else
                updateIconsAdapter(null,-1,true,false, true)
            fillAndResetCardStack(null)
            refillCardStack(null,null,true)
        }else{
            Log.v("ResetRecycler","is regular api")
            if (feedCounter == userLikes!!.size-1){
                manageLoadingLayout(false,"Success")
                hideProgressBar()
                feedCounter = 1
                if (adapter==null)
                    fillAndResetRecycler(null,-1)
                else
                    updateIconsAdapter(null,-1,true,false,true)
                fillAndResetCardStack(null)
                var isSeach = false
                if (!searchValue.isNullOrEmpty())
                    isSeach = true
                refillCardStack(null,null,isSeach)
//                var valuesString = ""
                for (i in RssFeedDao().findAllUnviewedFeedsBySearchIconsAll(null)!!){
                    Log.v("DatabaseItems",i.toString())
                }
//                DatabaseHelper(this,"").sendDBString(valuesString)
            }else{
                feedCounter++
            }
        }
    }

    fun requestNewRss(keyword:String?){
        Log.v("NewRequestRss","Requesting RSS")
        if (!keyword.isNullOrEmpty()){
            showProgressBar()
            searchValue = keyword!!
            Log.v("ResetRecycler","SEARCH REQUEST: "+searchValue)
            currentFeedCall = GetRSSService(this).getRSS(searchValue)
            isServiceRunning = true
//            if (adapter==null)
//                fillAndResetRecycler(searchValue,-1)
//            else
//                updateIconsAdapter(null,-1,true,false)
//            fillAndResetCardStack(null)
//            var isSeach = true
//            refillCardStack(null,null,isSeach)

//                    dismissProgressDialog()
//            requestMyPosts()
        }else{
            apiCallSearch = false
            if (userLikes!=null && !userLikes!!.isEmpty()){
                isServiceRunning = true
                Log.v("NewRequestRss","First Position, Starting Request")
                for (i in 0..(userLikes!!.size-1)){
                    Log.v("NewRequestRss","Request rss: "+userLikes!![i])
                    currentFeedCall = GetRSSService(this).getRSS(userLikes!![i])
                    if (i == (userLikes!!.size-1)){
                        Log.v("NewRequestRss","Last Position, Finishing Request")
                        isServiceRunning = false
                        if (currentPage == SECTION_HOME){
                            if (adapter==null)
                                fillAndResetRecycler(null,-1)
                            else
                                updateIconsAdapter(null,-1,true,false,false)
                            fillAndResetCardStack(null)
                            var isSeach = false
                            if (!searchValue.isNullOrEmpty())
                                isSeach = true
                            refillCardStack(null,null,isSeach)
//                    dismissProgressDialog()
                        }
                        requestMyPosts()
                    }
                }
//            if (currentPosition < userLikes?.size!! && !userLikes!![currentPosition].equals("")){
//                isServiceRunning = true
//                Log.v("RequestRss","Request rss: "+userLikes!![currentPosition])
//                GetRSSService(this).getRSS(userLikes!![currentPosition])
////                GetBingNewsService(this).getRSS(userLikes!![currentPosition])
//                currentPosition++
//            }else{
//                if (currentRequestPosition == currentPosition){
//                    isServiceRunning = false
//                    fillAndResetRecycler(null,-1)
//                    fillAndResetCardStack(null)
//                    refillCardStack(null,null)
////                    dismissProgressDialog()
//                    hideProgressBar()
//                }
//                Log.v("RequestRss","Current Position is higher that list")
//            }
            }else{
                isServiceRunning = false
//            dismissProgressDialog()
                hideProgressBar()
                Log.v("NewRequestRss","User likes are empty")
            }
        }
    }

    fun startAsyncLoadFeed(items: List<RssItem>?, keyword: String, context:Context){
        AsyncLoadFeed(items,keyword,context).execute("")
    }

    fun startAsyncLoadFriendFeed(items: List<Posts>?, context:Context,isFriends:Boolean){
        AsyncLoadFriendFeed(items,context,isFriends).execute("")
    }

    fun startAsyncLoadFriendReplies(items: List<Replies>?, context: Context){
        if ((feedLst==null || feedLst!!.isEmpty()) && currentPage == SECTION_HOME){
            fillAndResetRecycler(null,-1)
            fillAndResetCardStack(null)
            var isSeach = false
            if (!searchValue.isNullOrEmpty())
                isSeach = true
            refillCardStack(null,null,isSeach)
        }
        if (items!=null && items.isNotEmpty())
            AsyncLoadFriendReplies(items,context).execute("")
        else
            hideProgressBar()
    }

    override fun onPause() {
        super.onPause()
        Log.v("ActivityState","Paused")
        reStartService()
    }

    private fun doSearch(){
        var user = UserDao().findCurrentUser()
        if (user!=null){
            var likes = user.likes?.split(",")
            if (likes!=null && likes.isNotEmpty()){
                userLikes = likes
                Log.v("ConsoleLog","Likes Size: "+userLikes?.size)
//                requestTweetNews()
//                showProgressDialog("Loading feeds")
                showProgressBar()
                manageLoadingLayout(true,"Loading Feeds")
//                requestNewRss()
                getUsersFriends(user.idFacebook!!)
            }else{
                Log.v("UserLikes","Empty")
            }
        }
    }

    fun showProgressBar(){
        progressBar.visibility = View.VISIBLE
    }

    fun hideProgressBar(){
        if (!isServiceRunning)
            progressBar.visibility = View.GONE
    }

    fun showProgressDialog(msg:String?){
        if (msg!=null){
            pDialog = SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
            pDialog?.getProgressHelper()?.setBarColor(R.color.drewpicColor)
            pDialog?.setTitleText(msg)
            pDialog?.setCancelable(false)
            pDialog?.show()
        }
    }

    fun dismissProgressDialog(){
        if (pDialog!=null && pDialog!!.isShowing){
            pDialog!!.dismiss()
        }
    }

    fun getEmojiByUnicode(unicode: Int): String {
        return String(Character.toChars(unicode))
    }

    fun getUsersFriends(userId:String){
        var request = GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/"+userId+"/friends",
                null,
                HttpMethod.GET,
                object: GraphRequest.Callback {
                    override fun onCompleted(response: GraphResponse?) {
                        if (response!=null){
                            try{
                                Log.v("FriendsUserObjects",response.rawResponse.toString())
                                var userFriends = Gson().fromJson(response.rawResponse, GetUserFriends::class.java)
                                if (userFriends!=null){
                                    if (userFriends.data!=null && userFriends.data!!.isNotEmpty()){
                                        userFriendsLst = ArrayList<UserFriends>()
                                        for (i in userFriends.data!!){
                                            var profilePicture = "https://graph.facebook.com/"+i.id+"/picture?type=large"
                                            var friend = UserFriendsDao().getFriendByFacebookId(i.id)
                                            if (friend==null){
                                                friend = UserFriends(-1,i.email,i.name,i.gender,profilePicture,i.id)
                                            }
                                            userFriendsLst!!.add(friend)
                                            UserFriendsDao().updateOrCreate(friend)
                                            Log.v("FriendsUserItem",friend.toString())
                                        }
                                        requestFriendsPosts()
                                    }
                                }
                            }catch (e:Exception){
                                Log.e("FriendsUserObjects",e.toString())
                            }
                        }else{
                            userFriendsLst = UserFriendsDao().findAllCurrentFriends()
                            requestFriendsPosts()
                        }
//                        hideProgressBar()
                    }
                }
        )
        request.executeAsync()
    }

    fun requestFriendsPosts(){
        Log.v("RequestRss","Requesting RSS")
        if (userFriendsLst!=null && !userFriendsLst!!.isEmpty()){
//            isServiceRunning = true
//            for (i in 0..(userFriendsLst!!.size-1)){
//                Log.v("RequestRss","Request rss: "+userFriendsLst!![i].toString())
//                GetFriendsPostsService(this).getPosts(userFriendsLst!![i].idFacebook!!)
//                if (i == (userFriendsLst!!.size-1)){
//                    isServiceRunning = false
//                    //Request Rss
//                    currentPosition = 0
//                    currentRequestPosition = 0
//                    requestNewRss()
//                }
//            }
            if (currentPosition < userFriendsLst?.size!! && userFriendsLst!![currentPosition]!=null){
                isServiceRunning = true
                Log.v("RequestRss","Request rss: "+userFriendsLst!![currentPosition].toString())
                GetFriendsPostsService(this).getPosts(userFriendsLst!![currentPosition].idFacebook!!,true)
                currentPosition++
            }else{
                if (currentRequestPosition == currentPosition){
                    isServiceRunning = false
                    //Request Rss
                    currentPosition = 0
                    currentRequestPosition = 0
                    requestNewRss(null)
                }
                Log.v("RequestRss","Current Position is higher that list")
            }
        }else{
            isServiceRunning = false
            //Request Rss
            currentPosition = 0
            currentRequestPosition = 0
            requestNewRss(null)
            Log.v("RequestRss","User likes are empty")
        }
    }

    fun requestMyPosts(){
        Log.v("RequestRss","Requesting my posts")
        val user = UserDao().findCurrentUser()
        GetFriendsPostsService(this).getPosts(user!!.idFacebook!!,false)
    }

    fun showWebViewDialog(link:String){
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.webview_dialog)
        val lp = WindowManager.LayoutParams()
        val window = dialog.getWindow()
        lp.copyFrom(window.getAttributes())
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        window.setAttributes(lp)

        val progressBar:CircleProgressBar = dialog.findViewById(R.id.progressBar)
        progressBar.setColorSchemeResources(R.color.drewpicColor)
        val webView: WebView = dialog.findViewById(R.id.webView)
        webView.getSettings().javaScriptEnabled = true
        webView.webViewClient = object:WebViewClient(){
            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                progressBar.visibility = View.GONE
            }
        }
        webView.loadUrl(link)
        val btnClose: ImageButton = dialog.findViewById(R.id.btnClose)
        btnClose.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    fun showDialogCard(item:RssFeedModel){
        Log.v("ShowingDialog","Init")
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialog_feed_item)
        val lp = WindowManager.LayoutParams()
        val window = dialog.getWindow()
        lp.copyFrom(window.getAttributes())
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        window.setAttributes(lp)


        val countReached:TextView = dialog.findViewById(R.id.countReached)
        val countAgree:TextView = dialog.findViewById(R.id.countAgree)
        val countDisagree:TextView = dialog.findViewById(R.id.countDisagree)
        val feedContent:TextView = dialog.findViewById(R.id.feedContent)
        val rssTitle:TextView = dialog.findViewById(R.id.rssTitle)
        val feedImage:ImageView = dialog.findViewById(R.id.feedImage)
        val imgProfile: CircleImage = dialog.findViewById(R.id.imgProfile)
        val imgProfile2: CircleImage = dialog.findViewById(R.id.imgProfile2)
        val txtPostComment: EditText = dialog.findViewById(R.id.txtPostComment)
        val txtPostComment2: EditText = dialog.findViewById(R.id.txtPostComment2)
        val lblTitle:TextView = dialog.findViewById(R.id.lblTitle)
        val txtPostTime:TextView = dialog.findViewById(R.id.txtPostTime)
        val txtKeyword:TextView = dialog.findViewById(R.id.txtKeyword)
        val commentContainer2: RelativeLayout = dialog.findViewById(R.id.commentContainer2)

        val stringTime = DateManage().parseMillistoTimeString(item.publishDate)
        txtPostTime.setText(stringTime+"h")

        lblTitle.setText(item.title.replace("&#39;","'"))

        val descriptionLength = item.description.length

        when {
            descriptionLength<=50 -> feedContent.setTextSize(TypedValue.COMPLEX_UNIT_SP,30f)
            descriptionLength<=100 -> feedContent.setTextSize(TypedValue.COMPLEX_UNIT_SP,22f)
//            descriptionLength<=200 -> feedContent.setTextSize(TypedValue.COMPLEX_UNIT_SP,20f)
            else -> feedContent.setTextSize(TypedValue.COMPLEX_UNIT_SP,15f)
        }

        val description = item.description.replace("&#39;","'")
        feedContent.setText(description)
        Log.e("FeedContent",description)
//        rssTitle?.setText(item.title.replace("&apos;", "'"))
        if (!searchValue.isNullOrEmpty()){
            rssTitle.setText("Search results for")
        }else{
            rssTitle.setText("Today's top stories in")
        }

        txtKeyword.setText("'"+item.keyword+"'")

        Log.e("ShowingDialog","Keyword: "+item.keyword+", Image: "+item.image)
        try{
            Glide.with(this).load(item.image).into(feedImage)
        }catch (e:Exception){
            Log.e("ShowingDialog",e.toString())
        }

        val likes = FeedRepliesDao().getLikesCountByPost(item.idServer)
        val dislikes = FeedRepliesDao().getDislikesCountByPost(item.idServer)

//        countReached?.setText(UserFriendsDao().getFriendsCount().toString())
//        countAgree?.setText(likes.toString())
//        countDisagree?.setText(dislikes.toString())

        val random = RandomValues()
        val totalReached = random.reached+UserFriendsDao().getFriendsCount()
        val totalAgree = random.agree+likes
        val totalDisagree = random.disagree+dislikes


        countReached.setText(totalReached.toString()+" People reached")
        countAgree.setText(totalAgree.toString())
        countDisagree.setText(totalDisagree.toString())
        txtPostComment.setTag(item.idMobile)

        if (item.isFriends){
            Log.v("ShowingDialog","Item is friend, no reply value available")
            commentContainer2.visibility = View.VISIBLE
            txtPostComment.isEnabled = false

            val user = UserFriendsDao().getFriendByFacebookId(item.friendsId)
            try{
                Log.e("ShowingDialog",user?.profilePicture?.replace("large","small"))
                Glide.with(this).load(user?.profilePicture?.replace("large","small")).into(imgProfile)
            }catch (e:Exception){
                Log.e("ShowingDialog",e.toString())
            }

            val myUser = UserDao().findCurrentUser()
            try{
                Log.e("ShowingDialog",myUser?.profilePicture?.replace("large","small"))
                Glide.with(this).load(myUser?.profilePicture?.replace("large","small")).into(imgProfile2)
            }catch (e:Exception){
                Log.e("ShowingDialog",e.toString())
            }

            var finalCaption = checkUnicodeInString(item.caption)
            var finalCaption2 = ""
            val caption2 = FeedRepliesDao().findMyReplyByPostId(item.idServer)
            if (caption2!=null)
                finalCaption2 = checkUnicodeInString(caption2.caption)

            txtPostComment.setText(finalCaption)
            txtPostComment2.setText(finalCaption2)

        }else{

            val user = UserDao().findCurrentUser()
            try{
                Log.e("ShowingDialog",user?.profilePicture?.replace("large","small"))
                Glide.with(this).load(user?.profilePicture?.replace("large","small")).into(imgProfile)
            }catch (e:Exception){
                Log.e("ShowingDialog",e.toString())
            }

            if (currentReply!=null){
                Log.v("ShowingDialog","Reply value is not null")
                val friend = UserFriendsDao().getFriendByFacebookId(currentReply!!.friendFacebookId)


                commentContainer2.visibility = View.VISIBLE

                Log.v("ShowingDialog","Caption 1: "+item.caption)
                val finalCaption1 = checkUnicodeInString(item.caption)

                Log.v("ShowingDialog","Caption 2: "+currentReply!!.caption)
                val finalCaption2 = checkUnicodeInString(currentReply!!.caption)

                Log.v("ShowingDialog","Final Caption 1: "+finalCaption1)
                Log.v("ShowingDialog","Final Caption 2: "+finalCaption2)
                txtPostComment.setText(finalCaption1)
                txtPostComment2.setText(finalCaption2)
                txtPostComment.isEnabled = false
                txtPostComment2.isEnabled = false

                try{
                    Log.e("ShowingDialog",friend?.profilePicture?.replace("large","small"))
                    Glide.with(this).load(friend?.profilePicture?.replace("large","small")).into(imgProfile2)
                }catch (e:Exception){
                    Log.e("ShowingDialog",e.toString())
                }
            }else{
                Log.v("ShowingDialog","Caption 1: "+item.caption)
                val finalCaption = checkUnicodeInString(item.caption)
                txtPostComment.setText(finalCaption)
                txtPostComment.isEnabled = false
            }
        }
        Log.v("ShowingDialog","Showing")
        dialog.show()
        Log.v("ShowingDialog","CountingDown")
        object: CountDownTimer(2000,1000){
            override fun onTick(millisUntilFinished: Long){
                Log.v("ShowingDialog","Left: "+millisUntilFinished.toInt())
            }

            override fun onFinish(){
                Log.v("ShowingDialog","Dismissing dialog")
                dialog.dismiss()
            }
        }.start()
    }

    @SuppressLint("ResourceAsColor")
    fun showTutorial(){
        var dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow().setBackgroundDrawable(ColorDrawable(R.color.dialogBgColor))
        dialog.setContentView(R.layout.tutorial_five)
        var lp = WindowManager.LayoutParams()
        var window = dialog.getWindow()
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        window.setAttributes(lp)

        val bubbleContainer: ConstraintLayout = dialog!!.findViewById(R.id.bubbleContainer)
        val bubbleLayout: BubbleLayout = dialog!!.findViewById(R.id.bubbleLayout)
        val txtBubble: TextView = dialog!!.findViewById(R.id.txtBubble)

        val settings = SettingsDao().getSettings()
        if (settings?.showDislike!!){
            bubbleContainer?.visibility = View.VISIBLE
            txtBubble?.setText("Click here to dislike")
            bubbleLayout?.arrowPosition = 100f
            bubbleContainer!!.setOnClickListener {
                settings?.showDislike = false
                SettingsDao().updateOrCreate(settings)
                if (settings?.showLike!!){
                    bubbleLayout?.arrowPosition = 400f
                    txtBubble?.setText("Click here to like")
                    bubbleContainer!!.setOnClickListener {
                        settings?.showLike = false
                        SettingsDao().updateOrCreate(settings)
                        dialog.dismiss()
                    }
                }else{
                    dialog.dismiss()
                }
            }
        }else if(settings?.showLike){
            bubbleContainer?.visibility = View.VISIBLE
            bubbleLayout?.arrowPosition = 400f
            txtBubble?.setText("Click here to like")
            bubbleContainer!!.setOnClickListener {
                settings?.showLike = false
                SettingsDao().updateOrCreate(settings)
                dialog.dismiss()
            }
        }

        dialog.setCancelable(false)
        dialog.show()
    }
}
