package com.aarr.drewpic.Views

import android.app.Dialog
import android.app.SearchManager
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.aarr.drewpic.Adapters.BlacklistAdapter
import com.aarr.drewpic.Database.Dao.BlackListFilterDao
import com.aarr.drewpic.Database.Model.BlacklistFilter

import com.aarr.drewpic.R
import com.jaredrummler.materialspinner.MaterialSpinner
import java.util.*

class UrlFilter : AppCompatActivity() {

    var mSpinner: MaterialSpinner? = null
    var recycler: RecyclerView? = null
    var toolbar: Toolbar? = null
    val SEARCH_ENGINES: Int? = 0
    val VIDEO_ENGINES: Int? = 1
    val OTHERS: Int? = 2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_url_filter)

        mSpinner = findViewById<MaterialSpinner>(R.id.materialSpinner) as MaterialSpinner
        mSpinner!!.setItems("Search engines", "Video engines","Others")
        mSpinner!!.setOnItemSelectedListener(MaterialSpinner.OnItemSelectedListener<String> {
            view, position, id, item ->
            Snackbar.make(view, item, Snackbar.LENGTH_LONG).show()

            fillAndResetRecycler(null)
        })
        recycler = findViewById<RecyclerView>(R.id.recycler) as RecyclerView
        toolbar = findViewById<Toolbar>(R.id.toolbar) as Toolbar
        toolbar!!.setTitle("Blacklist Filter")
        setSupportActionBar(toolbar)
        toolbar!!.setNavigationOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                onBackPressed()
            }
        })
        fillAndResetRecycler(null)
    }



    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        menuInflater.inflate(R.menu.url_filter, menu)
        getMenuInflater().inflate(R.menu.url_filter, menu)
        if (menu==null){
            Log.v("NULLVALIDATION","MENU IS NULL")
        }
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        if (searchManager==null){
            Log.v("NULLVALIDATION","SEARCH MANAGER IS NULL")
        }
        if (menu!!.findItem(R.id.search).actionView==null){
            Log.v("NULLVALIDATION","ACTION VIEW IS NULL")
        }
        val searchView = menu!!.findItem(R.id.search).actionView as android.widget.SearchView
        if (searchView==null){
            Log.v("NULLVALIDATION","SEARCH VIEW IS NULL")
        }

        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.queryHint = "Search by url"

        searchView.setOnCloseListener(object: android.widget.SearchView.OnCloseListener {
            override fun onClose(): Boolean {
                fillAndResetRecycler(null)
                return true
            }
        })

        searchView.setOnQueryTextListener(object : android.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }

            override fun onQueryTextSubmit(s: String): Boolean {
                fillAndResetRecycler(s)
                return true
            }
        })
        return true
    }

    fun fillAndResetRecycler(query:String?){
        val type:Int = mSpinner!!.selectedIndex
        var lstFilters:List<BlacklistFilter>? = BlackListFilterDao().findBlacklistByUrl(query,type)

        recycler!!.adapter = null
        var adapter:BlacklistAdapter? = BlacklistAdapter(type,lstFilters,this)
        recycler!!.layoutManager = LinearLayoutManager(this)
        recycler!!.adapter = adapter
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val type:Int = mSpinner!!.selectedIndex
        if (item!!.itemId == R.id.addUrl){
            val dialog = Dialog(this)
            dialog.setContentView(R.layout.custom_input_text)

            val input = dialog.findViewById<EditText>(R.id.inputField) as EditText
            val btnSave = dialog.findViewById<Button>(R.id.btnSave) as Button
            btnSave.setOnClickListener {
                dialog.dismiss()
                var url = input.text.toString()
                if (url!=null && !url.equals("")){
                    var lstFilters:List<BlacklistFilter>? = BlackListFilterDao().findBlacklistByUrl(url,type)
                    if (lstFilters!=null && lstFilters.size>0){
                        Toast.makeText(this,"This url is it already blacklisted",Toast.LENGTH_SHORT).show()
                    }else{
                        try {
                            url = url.replace("www.","")
                        }catch(e:Exception){
                            Log.w("ReplaceWWW",e.toString())
                        }
                        var newFilter: BlacklistFilter? = BlacklistFilter()
                        newFilter!!.url = getCleanUrl(url)
                        newFilter!!.createdDate = Date()
                        newFilter!!.isActive = true
                        newFilter!!.type = type
                        BlacklistFilter.getRepository().create(newFilter)

                        fillAndResetRecycler(null)
                    }
                }
            }
            val btnClose = dialog.findViewById<Button>(R.id.btnClose) as Button
            btnClose.setOnClickListener { dialog.dismiss() }
            dialog.show()
        }
        return super.onOptionsItemSelected(item)
    }

    fun getCleanUrl(url: String): String {
        var url = url
        url = url.replaceFirst("^(http://www\\.|http://|www\\.https://www\\.|https://|www\\.http://m\\.|http://|m\\.https://m\\.|https://|m\\.www\\.)".toRegex(), "")
        return url
    }
}
