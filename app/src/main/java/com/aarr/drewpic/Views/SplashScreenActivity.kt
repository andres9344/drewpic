package com.aarr.drewpic.Views

import android.Manifest
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import com.aarr.drewpic.Database.Dao.UserDao

import com.aarr.drewpic.R
import com.aarr.drewpic.Services.FloatingViewService
import com.facebook.FacebookSdk
import com.facebook.login.LoginManager
import android.content.Context.ACTIVITY_SERVICE
import android.app.ActivityManager
import android.content.Context
import android.util.Log
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.TedPermission
import java.util.ArrayList


class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        FacebookSdk.sdkInitialize(getApplicationContext())
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        killService()

//        exitSplash()
        checkPermission()
    }

    fun checkPermission(){
        val permissionListener = object:PermissionListener{
            override fun onPermissionGranted() {
                Log.v("Permissions","Granted")
                exitSplash()
            }

            override fun onPermissionDenied(p0: ArrayList<String>?) {
                finish()
                Log.v("Permissions","Denied")
            }

        }

        TedPermission.with(this)
                .setPermissionListener(permissionListener)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.SYSTEM_ALERT_WINDOW,
                        Manifest.permission.WAKE_LOCK,
                        Manifest.permission.INTERNET)
                .check()
    }

    fun killService(){
        Log.v("RssService","Killing Service")
        val service = Intent(this@SplashScreenActivity, FloatingViewService::class.java)
        stopService(service)
    }

    fun exitSplash(){
//        object: CountDownTimer(2000,1000){
//            override fun onTick(millisUntilFinished: Long) = Unit
//
//            override fun onFinish() = if (UserDao().findCurrentUser()!=null){
//                val intent = Intent(this@SplashScreenActivity, Main2Activity::class.java)
//                startActivity(intent)
//                finish()
//            }else{
//                var intent = Intent(this@SplashScreenActivity,SignupActivity::class.java)
//                startActivity(intent)
//                finish()
//            }
//        }.start()
        if (UserDao().findCurrentUser()!=null){
            val intent = Intent(this@SplashScreenActivity, Main2Activity::class.java)
            startActivity(intent)
            finish()
        }else{
            var intent = Intent(this@SplashScreenActivity,SignupActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun isMyServiceRunning(serviceClass: Class<*>): Boolean {
        val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }
}
