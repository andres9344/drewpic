package com.aarr.drewpic.Views

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.aarr.drewpic.R
import kotlinx.android.synthetic.main.activity_post.*
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import android.util.Log
import com.facebook.share.model.SharePhoto
import com.facebook.share.model.SharePhotoContent
import com.facebook.share.widget.ShareDialog
import android.content.Intent
import android.widget.Toast
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.share.Sharer
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import java.util.*


class PostActivity : AppCompatActivity() {

    var callbackManager: CallbackManager? = null
    private var manager: LoginManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post)

        callbackManager = CallbackManager.Factory.create();

        val i = intent
        val imgUrl = i.getStringExtra("ImgUrl")
        var bm: Bitmap? = null
        if (!imgUrl.isNullOrBlank()){
            bm = getBitmapFromFile(imgUrl)
        }

        btnPost.setOnClickListener {
            if (bm!=null){
                if (AccessToken.getCurrentAccessToken().permissions.isNotEmpty()){
                    for (i in AccessToken.getCurrentAccessToken().permissions){
                        Log.e("FacebookPermission",i)
                    }
                }
                if (!AccessToken.getCurrentAccessToken().permissions.contains("publish_actions")){
                    requestPublishPermission(bm!!)
                }else{
                    postImgFacebook(bm!!)
                }
            }else{
                Log.e("BitmapValue","Bitmap is null")
            }
        }
    }

    fun requestPublishPermission(img:Bitmap){
        val permissionNeeds = Arrays.asList("publish_actions")
        manager = LoginManager.getInstance()
        manager?.logInWithPublishPermissions(this, permissionNeeds);
        manager?.registerCallback(callbackManager, object: FacebookCallback<LoginResult> {
            override fun onError(error: FacebookException?) {
                Toast.makeText(this@PostActivity, "Facebook login error: " + error?.message, Toast.LENGTH_SHORT).show();
            }

            override fun onSuccess(result: LoginResult?) {
//                Toast.makeText(this@PostActivity, "Facebook login success!", Toast.LENGTH_SHORT).show();
                postImgFacebook(img)
            }

            override fun onCancel() {
                Toast.makeText(this@PostActivity, "Facebook login canceled!", Toast.LENGTH_SHORT).show();
            }
        })
    }

    fun postImgFacebook(img:Bitmap){
        val sharePhoto = SharePhoto.Builder()
                .setBitmap(img)
                .setCaption("My First photo upload")
                .build()

        val sharePhotoContent = SharePhotoContent.Builder()
                .addPhoto(sharePhoto)
                .build()

        val shareDialog = ShareDialog(this)
        shareDialog.registerCallback(callbackManager, object: FacebookCallback<Sharer.Result>{
            override fun onCancel() {
                Log.e("FacebookPost","Cancel")
            }

            override fun onError(error: FacebookException?) {
                Log.e("FacebookPost","Error: "+error.toString())
            }

            override fun onSuccess(result: Sharer.Result?) {
                Log.e("FacebookPost","Success")
                Toast.makeText(this@PostActivity, "Feed posted succesfully!", Toast.LENGTH_SHORT).show();
                finish()
            }

        })

        if (shareDialog.canShow(sharePhotoContent)){
            shareDialog.show(sharePhotoContent)
        }
    }

    fun getBitmapFromFile(url:String):Bitmap{
        val options = BitmapFactory.Options()
        options.inPreferredConfig = Bitmap.Config.ARGB_8888
        val bitmap = BitmapFactory.decodeFile(url, options)
        imgPost.setImageBitmap(bitmap)
        return bitmap
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager?.onActivityResult(requestCode, resultCode, data)
    }
}
