package com.aarr.drewpic.Views

import android.annotation.SuppressLint
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import android.view.View
import android.webkit.*
import com.aarr.drewpic.Database.Dao.RssFeedDao
import com.aarr.drewpic.Database.Dao.UserDao
import com.aarr.drewpic.Database.Helper.DatabaseHelper
import com.aarr.drewpic.Database.Model.RssFeedModel
import com.aarr.drewpic.Database.Model.User

import com.aarr.drewpic.R
import com.aarr.drewpic.Services.*
import com.aarr.drewpic.Services.Response.GetBingNews.Value
import com.aarr.drewpic.Services.Response.QwantResponse.News.Item
import com.aarr.drewpic.Services.Response.RSS.RssItemModel
import com.aarr.drewpic.Services.Response.TweetsNews.GetTwitterNews
import com.aarr.drewpic.Services.Response.TweetsNews.Tweet
import com.aarr.drewpic.Util.DateManage
import com.facebook.AccessToken
import com.facebook.FacebookSdk
import com.facebook.internal.LockOnGetVariable
import com.facebook.login.LoginManager
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.j256.ormlite.table.TableUtils
import kotlinx.android.synthetic.main.activity_profile.*
import me.toptas.rssconverter.RssItem
import org.json.JSONObject
import java.util.regex.Pattern

class ProfileActivity : AppCompatActivity() {

    private var userLikes: List<String> ? = null
    private var feedResults: List<Value> ? = null
    private var newsResults: List<Item> ? = null
    private var currentPosition = 0
    private var currentFeedPosition = 0
    private var currentKeyword = ""
    private var feedLst: MutableList<RssFeedModel>? = ArrayList<RssFeedModel>()
    private var feedIconLst: MutableList<RssFeedModel>? = ArrayList<RssFeedModel>()
    private var firstSearch = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        firstSearch = true


//        killService()
//        reStartService()
//        loadWebView()
        doSearch()

        btnFacebook.setOnClickListener(object: View.OnClickListener{
            override fun onClick(p0: View?) {
                TableUtils.clearTable(DatabaseHelper.helper.connectionSource,User::class.java)
                LoginManager.getInstance().logOut()
                finish()
                startActivity(Intent(this@ProfileActivity,SignupActivity::class.java))
            }
        })
    }

    private fun doSearch(){
        var user = UserDao().findCurrentUser()
        if (user!=null){
            var likes = user.likes?.split(",")
            if (likes!=null && likes.isNotEmpty()){
                userLikes = likes
                Log.v("ConsoleLog","Likes Size: "+userLikes?.size)
//                requestTweetNews()
                requestNewRss()
            }else{
                Log.v("UserLikes","Empty")
            }
        }
    }

    override fun onResume() {
        super.onResume()
        val token = AccessToken.getCurrentAccessToken()
        if (token!=null){
            Log.e("FacebookToken",token.token)
        }
    }

    private fun requestNewRss(){
        Log.v("RequestRss","Requesting RSS")
        if (userLikes!=null && !userLikes!!.isEmpty()){
            if (currentPosition < userLikes?.size!! && !userLikes!![currentPosition].equals("")){
                Log.v("RequestRss","Request rss: "+userLikes!![currentPosition])
                GetRSSService(this).getRSS(userLikes!![currentPosition])
//                GetBingNewsService(this).getRSS(userLikes!![currentPosition])
                currentPosition++
            }   else{
                killService()
                reStartService()
                Log.v("RequestRss","Current Position is higher that list")
            }
        }else{
            Log.v("RequestRss","User likes are empty")
        }
    }

    private fun requestTweetNews(){
        Log.v("RequestRss","Requesting RSS")
        if (userLikes!=null && !userLikes!!.isEmpty()){
            if (currentPosition < userLikes?.size!! && !userLikes!![currentPosition].equals("")){
                Log.v("RequestRss","Request rss: "+userLikes!![currentPosition])
                currentKeyword = userLikes!![currentPosition]
                reloadWebView(userLikes!![currentPosition])
                currentPosition++
            }else{
                killService()
                reStartService()
                Log.v("RequestRss","Current Position is higher that list")
            }
        }else{
            Log.v("RequestRss","User likes are empty")
        }
    }

    private fun requestNews(){
        Log.v("RequestRss","Requesting RSS")
        if (userLikes!=null && !userLikes!!.isEmpty()){
            if (currentPosition < userLikes?.size!! && !userLikes!![currentPosition].equals("")){
                Log.v("RequestRss","Request rss: "+userLikes!![currentPosition])
//                GetRSSService(this).getRSS(userLikes!![currentPosition])
                GetQwantNewsService(this).getNews(userLikes!![currentPosition])
                currentPosition++
            }   else{
                killService()
                reStartService()
                Log.v("RequestRss","Current Position is higher that list")
            }
        }else{
            Log.v("RequestRss","User likes are empty")
        }
    }

    fun loadRssFeed(items: List<RssItem>?, keyword: String){
        if (items!=null && items.isNotEmpty()){
            for (item in items){
                Log.v("AddItem","Adding: "+keyword)
                Log.v("AddItemValue","Value: "+item.toString())
                var feed : RssFeedModel? = RssFeedDao().findRssFeedModelByTitle(item.title)
                if (feed==null){
                    var actualImg = ""
                    if (item.image!=null){
                        actualImg = item.image

                        var description = item.description.replace("...","")
                        var lastDot = item.description.lastIndexOf(".",0,false)
                        Log.v("ItemDescription","Description: "+description)
                        Log.v("ItemDescription","Length: "+description.length)
//                    Log.v("ItemDescription","Last dot: "+lastDot)

//                    Log.v("ItemDescription","Last dot Index: "+description.get(lastDot-1))
//
//                    Log.v("ItemDescription","TimeMillis: "+DateManage().parseDateFeedToMillis(item.publishDate))

                        var rssIconItem = RssFeedModel(
                                -1,
                                -1,
                                keyword,
                                item.title,
                                item.link,
                                actualImg,
                                DateManage().parseDateFeedToMillis(item.publishDate),
                                actualImg,
                                removeHtmlTags(item.description),
                                false,
                                "",
                                "",
                                0,
                                0,
                                false,
                                "",
                                false,
                                false)

//                var rssIconItem = RssItemModel(
//                        keyword,
//                        item.name,
//                        item.url,
//                        actualImg,
//                        item.datePublished,
//                        actualImg,
//                        item.description)

                        feedIconLst?.add(rssIconItem)
                        feedLst?.add(rssIconItem)

                        Log.v("ItemDescription","InsertingData: "+rssIconItem.toString())

                        RssFeedDao().updateOrCreate(rssIconItem)
                    }else{
                        Log.v("AddItem","Item Image is null")
                    }
                }
//            getBingImages(rssIconItem)
                requestNewRss()
            }
        }else{
            requestNewRss()
            Log.v("AddItem","Item is null")
        }
    }

    fun loadRssFeed(item: Value?, keyword: String){
        if (item!=null){
            Log.v("AddItem","Adding: "+keyword)
            Log.v("AddItemValue","Value: "+item.toString())
            var feed : RssFeedModel? = RssFeedDao().findRssFeedModelByTitle(item.name)
            if (feed!=null){
                var actualImg = ""
                if (item.image!=null)
                    actualImg = item.image.thumbnail.contentUrl

                var description = item.description.replace("...","")
                var lastDot = item.description.lastIndexOf(".",0,false)
                Log.v("ItemDescription","Description: "+description)
//                Log.v("ItemDescription","Last dot: "+lastDot)
//                Log.v("ItemDescription","Length: "+description.length)
//                Log.v("ItemDescription","Last dot Index: "+description.get(lastDot-1))
//
//                Log.v("ItemDescription","TimeMillis: "+DateManage().parseDateFeedToMillis(item.datePublished))

                var rssIconItem = RssFeedModel(
                        -1,
                        -1,
                        keyword,
                        item.name,
                        item.url,
                        actualImg,
                        DateManage().parseDateFeedToMillis(item.datePublished),
                        actualImg,
                        item.description,
                        false,
                        "",
                        "",
                        0,
                        0,
                        false,
                        "",
                        false,
                        false)

//                var rssIconItem = RssItemModel(
//                        keyword,
//                        item.name,
//                        item.url,
//                        actualImg,
//                        item.datePublished,
//                        actualImg,
//                        item.description)

                feedIconLst?.add(rssIconItem)
                feedLst?.add(rssIconItem)

                Log.v("ItemDescription","InsertingData: "+rssIconItem.toString())

                RssFeedDao().updateOrCreate(rssIconItem)
            }
//            getBingImages(rssIconItem)
            requestNewRss()
        }else{
            requestNewRss()
            Log.v("AddItem","Item is null")
        }
    }

//    fun loadRssFeed(item: List<Value>?, keyword: String){
//        if (item!=null && item.isNotEmpty()){
//            currentFeedPosition = 0
//            feedResults = item
//            currentKeyword = keyword
//            runResultsList()
//        }else{
//            requestNewRss()
//            Log.v("AddItem","Item is null")
//        }
//    }

    fun loadNewsFeed(item: List<Item>?, keyword: String){
        if (item!=null && item.isNotEmpty()){
            currentFeedPosition = 0
            newsResults = item
            currentKeyword = keyword
            runNewsResultsList()
        }else{
            requestNews()
            Log.v("AddItem","Item is null")
        }
    }

    fun loadTweetNewsFeed(item: List<Tweet>?, keyword: String){
        if (item!=null && item.isNotEmpty()){
            currentFeedPosition = 0
            currentKeyword = keyword
            for (i in item){
                Log.v("RequestRss", "Request rss: " + i)
                var feed: RssFeedModel? = RssFeedDao().findRssFeedModelByDescription(removeHtmlTags(i.description!!))
                if (feed == null) {
                    Log.v("AddItem", "Adding: " + currentKeyword)
                    var thumbImg = ""
                    var actualImg = ""
                    if (!i.image.isNullOrEmpty()){
                        thumbImg = i.image!!
                        actualImg = i.image!!
                    }


//                    val description = i.description.replace("...", "")
//                    val lastDot = i.description.lastIndexOf(".", 0, false)
//                    Log.v("ItemDescription","Last dot Index: "+description.get(lastDot-1))

//                    Log.v("ItemDescription","TimeMillis: "+DateManage().parseDateFeedToMillis(i.datePublished))

                    val item = RssFeedModel(
                            -1,
                            -1,
                            currentKeyword,
                            "",
                            i.time!!,
                            thumbImg,
                            0,
                            actualImg,
                            removeHtmlTags(i.description!!),
                            false,
                            "",
                            "",
                            0,
                            0,
                            false,
                            "",
                            false,false)

                    Log.v("ItemDescription", "InsertingData: " + item.toString())

//                    getBingImages(rssIconItem)

                    currentFeedPosition++

                    RssFeedDao().updateOrCreate(item)
                }
            }
            requestTweetNews()
        }else{
            requestTweetNews()
            Log.v("AddItem","Item is null")
        }
    }

    fun runNewsResultsList() {
        if (newsResults != null && !newsResults!!.isEmpty()) {
            if (currentFeedPosition < newsResults?.size!! && newsResults!![currentFeedPosition] != null) {
                Log.v("RequestRss", "Request rss: " + newsResults!![currentFeedPosition])
//                GetRSSService(this).getRSS(userLikes!![currentPosition])
                val i = newsResults!![currentFeedPosition]
                var feed: RssFeedModel? = RssFeedDao().findRssFeedModelByTitle(removeHtmlTags(i.title))
                if (feed == null) {
                    Log.v("AddItem", "Adding: " + currentKeyword)
                    var thumbImg = ""
                    var actualImg = ""
                    if (i.media[0] != null && i.media.isNotEmpty()){
                        thumbImg = i.media[0].pict.url
                        actualImg = i.media[1].pict.url
                    }


//                    val description = i.description.replace("...", "")
//                    val lastDot = i.description.lastIndexOf(".", 0, false)
//                    Log.v("ItemDescription","Last dot Index: "+description.get(lastDot-1))

//                    Log.v("ItemDescription","TimeMillis: "+DateManage().parseDateFeedToMillis(i.datePublished))

                    val item = RssFeedModel(
                            -1,
                            -1,
                            currentKeyword,
                            removeHtmlTags(i.title),
                            i.url,
                            thumbImg,
                            i.date as Long,
                            actualImg,
                            removeHtmlTags(i.desc),
                            false,
                            "",
                            "",
                            0,
                            0,
                            false,
                            "",
                            false,false)

                    Log.v("ItemDescription", "InsertingData: " + item.toString())

//                    getBingImages(rssIconItem)

                    currentFeedPosition++

                    RssFeedDao().updateOrCreate(item)

                    runNewsResultsList()
                } else {
                    currentFeedPosition++
                    runNewsResultsList()
                    Log.v("FeedResult", "Feed already exist")
                }
            } else {
                requestNews()
                Log.v("FeedResult", "Out of size")
            }
        }else{
            requestNews()
        }
    }

    fun runResultsList() {
        if (feedResults != null && !feedResults!!.isEmpty()) {
            if (currentFeedPosition < feedResults?.size!! && feedResults!![currentFeedPosition] != null) {
                Log.v("RequestRss", "Request rss: " + feedResults!![currentFeedPosition])
//                GetRSSService(this).getRSS(userLikes!![currentPosition])
                val i = feedResults!![currentFeedPosition]
                var feed: RssFeedModel? = RssFeedDao().findRssFeedModelByTitle(i.name)
                if (feed == null) {
                    Log.v("AddItem", "Adding: " + currentKeyword)
                    var actualImg = ""
                    if (i.image != null)
                        actualImg = i.image.thumbnail.contentUrl

                    val description = i.description.replace("...", "")
                    val lastDot = i.description.lastIndexOf(".", 0, false)
//                    Log.v("ItemDescription","Last dot Index: "+description.get(lastDot-1))

//                    Log.v("ItemDescription","TimeMillis: "+DateManage().parseDateFeedToMillis(i.datePublished))

                    val rssIconItem = RssFeedModel(
                            -1,
                            -1,
                            currentKeyword,
                            i.name.replace("'", "&apos;"),
                            i.url,
                            actualImg,
                            DateManage().parseDateFeedToMillis(i.datePublished),
                            actualImg,
                            getParsedDescriptionText(i.description)!!,
                            false,
                            "",
                            "",
                            0,
                            0,
                            false,
                            "",
                            false,false)

                    Log.v("ItemDescription", "InsertingData: " + rssIconItem.toString())

                    getBingImages(rssIconItem)

                    currentFeedPosition++
                } else {
                    currentFeedPosition++
                    runResultsList()
                    Log.v("FeedResult", "Feed already exist")
                }
            } else {
                requestNewRss()
                Log.v("FeedResult", "Out of size")
            }
        }else{
            requestNewRss()
        }
    }

    fun getParsedDescriptionText(text:String?):String?{
        var newString:String? = null
        if (!text.isNullOrBlank()){
            Log.v("ParsingText", "Text is not null: "+text)
            val dotLastIndex = text!!.replace("...","").lastIndexOf(".")
            val commaLastIndex = text!!.replace("...","").lastIndexOf(",")
            if (dotLastIndex>commaLastIndex){
                if (dotLastIndex>0){
                    Log.v("ParsingText", "Dot last index: "+dotLastIndex)
                    newString = text.substring(0,dotLastIndex)
                }else{
                    newString = text
                }
            }else{
                if (commaLastIndex>0){
                    Log.v("ParsingText", "Comma last index: "+commaLastIndex)
                    newString = text.substring(0,commaLastIndex)
                }else{
                    newString = text
                }
            }
        }else{
            Log.v("ParsingText", "Text is null")
        }
        return newString
    }

//    fun loadRssFeed(item: RssItem?, keyword: String){
//        if (item!=null){
//            Log.v("AddItem","Adding: "+keyword)
//            Log.v("AddItemValue","Value: "+item.toString())
//            var actualImg = ""
//            if (item.image!=null)
//                actualImg = item.image
//
//            var rssIconItem = RssItemModel(
//                    keyword,
//                    item.title,
//                    item.link,
//                    actualImg,
//                    item.publishDate,
//                    actualImg,
//                    "")
//
////            feedIconLst?.add(parseHTML(item.description,rssIconItem))
////            feedLst?.add(parseHTML(item.description,rssIconItem))
//
//            getBingImages(rssIconItem)
//        }else{
//            requestNewRss()
//            Log.v("AddItem","Item is null")
//        }
//    }

    fun getBingImages(item:RssFeedModel){
        val service = GetBingImagesService(this)
        service.getImages(item,item.title)
    }

    fun bingImageResult(item:RssFeedModel?){
        if (item!=null){
            feedIconLst?.add(parseHTML(item.description,item))
            feedLst?.add(parseHTML(item.description,item))
            RssFeedDao().updateOrCreate(item)
        }
        runResultsList()
    }

    fun removeHtmlTags(text: String): String {
        val tagsRegex = Regex("<[^>]*>")
        val parsedText = text
                .replace(tagsRegex,"")
                .replace("&nbsp;"," ")
                .replace("Full coverage","")
        Log.v("RssItemValue",parsedText.toString())
        return parsedText
    }

    fun parseHTML(html: String, item: RssFeedModel): RssFeedModel {
        val aTagRegex = "<a[^>]+href\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>([^<]+)<\\/a>"
        val pattern = Pattern.compile(aTagRegex)
        val matcher = pattern.matcher(html)
        Log.v("HtmlText","Checking: "+item.toString())
        if (matcher.find(0)) {
            Log.v("HtmlText","Matcher 1")
            val parsedHtml = html.substring(matcher.start(), matcher.end())
            try{
                Log.v("HtmlText","Result 1: "+matcher.group(0))
                Log.v("HtmlText","Result 2: "+matcher.group(1))
            }catch (e:Exception){
                Log.e("HtmlTextException",e.toString())
            }
            var finalDescription: String = ""
            try{
                finalDescription = matcher.group(1).replace(":","%3A").replace("/","%2F")
                finalDescription = "http://drewpic.com/tumblr2/code-grabber-mobile.php?url="+finalDescription
            }catch (e:Exception){
                Log.e("HtmlTextException",e.toString())
            }
            Log.v("HtmlText","Description: "+finalDescription)
            item.description = finalDescription
//                break
        }
//        while (matcher.find()){
//            if (matcher.find()) {
//                Log.v("HtmlText","Groups Count:  "+matcher.groupCount())
//                val parsedHtml = html.substring(matcher.start(), matcher.end())
//                try{
//                    Log.v("HtmlText","Result 1: "+matcher.group(0))
//                    Log.v("HtmlText","Result 2: "+matcher.group(1))
//                    Log.v("HtmlText","Result 3: "+matcher.group(2))
//                    Log.v("HtmlText","Result 4: "+matcher.group())
//                }catch (e:Exception){
//                    Log.e("HtmlTextException",e.toString())
//                }
//                item.thumbnailImg = matcher.group(0)
//                item.description = matcher.group(1)
////                break
//            } else {
//                // TODO handle condition when input doesn't have an email address
//            }
//        }
        Log.v("RssItemValue",item.toString())
        return item
    }

    fun reStartService(){
        Log.v("RssService","Re-starting Service")
        val service = Intent(this@ProfileActivity,FloatingViewService::class.java)
        service.putParcelableArrayListExtra("FeedIconList",feedIconLst as java.util.ArrayList<out Parcelable>)
        service.putParcelableArrayListExtra("FeedList",feedLst as java.util.ArrayList<out Parcelable>)
        val token = AccessToken.getCurrentAccessToken()
        if (token!=null){
            service.putExtra("Token",token.token)
        }
        startService(service)
        finish()
    }

    fun killService(){
        Log.v("RssService","Killing Service")
        val service = Intent(this@ProfileActivity,FloatingViewService::class.java)
        stopService(service)
    }

    fun loadWebView(){
        webLoader!!.getSettings().javaScriptEnabled = true
        webLoader!!.webChromeClient = object: WebChromeClient(){
            override fun onConsoleMessage(consoleMessage: ConsoleMessage?): Boolean {
                super.onConsoleMessage(consoleMessage)
                Log.v("ConsoleLog","Console Log")
                Log.v("ConsoleLog","Current Like: "+currentPosition)
                if (consoleMessage!=null){
                    var jsonObject: JSONObject?=null
                    try{
                        jsonObject = JSONObject(consoleMessage.message())
                    }catch (e:Exception){
                        Log.e("ConsoleLog","Error Parsing Object")
                    }
                    Log.v("ConsoleLog",consoleMessage.message())
                    if (jsonObject!=null){
                        val response = Gson().fromJson<GetTwitterNews>(consoleMessage.message(),GetTwitterNews::class.java)
                        Log.v("ConsoleLog","Object: "+response)
                        loadTweetNewsFeed(response.tweets,currentKeyword)
                    }
                }
                return true
            }
        }
        webLoader!!.webViewClient = object:WebViewClient(){
            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                if (firstSearch){
                    firstSearch=false
                    doSearch()
                }
                Log.v("WebView","Page Finished")
            }
        }
        webLoader!!.loadUrl("http://www.drewpic.net/index2")
    }

    fun reloadWebView(keyword:String){
        if (webLoader!=null){
            webLoader.clearCache(true)
            webLoader.clearHistory()
            val loadJavascript = "javascript:searchWith('"+keyword+"')"
            Log.v("WebLoader","Loading Javascript: "+loadJavascript)
            webLoader!!.loadUrl(loadJavascript)
        }else{
            Log.e("WebLoader","Web Loader is null")
        }
    }

}
