package com.aarr.drewpic.Views.Fragments


import android.app.Dialog
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.TextView
import com.aarr.drewpic.Adapters.CategoryAdapter

import com.aarr.drewpic.R
import com.aarr.drewpic.Services.Response.Categories.Data
import com.aarr.drewpic.Services.Response.SignUp.SignUpResponse
import com.lsjwzh.widget.materialloadingprogressbar.CircleProgressBar
import java.net.MalformedURLException
import java.net.URL
import android.os.Build
import android.os.CountDownTimer
import android.support.annotation.RequiresApi
import android.support.design.widget.BottomNavigationView
import android.support.v4.content.ContextCompat
import android.view.*
import android.webkit.WebResourceResponse
import android.widget.ImageButton
import android.widget.RelativeLayout
import android.widget.Toast
import com.aarr.drewpic.Database.Dao.CategoriesDao
import com.aarr.drewpic.Database.Dao.SubCategoriesDao
import com.aarr.drewpic.Database.Dao.UserDao
import com.aarr.drewpic.Database.Helper.DatabaseHelper
import com.aarr.drewpic.Database.Model.User
import com.aarr.drewpic.Views.SignupActivity
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem
import com.eightbitlab.bottomnavigationbar.BottomBarItem
import com.eightbitlab.bottomnavigationbar.BottomNavigationBar
import java.util.*


/**
 * A simple [Fragment] subclass.
 */
class WebFragments : Fragment() {

    private var rootView: View? = null
    private var webView: WebView? = null
    private var selectedCat: String? = null
    private var progressBar: CircleProgressBar? = null
    private var dialog: Dialog? = null
    private var baseUrl = "drewpic.com"
//    private var bottomMenu: BottomNavigationBar? = null
    private var bottomMenu: AHBottomNavigation? = null
    private var parentActivity: SignupActivity? = null
    private var categories: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        rootView = inflater!!.inflate(R.layout.fragment_web, container, false)
        parentActivity = activity as SignupActivity
        progressBar = rootView!!.findViewById<CircleProgressBar>(R.id.progressBar) as CircleProgressBar
        progressBar!!.setColorSchemeResources(R.color.colorAccent)
        initAndresetWebView()
        initMenu(true)

        if (getUserData() != null) {
            categories = getUserData()!!.subCategory
        }
        if (categories==null){
            Log.v("BundleData","DATA LIST IS NULL")
        }else{
            var catsValue = categories!!.split(",")
            selectedCat = catsValue[Random().nextInt((catsValue.size-1) - 0 + 1) + 0]
            Log.v("BundleData","DATA LIST IS NOT NULL")
            Log.v("BundleData",selectedCat)
            setWebView("http://drewpic.com/tumblr2/searchnew.php?subcategory="+selectedCat+"&limit=3",true)
        }

        return rootView
    }

    fun getUserData():User?{
        return UserDao().findCurrentUser()
    }

//    fun initMenu(isNotification:Boolean){
//        bottomMenu = rootView!!.findViewById(R.id.bottom_bar) as BottomNavigationBar
//        bottomMenu!!.removeAllViews()
//        var hotIcon : Int = R.mipmap.hot32
//        if (isNotification){
//            hotIcon = R.mipmap.hot32_new
//        }
//        var item: BottomBarItem = BottomBarItem(R.mipmap.home_32, R.string.menu_home)
//        var item2: BottomBarItem = BottomBarItem(R.mipmap.ic_view_list_black_36dp, R.string.menu_categories)
//        var item3: BottomBarItem = BottomBarItem(hotIcon, R.string.menu_popular)
//        var item4: BottomBarItem = BottomBarItem(R.mipmap.exit32, R.string.menu_signout)
//
//        bottomMenu!!.addTab(item)
//        bottomMenu!!.addTab(item2)
//        bottomMenu!!.addTab(item3)
//        bottomMenu!!.addTab(item4)
//
//        bottomMenu!!.selectTab(0,false)
//        bottomMenu!!.setOnReselectListener(object:BottomNavigationBar.OnReselectListener{
//            override fun onReselect(position: Int) {
//                when (position){
//                    0 ->
//                        checkWebSite()
//                    1 ->
//                        goToCategories()
//                    2 ->
//                        loadPopular()
//                    3 ->
//                        signOut()
//                }
//            }
//
//        })
//        bottomMenu!!.setOnSelectListener(object:BottomNavigationBar.OnSelectListener{
//            override fun onSelect(position: Int) {
//                when (position){
//                    0 ->
//                        checkWebSite()
//                    1 ->
//                        goToCategories()
//                    2 ->
//                        loadPopular()
//                    3 ->
//                        signOut()
//                }
//            }
//        })
//    }

    fun initMenu(isNotification:Boolean){
        bottomMenu = rootView!!.findViewById<AHBottomNavigation>(R.id.bottom_bar) as AHBottomNavigation

        var item: AHBottomNavigationItem = AHBottomNavigationItem(getString(R.string.menu_home),R.mipmap.home_32)
        var item2: AHBottomNavigationItem = AHBottomNavigationItem(getString(R.string.menu_categories), R.mipmap.ic_view_list_black_36dp)
        var item3: AHBottomNavigationItem = AHBottomNavigationItem(getString(R.string.menu_popular), R.mipmap.hot32)
        var item4: AHBottomNavigationItem = AHBottomNavigationItem(getString(R.string.menu_signout), R.mipmap.exit32)

        bottomMenu!!.addItem(item)
        bottomMenu!!.addItem(item2)
        bottomMenu!!.addItem(item3)
        bottomMenu!!.addItem(item4)

        bottomMenu!!.setDefaultBackgroundColor(ContextCompat.getColor(activity!!,R.color.colorPrimary))
        bottomMenu!!.setAccentColor(ContextCompat.getColor(activity!!,R.color.colorAccent))
        bottomMenu!!.setInactiveColor(Color.parseColor("#FFFFFF"))
        bottomMenu!!.isForceTint = true
        bottomMenu!!.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW)
        bottomMenu!!.setCurrentItem(0)
        bottomMenu!!.setNotification(" ",2)

        if (isNotification){

        }
        bottomMenu!!.setOnTabSelectedListener(object:AHBottomNavigation.OnTabSelectedListener{
            override fun onTabSelected(position: Int, wasSelected: Boolean): Boolean {
                when (position){
                    0 ->
                        checkWebSite()
                    1 ->
                        goToCategories()
                    2 ->
                        loadPopular()
                    3 ->
                        signOut()
                }
                return true
            }
        })
    }

    fun signOut(){
        parentActivity!!.signOut()
    }

    fun loadPopular(){
        bottomMenu!!.setNotification("",2)
        var keyword = SubCategoriesDao().getRandomKeyword()
        if (keyword!=null && !keyword.equals("")){
            Log.v("Keyword",keyword)
            initAndresetWebView()
            setWebView("http://drewpic.com/tumblr2/b.php?keyword="+keyword,true)
        }
    }


    fun initAndresetWebView(){
        webView = rootView!!.findViewById<WebView>(R.id.webView) as WebView
        webView!!.getSettings().useWideViewPort = true
        webView!!.getSettings().javaScriptEnabled = true
        webView!!.setVerticalScrollBarEnabled(true)
        webView!!.setHorizontalScrollBarEnabled(true)
        webView!!.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY)
    }

    fun goToCategories(){
//        DatabaseHelper(activity).BD_backup()
        if (parentActivity!=null){
            parentActivity!!.returnWebFragment(false)
        }
    }

    fun checkWebSite(){
        var catsValue = categories!!.split(",")
        selectedCat = catsValue[Random().nextInt((catsValue.size-1) - 0 + 1) + 0]
        Log.v("BundleData","DATA LIST IS NOT NULL")
        Log.v("BundleData",selectedCat)
        Log.v("Option","1")
        initAndresetWebView()
        setWebView("http://drewpic.com/tumblr2/searchnew.php?subcategory="+selectedCat+"&limit=3",true)
    }

    fun setWebView(urlValue: String,resetOnFinish:Boolean) {
        Log.v("UnparsedUrlWebView", urlValue)

        progressBar!!.setVisibility(View.VISIBLE)
        if (webView != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                webView!!.webViewClient = object : WebViewClient() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            var urlValue: URL? = URL(request!!.url!!.toString())
                            Log.v("UrlHost",urlValue!!.host)
                            if (baseUrl.equals(urlValue!!.host)){
                                Log.v("UrlHost","Same host")
                                setWebView(request!!.url!!.toString(),false)
                            }else{
                                Log.v("UrlHost","Different host")
                                showSummarized(request!!.url!!.toString())
                            }
                        }
                        return true
                    }

                    override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                        Log.v("onPageStarted", url)
                        progressBar!!.visibility = View.VISIBLE
                        Log.v("OriginalUrl", url)
                        super.onPageStarted(view, url, favicon)
                    }

                    override fun onPageFinished(view: WebView, url: String) {
                        Log.i("Process", "Finished loading URL: " + url)
                        progressBar!!.setVisibility(View.INVISIBLE)
                        if (resetOnFinish){
                            webView!!.clearCache(true)
                            webView!!.clearFormData()
                            webView!!.clearHistory()
                            webView!!.clearMatches()
                            webView!!.clearSslPreferences()
                        }
        //                    isFromGoTo = false
        //                    isHome = false
                    }
                }
            } else {
                webView!!.setWebViewClient(object : WebViewClient() {

                    override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                        var urlValue: URL? = URL(url)
                        Log.v("UrlHost",urlValue!!.host)
                        if (baseUrl.equals(urlValue!!.host)){
                            Log.v("UrlHost","Same host")
                            setWebView(url!!,false)
                        }else{
                            Log.v("UrlHost","Different host")
                            showSummarized(url)
                        }
                        return true
                    }

                    override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                        Log.v("onPageStarted", url)
                        progressBar!!.visibility = View.VISIBLE
                        Log.v("OriginalUrl", url)
                        super.onPageStarted(view, url, favicon)
                    }

                    override fun onPageFinished(view: WebView, url: String) {
                        Log.i("Process", "Finished loading URL: " + url)
                        progressBar!!.setVisibility(View.INVISIBLE)
                        if (resetOnFinish){
                            webView!!.clearCache(true)
                            webView!!.clearFormData()
                            webView!!.clearHistory()
                            webView!!.clearMatches()
                            webView!!.clearSslPreferences()
                        }
//                    isFromGoTo = false
//                    isHome = false
                    }
                })
            }
            Log.v("Loading",urlValue)
            webView!!.loadUrl(urlValue)
        }
    }

    fun goBack():Boolean{
        var goBack = false
        if (webView!=null){
            if (webView!!.canGoBack()){
                webView!!.goBack()
                goBack = true
            }
        }
        return goBack
    }

    fun showSummarized(currentUrl:String?){
        dialog = Dialog(activity)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                dialog!!.getWindow().setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog!!.setContentView(R.layout.custom_web_view)

        var lp: WindowManager.LayoutParams? = WindowManager.LayoutParams()
        lp!!.copyFrom(dialog!!.getWindow().getAttributes())
        lp!!.width = WindowManager.LayoutParams.MATCH_PARENT
        lp!!.height = WindowManager.LayoutParams.MATCH_PARENT

        var fullUrl = dialog!!.findViewById<TextView>(R.id.fullUrl) as TextView
        fullUrl!!.setText(currentUrl)
        fullUrl!!.setOnClickListener {
            dialog!!.dismiss()
            showNonSummarized(currentUrl)
        }

        var bubbleContainer = dialog!!.findViewById<RelativeLayout>(R.id.bubbleContainer) as RelativeLayout
        if (getUserData()!!.fullPageBubble){
            bubbleContainer!!.visibility = View.VISIBLE
            object: CountDownTimer(5000,1000) {
                override fun onTick(millisUntilFinished: Long) {
                    Log.v("TimerTick","Tick: "+millisUntilFinished)
                }

                override fun onFinish() {
                    bubbleContainer!!.visibility = View.GONE
                }

            }.start()
            UserDao().updateBubbleShow()
        }
        var dialogProgressBar = dialog!!.findViewById<CircleProgressBar>(R.id.progressBar) as CircleProgressBar
        dialogProgressBar!!.setColorSchemeResources(R.color.colorAccent)
        var btnClose: ImageButton? = dialog!!.findViewById<ImageButton>(R.id.btnClose) as ImageButton
        btnClose!!.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                dialog!!.dismiss()
            }
        })
        var webViewDialog: WebView? = dialog!!.findViewById<WebView>(R.id.customWebView) as WebView
        if (webViewDialog!=null){
            webViewDialog.getSettings().setLoadWithOverviewMode(true)
            webViewDialog.getSettings().javaScriptEnabled = true
            webViewDialog.setVerticalScrollBarEnabled(true)
            webViewDialog.setHorizontalScrollBarEnabled(true)
            webViewDialog.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY)
//                    webViewDialog.getSettings().setUseWideViewPort(true)
//                    var currentUrl : String? = urlContainer!!.text.toString()
            Log.v("CheckUrl","1:"+currentUrl)
//                    Log.v("CheckUrl","2:"+currentUrlAux)

            if (currentUrl!=null && !currentUrl.equals("")){
                var fullUrl : String? = "http://drewpic.com/tumblr2/code-grabber.php?url="+removeProtocol(currentUrl)
                Log.v("Loading",fullUrl)
                webViewDialog!!.setWebViewClient(object: WebViewClient() {
                    override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                        super.onPageStarted(view, url, favicon)
                    }

                    override fun onPageFinished(view: WebView?, url: String?) {
                        dialogProgressBar.visibility = View.GONE
                        super.onPageFinished(view, url)
                    }
                })
                dialogProgressBar.visibility = View.VISIBLE
                webViewDialog!!.loadUrl(fullUrl)
            }
        }
        dialog!!.show()
        dialog!!.getWindow().setAttributes(lp)
    }

    fun showNonSummarized(currentUrl:String?){
        dialog = Dialog(activity)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog!!.getWindow().setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog!!.setContentView(R.layout.custom_web_view_fullscreen)

        var lp: WindowManager.LayoutParams? = WindowManager.LayoutParams()
        lp!!.copyFrom(dialog!!.getWindow().getAttributes())
        lp!!.width = WindowManager.LayoutParams.MATCH_PARENT
        lp!!.height = WindowManager.LayoutParams.MATCH_PARENT

        var txtTitle = dialog!!.findViewById<TextView>(R.id.txtTitle) as TextView
        var txtUrl = dialog!!.findViewById<TextView>(R.id.txtUrl) as TextView
        txtUrl!!.setText(currentUrl)
        var dialogProgressBar = dialog!!.findViewById<CircleProgressBar>(R.id.progressBar) as CircleProgressBar
        dialogProgressBar!!.setColorSchemeResources(R.color.colorAccent)
        var btnClose: ImageButton? = dialog!!.findViewById<ImageButton>(R.id.btnClose) as ImageButton
        btnClose!!.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                dialog!!.dismiss()
            }
        })
        var webViewDialog: WebView? = dialog!!.findViewById<WebView>(R.id.customWebView) as WebView
        if (webViewDialog!=null){
            webViewDialog.getSettings().setLoadWithOverviewMode(true)
            webViewDialog.getSettings().javaScriptEnabled = true
            webViewDialog.setVerticalScrollBarEnabled(true)
            webViewDialog.setHorizontalScrollBarEnabled(true)
            webViewDialog.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY)
//                    webViewDialog.getSettings().setUseWideViewPort(true)
//                    var currentUrl : String? = urlContainer!!.text.toString()
            Log.v("CheckUrl","1:"+currentUrl)
//                    Log.v("CheckUrl","2:"+currentUrlAux)

            if (currentUrl!=null && !currentUrl.equals("")){
                var fullUrl : String? = currentUrl
                Log.v("Loading",fullUrl)
                webViewDialog!!.setWebViewClient(object: WebViewClient() {
                    override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
//                        txtTitle!!.setText(view!!.title)
                        super.onPageStarted(view, url, favicon)
                    }

                    override fun onPageFinished(view: WebView?, url: String?) {
                        txtTitle!!.setText(view!!.title)
                        dialogProgressBar.visibility = View.GONE
                        super.onPageFinished(view, url)
                    }
                })
                dialogProgressBar.visibility = View.VISIBLE
                webViewDialog!!.loadUrl(fullUrl)
            }
        }
        dialog!!.show()
        dialog!!.getWindow().setAttributes(lp)
    }

    fun removeProtocol(url: String): String {
        var url = url
        url = url.replaceFirst("^(http://\\.https://\\./\\.)".toRegex(), "")
        return url
    }

}// Required empty public constructor
