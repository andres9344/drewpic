package com.aarr.drewpic.Views.Fragments


import android.accounts.Account
import android.accounts.AccountManager
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager

import com.aarr.drewpic.R
import com.aarr.drewpic.Services.SignUpService
import com.aarr.drewpic.Util.OnSwipeTouchListener
import android.util.Patterns
import android.text.InputType
import android.text.TextWatcher
import android.widget.*
import com.aarr.drewpic.Database.Helper.DatabaseHelper
import com.aarr.drewpic.Database.Model.Categories
import com.aarr.drewpic.Database.Model.SubCategories
import com.aarr.drewpic.Database.Model.User
import com.aarr.drewpic.Views.SignupActivity
import com.j256.ormlite.table.TableUtils


/**
 * A simple [Fragment] subclass.
 */
class EmailFragment : Fragment() {

    private var rootView: View? = null
    private var emailContainer: EditText? = null
    private var btnSend: Button? = null
    private var dialogLst: MutableList<CharSequence>? = ArrayList<CharSequence>()
    private var layoutContainer: RelativeLayout? = null
    private var btnRegister: Button? = null
    private var btnFacebook: RelativeLayout?=null
    private var parentActivity:SignupActivity?=null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        rootView = inflater!!.inflate(R.layout.fragment_email, container, false)

        parentActivity = activity as SignupActivity

        btnFacebook = rootView!!.findViewById<RelativeLayout>(R.id.btnFacebook) as RelativeLayout
        btnFacebook!!.setOnClickListener {
            parentActivity!!.facebookLogin()
        }

        emailContainer = rootView!!.findViewById<EditText>(R.id.emailContainer) as EditText
        val imm = parentActivity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(emailContainer, InputMethodManager.SHOW_IMPLICIT)
        emailContainer!!.setOnEditorActionListener(object: TextView.OnEditorActionListener{
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    v!!.clearFocus()
                    hideKeyboard()
                    btnSend!!.performClick()
                    return true
                }
                return false
            }
        })
        emailContainer!!.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                btnRegister!!.visibility = View.VISIBLE
            }

        })
        emailContainer!!.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?) {
                Toast.makeText(activity,"Input clicked",Toast.LENGTH_SHORT)
                dialogLst = ArrayList<CharSequence>()
                try{
                    var accounts = AccountManager.get(activity).accounts
                    if (accounts!=null && accounts.isNotEmpty()) {
                        Log.v("Account", accounts[0].name)
                        val emailPattern = Patterns.EMAIL_ADDRESS
                        for (item in accounts) {
                            if (emailPattern.matcher(item.name).matches()) {
                                dialogLst!!.add(item.name as CharSequence)
                            }
                        }
                    }
                }catch(e:Exception){
                    Log.e("AccountsException",e.toString())
                }

                dialogLst!!.add("Other")
                var dialog: AlertDialog.Builder? = AlertDialog.Builder(activity)
                dialog!!.setTitle("Select an email")
                dialog!!.setSingleChoiceItems(dialogLst!!.toTypedArray(),-1,object : DialogInterface.OnClickListener{
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        dialog!!.dismiss()
                        if (dialogLst!![which].equals("Other")){
                            showInputDialog()
//                                emailContainer!!.isFocusableInTouchMode = true
//                                emailContainer!!.requestFocus()
//                                val inputMethodManager = context
//                                        .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//                                inputMethodManager.showSoftInput(emailContainer, InputMethodManager.SHOW_IMPLICIT)
                        }else{
                            emailContainer!!.setText(dialogLst!![which].toString())
                        }
                    }

                })
                dialog.show()
            }
        })
        btnRegister = rootView!!.findViewById<Button>(R.id.btnRegister) as Button
        btnRegister!!.setOnClickListener {
            var parentActivity = activity as SignupActivity
            if (parentActivity!=null){
                parentActivity.showProgressDialog("Checking email...")
            }
            var emailValue = emailContainer!!.text.toString()
            SignUpService(parentActivity!!).SignUp(emailValue)
        }
        btnSend = rootView!!.findViewById<Button>(R.id.btnSend) as Button
        btnSend!!.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?) {
                var emailValue = emailContainer!!.text.toString()
                SignUpService(parentActivity!!).SignUp(emailValue)
            }
        })
        btnSend!!.requestFocus()

        if (isSignOut()){
            TableUtils.clearTable(DatabaseHelper.helper.connectionSource, User::class.java)
            TableUtils.clearTable(DatabaseHelper.helper.connectionSource, Categories::class.java)
            TableUtils.clearTable(DatabaseHelper.helper.connectionSource, SubCategories::class.java)
            var sp = parentActivity!!.getSharedPreferences("SignOut",Context.MODE_PRIVATE).edit()
            sp.putBoolean("isSignOut",false)
            sp.commit()
        }

        var bundle: Bundle? = this.arguments
        if (bundle!=null && bundle!!.getBoolean("UserExist",false)){
            var parentActivity = activity as SignupActivity
            parentActivity.goToCategories()
        }

        return rootView
    }

    fun isSignOut():Boolean{
        var sp = activity?.getSharedPreferences("SignOut",Context.MODE_PRIVATE)
        return sp!!.getBoolean("isSignOut",false)
    }

    fun hideKeyboard() {
        val view = activity?.currentFocus
        if (view != null) {
            val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun showInputDialog(){
        val builder = AlertDialog.Builder(activity)
        builder.setTitle("Type your email address")

// Set up the input
        val input = EditText(activity)
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.inputType = InputType.TYPE_CLASS_TEXT
        input.hint = "youremail@host.com"
        builder.setView(input)

// Set up the buttons
        builder.setPositiveButton("OK") { dialog, which ->
            var emailValue = input.text.toString()
            val emailPattern = Patterns.EMAIL_ADDRESS
            if (emailPattern.matcher(emailValue).matches()) {
                dialog.dismiss()
                emailContainer!!.setText(emailValue)
            }else{
                Toast.makeText(activity,"Invalid email",Toast.LENGTH_SHORT)
            }
        }
        builder.setNegativeButton("Cancel") { dialog, which -> dialog.cancel() }

        builder.show()
    }

}// Required empty public constructor
