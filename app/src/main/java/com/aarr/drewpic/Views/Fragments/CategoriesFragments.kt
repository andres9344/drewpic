package com.aarr.drewpic.Views.Fragments


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.aarr.drewpic.Adapters.CategoryAdapter
import com.aarr.drewpic.Database.Dao.CategoriesDao
import com.aarr.drewpic.Database.Dao.SubCategoriesDao
import com.aarr.drewpic.Database.Dao.UserDao
import com.aarr.drewpic.Database.Helper.DatabaseHelper
import com.aarr.drewpic.Database.Model.Categories
import com.aarr.drewpic.Database.Model.SubCategories
import com.aarr.drewpic.Database.Model.SubCategoryModel
import com.aarr.drewpic.Database.Model.User

import com.aarr.drewpic.R
import com.aarr.drewpic.Services.Response.Categories.Data
import com.aarr.drewpic.Services.Response.SignUp.SignUpResponse
import com.aarr.drewpic.Views.SignupActivity
import com.j256.ormlite.table.TableUtils


/**
 * A simple [Fragment] subclass.
 */
class CategoriesFragments : Fragment() {

    private var rootView: View? = null
    private var recycler: RecyclerView? = null
    private var lstData: List<Data>? = null
    private var lstCategories: MutableList<Categories>? = null
    private var userData: User? = null
    private var selectedCount: TextView? = null
    private var toWeb: Boolean = false


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        rootView = inflater!!.inflate(R.layout.fragment_categories_fragments, container, false)
        selectedCount = rootView!!.findViewById<TextView>(R.id.selectedCount) as TextView
        recycler = rootView!!.findViewById<RecyclerView>(R.id.recycler) as RecyclerView
        val parentActivity = activity as SignupActivity
        val bundle = this.arguments
        if (bundle != null) {
            toWeb = bundle.getBoolean("ToWeb",false)
        }
        getUserData()
        getCategories()
        if (lstCategories==null){
            Log.v("BundleData","DATA LIST IS NULL")
        }else{
            Log.v("BundleData","DATA LIST IS NOT NULL")
            Log.v("BundleData",lstCategories.toString())
        }
        if (userData==null){
            Log.v("BundleData","USER DATA IS NULL")
        }else{
            var parentActivity = activity as SignupActivity
            if (isReturnCategories()!!){
                Log.v("BundleData","IS RETURN CATEGORIES")
                var sp = activity!!.getSharedPreferences("ReturnCategories",Context.MODE_PRIVATE).edit()
                sp.putBoolean("Return",false)
                sp.apply()
                if (userData!!.subCategory != null && !userData!!.subCategory!!.equals("")){
                    val subCategories = userData!!.subCategory!!.split(",")
                    selectedCount!!.setText(userData!!.subCategory!!.split(",").size.toString())
                    if (lstCategories!=null && lstCategories!!.isNotEmpty()){
                        for (item in lstCategories!!){
                            var lstSubs = SubCategoriesDao().findSubCategoriesByCategory(item.id)
                            for (item2 in lstSubs!!){
                                val sub2 = item2!!.subCat!!.split("#")
                                for (item3 in subCategories){
                                    if (sub2[0] == item3){
                                        val newItem: SubCategoryModel = SubCategoryModel(sub2[0].toInt(),sub2[1])
                                        parentActivity.addSubCategory(newItem)
                                    }
                                }
                            }
                        }
                    }
                }else if (parentActivity.lstSelected!=null && parentActivity.lstSelected.isNotEmpty()){
                    selectedCount!!.setText(parentActivity.lstSelected.size.toString())
                    if (lstCategories!=null && lstCategories!!.isNotEmpty()){
                        for (item in lstCategories!!){
                            var lstSubs = SubCategoriesDao().findSubCategoriesByCategory(item.id)
                            for (item2 in lstSubs!!){
                                val sub2 = item2!!.subCat!!.split("#")
                                for (item3 in parentActivity.lstSelected){
                                    if (sub2[0] == item3.id.toString()){
                                        val newItem: SubCategoryModel = SubCategoryModel(sub2[0].toInt(),sub2[1])
                                        parentActivity.addSubCategory(newItem)
                                    }
                                }
                            }
                        }
                    }
                }
            }else if (isSignOut()!!){
                Log.v("BundleData","IS SIGN OUT")
                TableUtils.clearTable(DatabaseHelper.helper.connectionSource, User::class.java)
                TableUtils.clearTable(DatabaseHelper.helper.connectionSource, Categories::class.java)
                TableUtils.clearTable(DatabaseHelper.helper.connectionSource, SubCategories::class.java)
                var sp = activity?.getSharedPreferences("SignOut",Context.MODE_PRIVATE)?.edit()
                sp?.putBoolean("isSignOut",false)
                sp?.apply()
                parentActivity.returnSignIn()
            }else{
                Log.v("BundleData","IS GOING TO WEB")
                if (userData!!.subCategory != null && !userData!!.subCategory!!.equals("")){
                    Log.v("BundleData","SUB CATEGORY IS NOT NULL")
                    parentActivity.goToWeb()
                }
            }
//            if (parentActivity.lstSelected.size>0){
//                parentActivity.keepOrResetSubCategories()
//            }
            Log.v("BundleData","USER DATA IS NOT NULL")
            Log.v("BundleData",userData.toString())
        }

        fillAndResetRecycler()


        return rootView
    }

    fun isSignOut():Boolean?{
        var sp = activity?.getSharedPreferences("SignOut", Context.MODE_PRIVATE)
        return sp?.getBoolean("isSignOut",false)
    }
    fun isReturnCategories():Boolean?{
        var sp = activity?.getSharedPreferences("ReturnCategories",Context.MODE_PRIVATE)
        return sp?.getBoolean("Return",false)
    }

    fun getUserData(){
        userData = UserDao().findCurrentUser()
    }

    fun getCategories(){
        lstCategories = CategoriesDao().findCategories()
    }

    fun updateSelectedCount(count:Int){
        selectedCount!!.setText(count.toString())
    }

    fun fillAndResetRecycler(){
        recycler!!.adapter = null
        var adapter: CategoryAdapter? = CategoryAdapter(lstCategories,activity)
        recycler!!.layoutManager = GridLayoutManager(activity,3)
        recycler!!.adapter = adapter
    }

    fun fragmentGoBack(){
        fragmentManager?.popBackStack()
    }

}// Required empty public constructor
