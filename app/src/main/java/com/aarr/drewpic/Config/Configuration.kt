package com.aarr.drewpic.Config

/**
 * Created by andresrodriguez on 12/9/17.
 */
class Configuration {

    val dbName = "/data/data/com.aarr.drewpic/drewpic.s3db"
    val GOOGLE_SEARCH_API = "AIzaSyBBTkVR_UXbFP4LJtdpZCuHb8YMw-J3aCo"
    val PAYMENT_CREDIT_DEBIT_CARD = 1
    val PAYMENT_PAYPAL = 2
    val PAYMENT_CASH = 3
}