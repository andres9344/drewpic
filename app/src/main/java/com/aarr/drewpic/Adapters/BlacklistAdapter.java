package com.aarr.drewpic.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;


import com.aarr.drewpic.Database.Model.BlacklistFilter;
import com.aarr.drewpic.R;
import com.aarr.drewpic.Views.UrlFilter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by andresrodriguez on 11/21/16.
 */
public class BlacklistAdapter extends RecyclerView.Adapter<BlacklistAdapter.BlacklistViewHolder> {

    private Context adapterContext;
    private List<BlacklistFilter> blacklist = null;
    private Dialog dialog = null;
    private UrlFilter activity = null;
    private int type;

    public List<BlacklistFilter> getBlacklist() {
        if (blacklist==null)
            blacklist = new ArrayList<BlacklistFilter>();
        return blacklist;
    }

    public void setBlacklist(List<BlacklistFilter> blacklist) {
        this.blacklist = blacklist;
    }

    public static class BlacklistViewHolder extends RecyclerView.ViewHolder {

//        public TextView idField;
        public TextView url;
        public TextView creationDate;
        public LinearLayout linearLayout;
        public Spinner protocolSpinner;

        public BlacklistViewHolder(View v) {
            super(v);
//            idField = (TextView) v.findViewById(R.id.idField);
            url = (TextView) v.findViewById(R.id.url);
//            creationDate = (TextView) v.findViewById(R.id.creationDate);
            linearLayout = (LinearLayout) v.findViewById(R.id.layout);

        }
    }

    public BlacklistAdapter(int type, List<BlacklistFilter> items, Context context) {
        setBlacklist(items);
        Log.v("BlacklistSize", String.valueOf(getBlacklist().size()));
        adapterContext = context;
        this.type = type;
    }

    @Override
    public BlacklistAdapter.BlacklistViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_url_filter, parent, false);
        activity = (UrlFilter) adapterContext;
        return new BlacklistViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final BlacklistAdapter.BlacklistViewHolder holder, final int position) {
        if (blacklist!=null){
            if (blacklist.size()>0){
//                holder.idField.setText("ID: " + String.valueOf(getBlacklist().get(position).getId()));
                holder.url.setText("URL: " + String.valueOf(getBlacklist().get(position).getUrl()));
//                holder.creationDate.setText("DATE: " + getBlacklist().get(position).getCreationDate().toString());

                holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog = new Dialog(adapterContext);
                        dialog.setContentView(R.layout.url_dialog);
                        dialog.setTitle("Select an option");
                        Button edit = (Button) dialog.findViewById(R.id.editUrl);
                        edit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                                final Dialog dialog1 = new Dialog(adapterContext);
                                dialog1.setContentView(R.layout.goto_layout);
                                dialog1.setTitle("Edit page");
                                Spinner protocolSpinner = (Spinner) dialog1.findViewById(R.id.protocolSpinner);
                                protocolSpinner.setVisibility(View.GONE);
                                final EditText urlDestination = (EditText) dialog1.findViewById(R.id.url);
                                dialog1.setCanceledOnTouchOutside(false);
                                final Button go = (Button) dialog1.findViewById(R.id.goButton);
                                go.setText("Save");
                                urlDestination.setText(getBlacklist().get(position).getUrl().replace("www.",""));
                                go.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        if (urlDestination!=null){
                                            String urlValue = urlDestination.getText().toString();
                                            if (urlValue!=null){
                                                if (!urlValue.equals("")){
                                                    try {
                                                        Log.v("UrlValue","http://www."+urlValue);
                                                        dialog.dismiss();
                                                        BlacklistFilter blacklist = new BlacklistFilter();
                                                        URL url = new URL("http://www."+urlValue);
                                                        String host = url.getHost();
                                                        getBlacklist().get(position).setCreatedDate(new Date());
                                                        getBlacklist().get(position).setUrl(getCleanUrl(host));
                                                        long a = blacklist.getRepository().update(getBlacklist().get(position));
                                                        Log.v("WhitelistCreated", String.valueOf(a));
                                                        UrlFilter act = (UrlFilter) adapterContext;
                                                        act.fillAndResetRecycler(null);
                                                        dialog1.dismiss();
                                                    } catch (MalformedURLException e) {
                                                        e.printStackTrace();
                                                    }

                                                }
                                            }
                                        }
                                    }
                                });
                                final Button cancel = (Button) dialog1.findViewById(R.id.cancelButton);
                                cancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        dialog1.cancel();
                                    }
                                });
                                dialog1.show();
                            }
                        });
                        Button delete = (Button) dialog.findViewById(R.id.deleteUrl);
                        delete.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                BlacklistFilter blacklist = new BlacklistFilter();
                                blacklist.getRepository().delete(getBlacklist().get(position));
                                UrlFilter act = (UrlFilter) adapterContext;
                                act.fillAndResetRecycler(null);
                                dialog.cancel();
                            }
                        });

                        dialog.show();
                    }
                });
            }
        }

    }

    public String getCleanUrl(String url){
        url = url.replaceFirst("^(http://www\\.|http://|www\\.https://www\\.|https://|www\\.http://m\\.|http://|m\\.https://m\\.|https://|m\\.www\\.)","");
        return url;
    }

    @Override
    public int getItemCount() {
        return getBlacklist().size();
    }
}
