package com.aarr.drewpic.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aarr.drewpic.R;
import com.aarr.drewpic.Views.BrowserSettings;
import com.aarr.drewpic.Views.SettingsActivity;
import com.aarr.drewpic.Views.UrlFilter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by andresrodriguez on 11/21/16.
 */
public class SettingsAdapter extends RecyclerView.Adapter<SettingsAdapter.SettingsViewHolder> {

    private Context adapterContext;
    private List<String> settings = null;
    private List<String> info = null;
    private List<Integer> images = null;
    private Dialog dialog = null;
    private SettingsActivity activity = null;

    public List<String> getSettings() {
        if (settings==null)
            settings = new ArrayList<String>();
        return settings;
    }

    public void setSettings(List<String> settings) {
        this.settings = settings;
    }

    public List<Integer> getImages() {
        return images;
    }

    public void setImages(List<Integer> images) {
        this.images = images;
    }

    public List<String> getInfo() {
        return info;
    }

    public void setInfo(List<String> info) {
        this.info = info;
    }

    public static class SettingsViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageSettings;
        public TextView nameSettings;
        public TextView infoSettings;
        public LinearLayout linearLayout;

        public SettingsViewHolder(View v) {
            super(v);
            imageSettings = (ImageView) v.findViewById(R.id.imageSettings);
            nameSettings = (TextView) v.findViewById(R.id.nameSettings);
            infoSettings = (TextView) v.findViewById(R.id.infoSettings);
            linearLayout = (LinearLayout) v.findViewById(R.id.layout);
        }
    }

    public SettingsAdapter(String[] items, Integer[] images, String[] info, Context context) {
        if (items != null)
            setSettings(new ArrayList<String>(Arrays.asList(items)));
        if (images != null)
            setImages(new ArrayList<Integer>(Arrays.asList(images)));
        if (info != null)
            setInfo(new ArrayList<String>(Arrays.asList(info)));
        Log.v("SettingsSize", String.valueOf(getSettings().size()));
        adapterContext = context;
        activity = (SettingsActivity) adapterContext;

    }

    @Override
    public SettingsAdapter.SettingsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_settings, parent, false);
        return new SettingsViewHolder(v);
    }

//    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final SettingsAdapter.SettingsViewHolder holder, final int position) {
        if (getSettings()!=null){
            if (getSettings().size()>0){
                holder.imageSettings.setImageDrawable(ContextCompat.getDrawable(adapterContext,getImages().get(position)));
                holder.nameSettings.setText(getSettings().get(position));
                holder.infoSettings.setText(getInfo().get(position));

                holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i;
                        switch (position){
                            case 0:
                                i = new Intent(adapterContext,BrowserSettings.class);
                                adapterContext.startActivity(i);
                                break;
                            case 1:
                                i = new Intent(adapterContext,UrlFilter.class);
                                adapterContext.startActivity(i);
                                break;
                        }
                    }
                });
            }
        }

    }

    @Override
    public int getItemCount() {
        return getSettings().size();
    }
}
