package com.aarr.drewpic.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.aarr.drewpic.Database.Dao.SubCategoriesDao;
import com.aarr.drewpic.Database.Model.BlacklistFilter;
import com.aarr.drewpic.Database.Model.Categories;
import com.aarr.drewpic.Database.Model.SubCategories;
import com.aarr.drewpic.R;
import com.aarr.drewpic.Services.Response.Categories.Data;
import com.aarr.drewpic.Views.SignupActivity;
import com.aarr.drewpic.Views.UrlFilter;
import com.alexzh.circleimageview.CircleImageView;
import com.alexzh.circleimageview.ItemSelectedListener;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by andresrodriguez on 11/21/16.
 */
public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {

    private Context adapterContext;
    private List<Categories> lstData = null;
    private Dialog dialog = null;
    private SignupActivity activity = null;
    private int type;

    public List<Categories> getLstData() {
        if (lstData==null){
            lstData = new ArrayList<Categories>();
        }
        return lstData;
    }

    public void setLstData(List<Categories> lstData) {
        this.lstData = lstData;
    }

    public static class CategoryViewHolder extends RecyclerView.ViewHolder {

//        public TextView idField;
        public TextView categoryName;
        public CircleImageView imgCategory;
        public RecyclerView recycler;
        public ImageButton btnBack;
        public RelativeLayout subCategoryContainer;

        public CategoryViewHolder(View v) {
            super(v);
            categoryName = (TextView) v.findViewById(R.id.categoryName);
            imgCategory = (CircleImageView) v.findViewById(R.id.imgCategory);
            recycler = (RecyclerView) v.findViewById(R.id.recycler);
            btnBack = (ImageButton) v.findViewById(R.id.btnBack);
            subCategoryContainer = (RelativeLayout) v.findViewById(R.id.subCategoryContainer);

        }
    }

    public CategoryAdapter(List<Categories> items, Context context) {
        setLstData(items);
        Log.v("DataListSize", String.valueOf(getLstData().size()));
        adapterContext = context;
    }

    @Override
    public CategoryAdapter.CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_item, parent, false);
        activity = (SignupActivity) adapterContext;
        return new CategoryViewHolder(v);
    }

    @Override
    public void onViewRecycled(CategoryViewHolder holder) {
        super.onViewRecycled(holder);
    }

    @Override
    public void onBindViewHolder(final CategoryAdapter.CategoryViewHolder holder, final int position) {
        if (getLstData()!=null){
            if (getLstData().size()>0){
                Log.v("Category","Category: "+getLstData().get(position).toString());


                Log.v("Category","Name: "+String.valueOf(getLstData().get(position).getCat()));
                Log.v("Category","Image: "+getLstData().get(position).getImage());

                holder.imgCategory.setImageDrawable(null);
                holder.imgCategory.setImageResource(0);
                holder.imgCategory.setImageURI(null);
                holder.imgCategory.setImageBitmap(null);

                holder.categoryName.setText(String.valueOf(getLstData().get(position).getCat()));

                if (getLstData().get(position).getImage()!=null){
                    Picasso
                            .with(adapterContext)
                            .load(getLstData().get(position).getImage())
                            .skipMemoryCache()
                            .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                            .into(holder.imgCategory);
                }

                holder.imgCategory.setOnItemSelectedClickListener(new ItemSelectedListener() {
                    @Override
                    public void onSelected(View view) {
                        holder.subCategoryContainer.setVisibility(View.VISIBLE);
                        holder.imgCategory.setVisibility(View.GONE);
                    }

                    @Override
                    public void onUnselected(View view) {
                        holder.subCategoryContainer.setVisibility(View.VISIBLE);
                        holder.imgCategory.setVisibility(View.GONE);
                    }
                });

                holder.btnBack.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.imgCategory.setSelected(false);
                        holder.imgCategory.setPressed(false);
                        holder.subCategoryContainer.setVisibility(View.GONE);
                        holder.imgCategory.setVisibility(View.VISIBLE);
                    }
                });

                List<SubCategories> subCats = new SubCategoriesDao().findSubCategoriesByCategory(getLstData().get(position).getId());
                if (subCats!=null && subCats.size()>0){
                    holder.recycler.setAdapter(null);
                    LinearLayoutManager lManager = new LinearLayoutManager(adapterContext);
                    holder.recycler.setLayoutManager(lManager);
                    SubCategoryAdapter adapter = new SubCategoryAdapter(subCats,adapterContext,this);
                    DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(holder.recycler.getContext(),
                            lManager.getOrientation());
                    holder.recycler.addItemDecoration(dividerItemDecoration);
                    holder.recycler.setAdapter(adapter);
                    RecyclerView.OnItemTouchListener mScrollTouchListener = new RecyclerView.OnItemTouchListener() {
                        @Override
                        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                            int action = e.getAction();
                            switch (action) {
                                case MotionEvent.ACTION_MOVE:
                                    rv.getParent().requestDisallowInterceptTouchEvent(true);
                                    break;
                            }
                            return false;
                        }

                        @Override
                        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

                        }

                        @Override
                        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

                        }
                    };

                    holder.recycler.addOnItemTouchListener(mScrollTouchListener);
                }
            }
        }

    }

    public String getCleanUrl(String url){
        url = url.replaceFirst("^(http://www\\.|http://|www\\.https://www\\.|https://|www\\.http://m\\.|http://|m\\.https://m\\.|https://|m\\.www\\.)","");
        return url;
    }

    @Override
    public int getItemCount() {
        return getLstData().size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
