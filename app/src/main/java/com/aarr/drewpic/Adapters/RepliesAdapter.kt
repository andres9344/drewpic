package com.aarr.drewpic.Adapters

import android.content.Context
import android.graphics.Color
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.aarr.drewpic.Database.Dao.RssFeedDao
import com.aarr.drewpic.Database.Dao.UserFriendsDao
import com.aarr.drewpic.Database.Model.FeedReplies
import com.aarr.drewpic.Database.Model.RssFeedModel
import com.aarr.drewpic.R
import com.aarr.drewpic.Util.DateManage
import com.aarr.drewpic.Views.Main2Activity
import com.bumptech.glide.Glide
import com.jackandphantom.circularimageview.CircleImage

/**
 * Created by andresrodriguez on 12/5/17.
 */
class RepliesAdapter(context: Context, items: List<FeedReplies>): RecyclerView.Adapter<RepliesAdapter.FeedsViewHolder>(){

    var rssItems: List<FeedReplies>? = null
        get() {
            if (field == null) {
                this.rssItems = ArrayList<FeedReplies>()
            }
            return field
        }
    var adapterContext: Context?=null
    var listener:IconClickListener?=null
    var lastPositionSelected = -1
    var nextIdSelected = -1
    var nextPosition = -1
    var thumbUpUnicode: Int = 0x1F44D
    var thumbDownUnicode: Int = 0x1F44E
    private val likeStatus = "Like"
    private val replyStatus = "Reply"

    class FeedsViewHolder(v: View) : RecyclerView.ViewHolder(v) {

        var imgFriend: CircleImage? = null
        var lblFriendName: TextView? = null
        var lblTime: TextView? = null
        var lblTotalLikes: TextView? = null
        var lblReactionType: TextView? = null
        var imgFeed: ImageView? = null
        var container: ConstraintLayout?=null

        init {
            imgFriend = v.findViewById(R.id.imgFriend)
            lblFriendName = v.findViewById(R.id.lblFriendName)
            lblTime = v.findViewById(R.id.lblTime)
            lblTotalLikes = v.findViewById(R.id.lblTotalLikes)
            lblReactionType = v.findViewById(R.id.lblReactionType)
            imgFeed = v.findViewById(R.id.imgFeed)
            container = v.findViewById(R.id.container)
        }
    }

    init {
        Log.v("BlacklistSize", items.size.toString())
        rssItems = items
        adapterContext = context
    }

    fun setCustomItemClickListener(listener:IconClickListener){
        this.listener = listener
    }

    interface IconClickListener {
        fun onItemClickListener(position:Int,feed:FeedReplies)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepliesAdapter.FeedsViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.replies_card_layout, parent, false)
        return FeedsViewHolder(v)
    }

    override fun onBindViewHolder(holder: RepliesAdapter.FeedsViewHolder, position: Int) {
        if (rssItems != null) {
            if (rssItems!!.isNotEmpty()) {
                try{
                    try{
                        holder.imgFriend?.setImageDrawable(null)
                        holder.imgFriend?.setImageResource(0)
                        holder.imgFriend?.setImageURI(null)
                        holder.imgFriend?.setImageBitmap(null)

                        holder.imgFeed?.setImageDrawable(null)
                        holder.imgFeed?.setImageResource(0)
                        holder.imgFeed?.setImageURI(null)
                        holder.imgFeed?.setImageBitmap(null)
                    }catch (e:Exception){
                        Log.e("CleanImageException",e.toString())
                    }

                    val feed = RssFeedDao().findRssFeedModelByServerId(rssItems!![position].serverPostId)
                    val friend = UserFriendsDao().getFriendByFacebookId(rssItems!![position].friendFacebookId)
                    val time = rssItems!![position].createdAt
                    val timeOld = DateManage().parseMillistoTimeInt(time)
                    val timeText = timeOld.toString()+" hours Ago"
                    var reaction = ""
                    when (rssItems!![position].status){
                        likeStatus->{
                            when (rssItems!![position].type){
                                "Right"-> reaction = "Likes your reaction"
                                "Left"-> reaction = "Dislikes your reaction"
                            }
                        }
                        replyStatus->{
                            reaction = "Replied your reaction"
                            holder.container!!.setOnClickListener {
                                val activity = adapterContext as Main2Activity
                                if (feed!=null){
                                    activity.showRepliedCard(feed!!,rssItems!![position])
                                }else{
                                    Toast.makeText(adapterContext,"This feed does not exist",Toast.LENGTH_SHORT).show()
                                }

                            }
                        }
                    }

                    try{
                        Glide
                                .with(adapterContext!!)
                                .load(friend!!.profilePicture?.replace("large","small"))
                                .into(holder.imgFriend!!)
                        Glide
                                .with(adapterContext!!)
                                .load(feed!!.image)
                                .into(holder.imgFeed!!)
                    }catch (e:Exception){
                        Log.e("RssImageException",e.toString())
                    }
                    holder.lblFriendName?.setText(friend!!.name)
                    holder.lblReactionType?.setText(reaction)
                    holder.lblTime?.setText(timeText)


                }catch (e:Exception){
                    Log.e("RssIconException",e.toString())
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return rssItems!!.size
    }

}