package com.aarr.drewpic.Adapters

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.icu.util.TimeUnit
import android.os.AsyncTask
import android.os.CountDownTimer
import android.support.constraint.ConstraintLayout
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.Spannable
import android.text.SpannableString
import android.text.TextWatcher
import android.text.style.ForegroundColorSpan
import android.text.style.UnderlineSpan
import android.util.Log
import android.util.Patterns
import android.util.TypedValue
import android.view.*
import android.webkit.*
import android.widget.*
import com.aarr.drewpic.Database.Dao.*
import com.aarr.drewpic.Database.Model.FeedReplies
import com.aarr.drewpic.Database.Model.PostImage
import com.aarr.drewpic.Database.Model.RssFeedModel
import com.aarr.drewpic.R
import com.aarr.drewpic.Services.Response.TweetsNews.GetTwitterNews
import com.aarr.drewpic.Util.DateManage
import com.aarr.drewpic.Util.HtmlUtil
import com.aarr.drewpic.Util.RandomValues
import com.aarr.drewpic.Views.Main2Activity
import com.bumptech.glide.Glide
import com.daasuu.bl.BubbleLayout
import com.google.gson.Gson
import com.jackandphantom.circularimageview.CircleImage
import io.square1.richtextlib.ui.RichContentView
import io.square1.richtextlib.v2.RichTextV2
import io.square1.richtextlib.v2.content.RichTextDocumentElement
import kotlinx.android.synthetic.main.activity_profile.*
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.net.URL
import java.nio.charset.Charset
import java.util.*
import java.util.regex.Pattern


/**
 * Created by andresrodriguez on 12/4/17.
 */
class RssFeedAdapter(context: Context, reply:FeedReplies?): ArrayAdapter<RssFeedModel>(context,0){

    var listener: RssFeedAdapter.ShareButtonListener?=null
    var feedListener: RssFeedAdapter.FeedButtonListener?=null
    var isSearch:Boolean=false
    var async: AsyncSaveImage?=null
    var thumbUpUnicode: Int = 0x1F44D
    var thumbDownUnicode: Int = 0x1F44E
    var thumbUpUnicodeParsed = "128077"
    var thumbDownUnicodeParsed = "128078"
    var thumbUpUnicodeString = "0x1F44D"
    var thumbDownUnicodeString = "0x1F44E"
    val worldUnicode: Int = 0x1F30E
    var replyValue: FeedReplies?=null
    var currentViewHolder: ViewHolder?=null
    var activity:Main2Activity?=null
    var alreadyLoaded:Boolean=false
    val ACTION_REVERSE = 101
    val ACTION_DISLIKE = 202
    val ACTION_LIKE = 303

    init {
        if (context is Main2Activity)
            activity = context as Main2Activity
        replyValue = reply
        Log.v("ShowingCard","Init reply value: "+replyValue.toString())
    }

    fun setAdapterType(search:Boolean){
        isSearch = search
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var holder: ViewHolder? = null

        var contentView = convertView
        if (contentView==null){
            Log.e("FeedAdapter","Content view is null")
            var inflater = LayoutInflater.from(context)
            contentView = inflater.inflate(R.layout.feed_item_new,parent,false)
            holder = ViewHolder(contentView)
            contentView.tag = holder
        }else{
            Log.e("FeedAdapter","Content view is not null")
            holder = contentView.tag as ViewHolder
        }
        currentViewHolder = holder

        var item:RssFeedModel = getItem(position)
        Log.v("ShowingCard","Feed: "+item.toString())
//        try{
//            var service = GetImagesService(context)
//            service.getImages(item.title)
//        }catch (e:Exception){
//            Log.e("ImageResponse",e.toString())
//        }

//        val settings = SettingsDao().getSettings()
//        if (settings?.showDislike!!){
//            holder!!.bubbleContainer?.visibility = View.VISIBLE
//            holder!!.txtBubble?.setText("Click here to dislike")
//            holder!!.bubbleLayout?.arrowPosition = 100f
//            holder.bubbleContainer!!.setOnClickListener {
//                settings?.showDislike = false
//                SettingsDao().updateOrCreate(settings)
//                if (settings?.showLike!!){
//                    holder!!.bubbleLayout?.arrowPosition = 400f
//                    holder!!.txtBubble?.setText("Click here to like")
//                    holder!!.bubbleContainer!!.setOnClickListener {
//                        activity?.resetCards = true
//                        settings?.showLike = false
//                        SettingsDao().updateOrCreate(settings)
//                        holder!!.bubbleContainer?.visibility = View.GONE
//                    }
//                }else{
//                    holder!!.bubbleContainer?.visibility = View.GONE
//                }
//            }
//        }else if(settings?.showLike){
//            holder!!.bubbleContainer?.visibility = View.VISIBLE
//            holder!!.bubbleLayout?.arrowPosition = 400f
//            holder!!.txtBubble?.setText("Click here to like")
//            holder.bubbleContainer!!.setOnClickListener {
//                activity?.resetCards = true
//                settings?.showLike = false
//                SettingsDao().updateOrCreate(settings)
//                holder!!.bubbleContainer?.visibility = View.GONE
//            }
//        }

        if (item.isTrend){
            holder.rssTitle?.setText("Trending Searches")
//            val content = SpannableString(item.keyword)
//            content.setSpan(UnderlineSpan(), 0, content.length, 0)
            holder.txtKeyword?.visibility = View.GONE
            holder.txtKeyword?.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG)
        }else if (isSearch){
            holder.rssTitle?.setText("Search results for")
            val searchValue = activity?.searchValue?:""
            holder.txtKeyword?.visibility = View.VISIBLE
//            val content = SpannableString(searchValue)
//            content.setSpan(UnderlineSpan(), 0, content.length, 0)
            holder.txtKeyword?.setText("'"+searchValue+"'")
            holder.txtKeyword?.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG)
        }else{
            holder.rssTitle?.setText("Today's top stories in")
            holder.txtKeyword?.visibility = View.VISIBLE
//            val content = SpannableString(item.keyword)
//            content.setSpan(UnderlineSpan(), 0, content.length, 0)
            holder.txtKeyword?.setText("'"+item.keyword+"'")
            holder.txtKeyword?.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG)
        }

        var countDown: CountDownTimer?=null
        if (item.isSuggestion){
            alreadyLoaded = false
            holder.feedInformacion!!.visibility = View.GONE
//            holder.reachedContainer!!.visibility = View.GONE
            holder.btnReverse!!.visibility = View.INVISIBLE
            holder.btnReached!!.visibility = View.INVISIBLE
            holder.countAgree!!.visibility = View.INVISIBLE
            holder.txtPostTime!!.visibility = View.GONE
            holder.feedContent!!.visibility = View.GONE
            holder.lblTitle!!.visibility = View.GONE
            holder.webView!!.visibility = View.VISIBLE

            loadWebView(holder.webView!!,item.keyword)
        }else if (item.isTrend){
            val random:Long = java.util.concurrent.TimeUnit.MINUTES.toMillis(RandomValues().getRandom(20,60).toLong())
            alreadyLoaded = false
            holder.feedInformacion!!.visibility = View.GONE
//            holder.reachedContainer!!.visibility = View.GONE
            holder.btnReverse!!.visibility = View.INVISIBLE
            holder.btnReached!!.visibility = View.INVISIBLE
            holder.countAgree!!.visibility = View.INVISIBLE
            holder.txtPostTime!!.visibility = View.GONE
            holder.feedContent!!.visibility = View.GONE
            holder.lblTitle!!.visibility = View.VISIBLE
            val richText = RichTextDocumentElement
                    .TextBuilder("Trending searches in the past ")
                    .color(ContextCompat.getColor(context,android.R.color.black))
                    .center()
                    .bold()
                    .append(parseMillisToTime(random))
                    .color(ContextCompat.getColor(context,android.R.color.holo_blue_dark))
                    .center()
                    .underline(true)
                    .bold()
                    .build()
//            holder.lblTitle!!.setTextColor(ContextCompat.getColor(context,android.R.color.black))
//            holder.lblTitle!!.setText("Trending searches in the past "+parseMillisToTime(random))
            holder.lblTitle!!.setText(richText)
            countDown = initCountDown(random,holder.lblTitle!!)
            holder.webView!!.visibility = View.VISIBLE

            loadTrendWebView(holder.webView!!)
        }else{
            holder.btnReverse!!.visibility = View.VISIBLE
            holder.btnReached!!.visibility = View.VISIBLE
            holder.countAgree!!.visibility = View.VISIBLE
            holder.feedInformacion!!.visibility = View.VISIBLE
//            holder.reachedContainer!!.visibility = View.VISIBLE
            holder.feedContent!!.visibility = View.VISIBLE
            holder.txtPostTime!!.visibility = View.VISIBLE
            holder.lblTitle!!.visibility = View.VISIBLE
            val richText = RichTextDocumentElement
                    .TextBuilder(item.title.replace("&#39;","'"))
                    .color(ContextCompat.getColor(context,R.color.drewpicColor))
                    .bold()
                    .center()
                    .build()
//            holder.lblTitle!!.setTextColor(ContextCompat.getColor(context,R.color.drewpicColor))
            holder.webView!!.visibility = View.GONE
            val stringTime = DateManage().parseMillistoTimeString(item.publishDate)
            holder.txtPostTime!!.setText(stringTime+"h")

            holder.lblTitle!!.setText(richText)

            val descriptionLength = item.description.length
            var txtSize = "25"
            when {
//                descriptionLength<=30 -> txtSize = "25"
                descriptionLength<=100 -> txtSize = "20"
                descriptionLength<=200 -> txtSize = "15"
                else -> txtSize = "12"
            }

//            val description = item.description.replace("&#39;","'")
            val url = URL(item.link)
            val host = url.host.split(".")
            var domain = ""
            if (host.isNotEmpty())
                if (host.size==2){
                    domain = host[0]
                }else if (host.size>2){
                    domain = host[1]
                }
            val description = item.description
//            val finalText = RichTextV2.textFromHtml(context,description)
            val finalText = HtmlUtil().getHtmlString(description,domain,txtSize)
//            val finalText = description
//            try{
//                checkUrlFromString2(description, holder.feedContent!!,item.link)
//            }catch (e:Exception){
//                Log.e("PatternMatch","Exception: "+e.toString())
//            }
//            holder.feedContent?.setText(description,TextView.BufferType.SPANNABLE)
//            holder.feedContent?.setText(finalText)
            Log.v("HtmlText","Loading: "+finalText)
            holder.feedContent?.webViewClient = object:WebViewClient(){
                override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                    val value = super.shouldOverrideUrlLoading(view, request)

                    Log.e("HtmlText","A tag clicked")


                    return value
                }

                override fun shouldOverrideKeyEvent(view: WebView?, event: KeyEvent?): Boolean {
                    val value = super.shouldOverrideKeyEvent(view, event)

                    Log.e("HtmlText","A tag clicked 2")

                    return value
                }
            }
            holder.feedContent?.setOnTouchListener(object:View.OnTouchListener{
                override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
                    var result = (p0 as WebView).hitTestResult
                    if (result.type == 8 && result.extra.contains("globe2")){
                        Log.e("HtmlText","Extra: "+result.extra)
                        Log.e("HtmlText","Type: "+result.type)
                        activity!!.showWebViewDialog(item.link)
                    }
                    return false
                }

            })
            holder.feedContent?.loadDataWithBaseURL("file:///android_asset/", finalText, "text/html", "UTF-8", "")
//            holder.feedContent?.setOnClickListener {
//                activity!!.showWebViewDialog(item.link)
//            }
            Log.e("FeedContent",description)
//        holder.rssTitle?.setText(item.title.replace("&apos;", "'"))


            Log.e("CardLoadingImage","Keyword: "+item.keyword+", Image: "+item.image)
            try{
                Glide.with(context).load(item.image).into(holder.feedImage!!)
            }catch (e:Exception){
                Log.e("LoadImageException",e.toString())
            }

            val likes = FeedRepliesDao().getLikesCountByPost(item.idServer)
            val dislikes = FeedRepliesDao().getDislikesCountByPost(item.idServer)

//        holder.countReached?.setText(UserFriendsDao().getFriendsCount().toString())
//        holder.countAgree?.setText(likes.toString())
//        holder.countDisagree?.setText(dislikes.toString())

            val random = RandomValues()
            val totalReached = random.reached+UserFriendsDao().getFriendsCount()
            val totalAgree = random.agree+likes
            val totalDisagree = random.disagree+dislikes


            holder.countReached?.setText(totalReached.toString())
            holder.countAgree?.setText(totalAgree.toString())
//            holder.countDisagree?.setText(totalDisagree.toString())
            holder.txtPostComment!!.setTag(item.idMobile)

            if (item.isFriends){
                Log.v("ShowingCard","Item is friend, no reply value available")
                holder.commentContainer2?.visibility = View.VISIBLE
                holder.txtPostComment?.isEnabled = false
                val txtWatcher2 = object:TextWatcher{
                    override fun afterTextChanged(p0: Editable?) {
                        val txtValue = p0.toString()
                        if (activity!=null)
                            activity!!.setFriendCaption(txtValue)
                        Log.v("Comment","Text: "+txtValue)
                        /*val bitmap = viewToBitmap(holder!!.feedContainer!!)
//                saveTempImg(item.idMobile,bitmap)
                        if (async!=null)
                            async!!.cancel(true)
                        async = AsyncSaveImage(item.idMobile,bitmap)
                        async!!.execute()*/

                    }

                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    }

                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    }

                }

                val user = UserFriendsDao().getFriendByFacebookId(item.friendsId)
                try{
                    Log.e("ProfilePicture",user?.profilePicture?.replace("large","small"))
                    Glide.with(context).load(user?.profilePicture?.replace("large","small")).into(holder.imgProfile!!)
                }catch (e:Exception){
                    Log.e("LoadImageException",e.toString())
                }

                val myUser = UserDao().findCurrentUser()
                try{
                    Log.e("ProfilePicture",myUser?.profilePicture?.replace("large","small"))
                    Glide.with(context).load(myUser?.profilePicture?.replace("large","small")).into(holder.imgProfile2!!)
                }catch (e:Exception){
                    Log.e("LoadImageException",e.toString())
                }

                var finalCaption = checkUnicodeInString(item.caption)

                holder.txtPostComment?.setText(finalCaption)
                holder.txtPostComment2?.addTextChangedListener(txtWatcher2)
                holder.txtPostComment2?.requestFocus()
                holder.txtPostComment2?.isActivated = true
                holder.txtPostComment2?.isPressed = true
                holder.txtPostComment2?.requestFocus()

                holder.btnDislike!!.setOnClickListener {
                    if (feedListener!=null){
                        blockButtons(holder!!)
//                        val bitmap = viewToBitmap(holder!!.feedContainer!!)
                        holder!!.txtPostComment2!!.removeTextChangedListener(txtWatcher2)
                        feedListener!!.onFeedButtonClickListener(null,position,item,ACTION_DISLIKE,holder!!.txtPostComment2!!,holder!!.feedContainer!!)
                        if (countDown!=null){
                            countDown?.cancel()
                        }
                    }
                }

                holder.btnLike!!.setOnClickListener {
                    if (feedListener!=null){
                        blockButtons(holder!!)
//                        val bitmap = viewToBitmap(holder!!.feedContainer!!)
                        holder!!.txtPostComment2!!.removeTextChangedListener(txtWatcher2)
                        feedListener!!.onFeedButtonClickListener(null,position,item,ACTION_LIKE,holder!!.txtPostComment2!!,holder!!.feedContainer!!)
                        if (countDown!=null){
                            countDown?.cancel()
                        }
                    }
                }
                holder.btnReverse!!.setOnClickListener {
                    if (feedListener!=null){
                        blockButtons(holder!!)
//                        val bitmap = viewToBitmap(holder!!.feedContainer!!)
                        holder!!.txtPostComment2!!.removeTextChangedListener(txtWatcher2)
                        feedListener!!.onFeedButtonClickListener(null,position,item,ACTION_REVERSE,holder!!.txtPostComment!!,holder!!.feedContainer!!)
                        if (countDown!=null){
                            countDown?.cancel()
                        }
                    }
                }

            }else{

                val user = UserDao().findCurrentUser()
                try{
                    Log.e("ProfilePicture",user?.profilePicture?.replace("large","small"))
                    Glide.with(context).load(user?.profilePicture?.replace("large","small")).into(holder.imgProfile!!)
                }catch (e:Exception){
                    Log.e("LoadImageException",e.toString())
                }

                if (replyValue!=null){
                    Log.v("ShowingCard","Reply value is not null")
                    val friend = UserFriendsDao().getFriendByFacebookId(replyValue!!.friendFacebookId)


                    holder.commentContainer2!!.visibility = View.VISIBLE

                    Log.v("ShowingCard","Caption 1: "+item.caption)
                    val finalCaption1 = checkUnicodeInString(item.caption)

                    Log.v("ShowingCard","Caption 2: "+replyValue!!.caption)
                    val finalCaption2 = checkUnicodeInString(replyValue!!.caption)

                    Log.v("ShowingCard","Final Caption 1: "+finalCaption1)
                    Log.v("ShowingCard","Final Caption 2: "+finalCaption2)
                    holder.txtPostComment!!.setText(finalCaption1)
                    holder.txtPostComment2!!.setText(finalCaption2)
                    holder.txtPostComment!!.isEnabled = false
                    holder.txtPostComment2!!.isEnabled = false

                    try{
                        Log.e("ProfilePicture",friend?.profilePicture?.replace("large","small"))
                        Glide.with(context).load(friend?.profilePicture?.replace("large","small")).into(holder.imgProfile2!!)
                    }catch (e:Exception){
                        Log.e("LoadImageException",e.toString())
                    }
                }else{
                    holder.commentContainer2?.visibility = View.GONE
                    holder.txtPostComment?.isEnabled = true
                    val txtWatcher = object:TextWatcher{
                        override fun afterTextChanged(p0: Editable?) {
                            val txtValue = p0.toString()
                            updateCaptionValue(item,txtValue)
                            Log.v("Comment","Text: "+txtValue)
                            /*val bitmap = viewToBitmap(holder!!.feedContainer!!)
//                saveTempImg(item.idMobile,bitmap)
                            if (async!=null)
                                async!!.cancel(true)
                            async = AsyncSaveImage(item.idMobile,bitmap)
                            async!!.execute()*/

                        }

                        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                        }

                        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                        }

                    }

                    Log.v("ShowingCard","Reply value is null")
                    holder.txtPostComment?.setText("")
                    holder.txtPostComment?.addTextChangedListener(txtWatcher)
                    holder.txtPostComment?.requestFocus()
                    holder.txtPostComment?.isActivated = true
                    holder.txtPostComment?.isPressed = true

                    holder.btnDislike!!.setOnClickListener {
                        if (feedListener!=null){
                            blockButtons(holder!!)
                            holder!!.txtPostComment!!.removeTextChangedListener(txtWatcher)
                            val bitmap = viewToBitmap(holder!!.feedContainer!!)
                            feedListener!!.onFeedButtonClickListener(bitmap,position,item,ACTION_DISLIKE,holder!!.txtPostComment!!,holder!!.feedContainer!!)
                            if (countDown!=null){
                                countDown?.cancel()
                            }
                        }
                    }

                    holder.btnLike!!.setOnClickListener {
                        if (feedListener!=null){
                            blockButtons(holder!!)
                            holder!!.txtPostComment!!.removeTextChangedListener(txtWatcher)
                            val bitmap = viewToBitmap(holder!!.feedContainer!!)
                            feedListener!!.onFeedButtonClickListener(bitmap,position,item,ACTION_LIKE,holder!!.txtPostComment!!,holder!!.feedContainer!!)
                            if (countDown!=null){
                                countDown?.cancel()
                            }
                        }
                    }

                    holder.btnReverse!!.setOnClickListener {
                        if (feedListener!=null){
                            blockButtons(holder!!)
                            holder!!.txtPostComment!!.removeTextChangedListener(txtWatcher)
                            val bitmap = viewToBitmap(holder!!.feedContainer!!)
                            feedListener!!.onFeedButtonClickListener(bitmap,position,item,ACTION_REVERSE,holder!!.txtPostComment!!,holder!!.feedContainer!!)
                            if (countDown!=null){
                                countDown?.cancel()
                            }
                        }
                    }
                }
            }
        }

        return contentView!!
    }

    fun blockButtons(holder: ViewHolder){
        holder.btnLike?.isEnabled = false
        holder.btnDislike?.isEnabled = false
        holder.btnReverse?.isEnabled = false
        object:CountDownTimer(2000,1000){
            override fun onFinish() {
                holder.btnLike?.isEnabled = true
                holder.btnDislike?.isEnabled = true
                holder.btnReverse?.isEnabled = true
            }

            override fun onTick(p0: Long) {
            }

        }.start()
    }

    fun initCountDown(random:Long,txtView:RichContentView):CountDownTimer{
        val countDown = object:CountDownTimer(random,1000){
            override fun onFinish() {
                val newRandom:Long = java.util.concurrent.TimeUnit.MINUTES.toMillis(RandomValues().getRandom(20,60).toLong())
                initCountDown(newRandom,txtView)
            }

            override fun onTick(milli: Long) {
                val richText = RichTextDocumentElement
                        .TextBuilder("Trending searches in the past ")
                        .color(ContextCompat.getColor(context,android.R.color.black))
                        .append(parseMillisToTime(milli))
                        .color(ContextCompat.getColor(context,android.R.color.holo_blue_dark))
                        .build()
//                txtView.setText("Trending searches in the past "+parseMillisToTime(milli))
                txtView.setText(richText)
            }

        }
        countDown.start()
        return countDown
    }

    fun parseMillisToTime(milli:Long):String{
        val Minutes = milli / (60 * 1000) % 60
        val Seconds = milli / 1000 % 60
        val count = Minutes.toString()+":"+Seconds.toString()
        return count
    }

    fun loadWebView(webLoader:WebView,keyword: String){
        webLoader.getSettings().javaScriptEnabled = true

        /*webLoader.webViewClient = object: WebViewClient(){
            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                Log.v("WebView","Page Finished")
                reloadWebView(keyword,view!!)
            }
        }*/
        val postData = "keywords="+keyword
        val charset = Charset.forName("UTF-8")
        webLoader.postUrl(
                "http://www.drewpic.net/index2",
                postData.toByteArray(charset)
                )

//        webLoader.loadUrl("http://www.drewpic.net/index2")
    }

    fun loadTrendWebView(webLoader:WebView){
        webLoader.getSettings().javaScriptEnabled = true

        val country = SettingsDao().getSettings()?.country?.toLowerCase()?.replace(" ","-")
        Log.v("CountryTrend",country)
        val postData = "country="+country
        val charset = Charset.forName("UTF-8")
        webLoader.postUrl(
                "http://www.drewpic.net/apis/trends_lastest",
                postData.toByteArray(charset)
        )
    }

    fun reloadWebView(keyword:String,webLoader:WebView){
        if (webLoader!=null){
            webLoader.clearCache(true)
            webLoader.clearHistory()
            val loadJavascript = "javascript:searchWith('"+keyword+"')"
            Log.v("WebLoader","Loading Javascript: "+loadJavascript)
            if (!alreadyLoaded){
                webLoader!!.loadUrl(loadJavascript)
                alreadyLoaded = true
            }

        }else{
            Log.e("WebLoader","Web Loader is null")
        }
    }

    fun checkUnicodeInString(text:String):String{
        var finalString = text
        when {
            text.contains(thumbUpUnicodeParsed) -> finalString = text.replace(thumbUpUnicodeParsed,getEmojiByUnicode(thumbUpUnicode))
            text.contains(thumbDownUnicodeParsed) -> finalString = text.replace(thumbDownUnicodeParsed,getEmojiByUnicode(thumbDownUnicode))
            text.contains(thumbUpUnicodeString) -> finalString = text.replace(thumbUpUnicodeString,getEmojiByUnicode(thumbUpUnicode))
            text.contains(thumbDownUnicodeString) -> finalString = text.replace(thumbDownUnicodeString,getEmojiByUnicode(thumbDownUnicode))
        }
        return finalString
    }

    fun updateCaptionValue(feed:RssFeedModel,caption:String){
        feed.caption = caption
        RssFeedDao().updateOrCreate(feed)
    }

    class AsyncSaveImage(id: Int, image: Bitmap): AsyncTask<String, Void, String>() {

        private var postId = -1
        private var img: Bitmap?=null

        init {
            postId = id
            img = image
        }

        override fun doInBackground(vararg p0: String?): String {
            saveTempImg(postId,img!!)
            return ""
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
        }

        fun saveTempImg(postId:Int,img:Bitmap){
            val encoded = encodeToBase64(img, Bitmap.CompressFormat.PNG,100)
            var postImage = PostImagesDao().findPostImageByPostId(postId)
            if (postImage!=null){
                postImage.image = encoded
                PostImagesDao().updateOrCreate(postImage)
            }else{
                postImage = PostImage(-1,postId,encoded)
                PostImagesDao().updateOrCreate(postImage)
            }
        }

        fun encodeToBase64(image: Bitmap, compressFormat: Bitmap.CompressFormat, quality: Int): String {
            val byteArrayOS = ByteArrayOutputStream()
            image.compress(compressFormat, quality, byteArrayOS)
            return android.util.Base64.encodeToString(byteArrayOS.toByteArray(), android.util.Base64.DEFAULT)
        }

    }

    fun viewToBitmap(view:View):Bitmap {
        var bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888)
        var canvas = Canvas(bitmap)
        view.draw(canvas)
        return bitmap
    }

    fun setShareButtonListener(listener:ShareButtonListener){
        this.listener = listener
    }

    interface ShareButtonListener {
        fun onShareButtonClickListener(img: Bitmap, position:Int, feed:RssFeedModel)
    }

    fun setFeedButtonListener(listener:FeedButtonListener){
        feedListener = listener
    }

    interface FeedButtonListener {
        fun onFeedButtonClickListener(img: Bitmap?, position:Int, feed:RssFeedModel, type:Int, comment: TextView, view:View)
    }

    override fun setNotifyOnChange(notifyOnChange: Boolean) {
        super.setNotifyOnChange(notifyOnChange)
        Log.e("FeedAdapter","NotifyChange, count: "+count)
    }

    class ViewHolder(view: View) {
        var countReached: TextView? = null
        var countAgree: TextView? = null
//        var countDisagree: TextView? = null
//        var feedContent: TextView? = null
//        var feedContent: RichContentView? = null
        var feedContent: WebView? = null
        var feedContainer: ConstraintLayout? = null
        var rssTitle: TextView? = null
        var feedImage: ImageView? = null
        var imgCategory: CircleImage? = null
        var btnShare: ImageView? = null
//        var imgShares: ImageView? = null
//        var imgLikes: ImageView? = null
//        var imgDislikes: ImageView? = null
        var feedHeader: ConstraintLayout? = null
        var imgProfile:CircleImage?=null
        var txtPostComment: EditText?=null
        var imgProfile2:CircleImage?=null
        var txtPostComment2: EditText?=null
        var txtPostTime: TextView?=null
//        var lblTitle: TextView?=null
        var lblTitle: RichContentView?=null
        var txtKeyword: TextView?=null
        var commentContainer2: RelativeLayout?=null
        var webView: WebView?=null
        var reachedContainer: LinearLayout?=null
        var btnReverse: LinearLayout?=null
        var btnDislike: LinearLayout?=null
        var btnLike: LinearLayout?=null
        var btnReached: LinearLayout?=null
        var feedInformacion: RelativeLayout?=null
        var bubbleLayout: BubbleLayout?=null
        var txtBubble: TextView?=null
        var bubbleContainer: ConstraintLayout?=null

        init {
            this.countReached = view.findViewById<TextView>(R.id.countReached) as TextView
            this.countAgree = view.findViewById<TextView>(R.id.countAgree) as TextView
//            this.countDisagree = view.findViewById<TextView>(R.id.countDisagree) as TextView
            this.feedContent = view.findViewById(R.id.feedContent)
            this.rssTitle = view.findViewById(R.id.rssTitle)
            this.imgCategory = view.findViewById(R.id.imgCategory)
            this.feedImage = view.findViewById<ImageView>(R.id.feedImage) as ImageView
            this.btnShare = view.findViewById(R.id.btnShare)
            this.feedContainer = view.findViewById(R.id.feedContainer)
            this.feedHeader = view.findViewById(R.id.feedHeader)
//            this.imgShares = view.findViewById(R.id.imgShares)
//            this.imgLikes = view.findViewById(R.id.imgLikes)
//            this.imgDislikes = view.findViewById(R.id.imgDislikes)
            this.imgProfile = view.findViewById(R.id.imgProfile)
            this.imgProfile2 = view.findViewById(R.id.imgProfile2)
            this.txtPostComment = view.findViewById(R.id.txtPostComment)
            this.txtPostComment2 = view.findViewById(R.id.txtPostComment2)
            this.lblTitle = view.findViewById(R.id.lblTitle)
            this.txtPostTime = view.findViewById(R.id.txtPostTime)
            this.txtKeyword = view.findViewById(R.id.txtKeyword)
            this.commentContainer2 = view.findViewById(R.id.commentContainer2)
            this.webView = view.findViewById(R.id.webView)
            this.btnReverse = view.findViewById(R.id.btnReverse)
            this.btnDislike = view.findViewById(R.id.btnDislike)
            this.btnLike = view.findViewById(R.id.btnLike)
            this.btnReached = view.findViewById(R.id.btnReached)
            this.reachedContainer = view.findViewById(R.id.reachedLayout)
            this.feedInformacion = view.findViewById(R.id.feedInformation)
            this.bubbleLayout = view.findViewById(R.id.bubbleLayout)
            this.txtBubble = view.findViewById(R.id.txtBubble)
            this.bubbleContainer = view.findViewById(R.id.bubbleContainer)
        }


    }

    fun getEmojiByUnicode(unicode: Int): String {
        return String(Character.toChars(unicode))
    }

    fun checkUrlFromString(text:String){
        val URL_REGEX = "^((https?|ftp)://|(www|ftp)\\.)?[a-z0-9-]+(\\.[a-z0-9-]+)+([/?].*)?\$"

        val p = Pattern.compile(URL_REGEX)
        val m = p.matcher(text)//replace with string to compare
        if (m.find(0)) {
            Log.v("PatternMatch","Group Count: "+m.groupCount())
            Log.v("PatternMatch","Group Print: "+m.group())
        }
    }

    fun checkUrlFromString2(text:String, textView:TextView, link:String){
        if (text.contains(".com")){
            val splited = text.split(" ")
            var newText = ""
            var oldText = ""
            if (splited!=null && splited.isNotEmpty()){
                Log.v("PatternMatch","Group Print: "+splited[splited.size-1])
                newText = getEmojiByUnicode(worldUnicode) + splited[splited.size-1]
                oldText = text.replace(splited[splited.size-1],"")
            }
            Log.v("PatternMatch","New Text: "+newText)
            Log.v("PatternMatch","Old Text: "+oldText)
            val finalText = oldText + newText
            Log.v("PatternMatch","Final Text: "+finalText)
            textView.setText(finalText, TextView.BufferType.SPANNABLE)
            val s = textView.getText() as Spannable
            val start = oldText.length
            val end = start + newText.length
            s.setSpan(ForegroundColorSpan(ContextCompat.getColor(context,R.color.drewpicColor)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

            textView.setOnClickListener {
                activity!!.showWebViewDialog(link)
            }
        }else{
            textView.setText(text)
            textView.setOnClickListener(null)
        }
    }


}

