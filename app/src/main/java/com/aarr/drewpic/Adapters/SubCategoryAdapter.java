package com.aarr.drewpic.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aarr.drewpic.Database.Model.SubCategories;
import com.aarr.drewpic.Database.Model.SubCategoryModel;
import com.aarr.drewpic.R;
import com.aarr.drewpic.Services.Response.Categories.Data;
import com.aarr.drewpic.Views.SignupActivity;
import com.alexzh.circleimageview.CircleImageView;
import com.alexzh.circleimageview.ItemSelectedListener;
import com.davidecirillo.multichoicerecyclerview.MultiChoiceAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andresrodriguez on 11/21/16.
 */
public class SubCategoryAdapter extends MultiChoiceAdapter<SubCategoryAdapter.SubCategoryViewHolder> {

    private Context adapterContext;
    private List<SubCategories> lstData = null;
    private Dialog dialog = null;
    private SignupActivity activity = null;
    private CategoryAdapter parentAdapter = null;
    private int type;

    public List<SubCategories> getLstData() {
        if (lstData==null){
            lstData = new ArrayList<SubCategories>();
        }
        return lstData;
    }

    public void setLstData(List<SubCategories> lstData) {
        this.lstData = lstData;
    }

    public static class SubCategoryViewHolder extends RecyclerView.ViewHolder {

//        public TextView idField;
        public TextView nameCategory;
        public LinearLayout itemContainer;

        public SubCategoryViewHolder(View v) {
            super(v);
            nameCategory = (TextView) v.findViewById(R.id.nameCategory);
            itemContainer = (LinearLayout) v.findViewById(R.id.itemContainer);

        }
    }

    public SubCategoryAdapter(List<SubCategories> items, Context context, CategoryAdapter parentAdapter) {
        setLstData(items);
        Log.v("DataListSize", String.valueOf(getLstData().size()));
        adapterContext = context;
        this.parentAdapter = parentAdapter;
    }

    @Override
    public SubCategoryAdapter.SubCategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_sub_category, parent, false);
        activity = (SignupActivity) adapterContext;
        return new SubCategoryViewHolder(v);
    }

//    @Override
//    public void setActive(@NonNull View view, boolean state) {
//
//        LinearLayout container = (LinearLayout) view.findViewById(R.id.itemContainer);
//        if (state) {
//            Log.v("ItemClick","Is Selected");
//            container.setBackgroundColor(ContextCompat.getColor(adapterContext, R.color.colorPrimaryDark));
//        } else {
//            Log.v("ItemClick","Is Deselected");
//            container.setBackgroundColor(ContextCompat.getColor(adapterContext, android.R.color.transparent));
//        }
//    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    protected View.OnClickListener defaultItemViewClickListener(final SubCategoryAdapter.SubCategoryViewHolder holder, final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] subCat = getLstData().get(position).getSubCat().split("#");
                int subCatId = Integer.parseInt(subCat[0]);
                SubCategoryModel item = new SubCategoryModel(subCatId,subCat[1]);
                Log.v("ItemClick","Check item:  "+item.toString());
                boolean isSelected = activity.subCategoryIsSelected(item);
                if (isSelected){
                    activity.removeSubCategory(item);
                    holder.itemContainer.setBackgroundColor(ContextCompat.getColor(adapterContext, android.R.color.transparent));
                }else{
                    activity.addSubCategory(item);
                    holder.itemContainer.setBackgroundColor(ContextCompat.getColor(adapterContext, R.color.colorAccent));
                }
                Log.v("ItemClick","Clicked "+position);
//                Toast.makeText(adapterContext, "Click on item " + position, Toast.LENGTH_SHORT).show();
            }
        };
    }

    @Override
    public void onBindViewHolder(final SubCategoryAdapter.SubCategoryViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);
        if (getLstData()!=null){
            if (getLstData().size()>0){
                List<SubCategoryModel> lst = activity.getLstSelected();
                if (lst!=null && lst.size()>0){
                    String[] subCat = getLstData().get(position).getSubCat().split("#");
                    int subCatId = Integer.parseInt(subCat[0]);
                    SubCategoryModel item = new SubCategoryModel(subCatId,subCat[1]);
                    Log.v("ItemClick","Check item:  "+item.toString());
                    boolean isSelected = activity.subCategoryIsSelected(item);
                    if (isSelected){
                        holder.itemContainer.setBackgroundColor(ContextCompat.getColor(adapterContext, R.color.colorAccent));
                    }else{
                        holder.itemContainer.setBackgroundColor(ContextCompat.getColor(adapterContext, android.R.color.transparent));
                    }
                }
                String[] subCat = getLstData().get(position).getSubCat().split("#");
                holder.nameCategory.setText(subCat[1]);

            }
        }

    }

    public String getCleanUrl(String url){
        url = url.replaceFirst("^(http://www\\.|http://|www\\.https://www\\.|https://|www\\.http://m\\.|http://|m\\.https://m\\.|https://|m\\.www\\.)","");
        return url;
    }

    @Override
    public int getItemCount() {
        return getLstData().size();
    }
}
