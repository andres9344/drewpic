package com.aarr.drewpic.Adapters

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.aarr.drewpic.Database.Dao.UserFriendsDao
import com.aarr.drewpic.Database.Model.RssFeedModel
import com.aarr.drewpic.R
import com.bumptech.glide.Glide
import com.jackandphantom.circularimageview.CircleImage

/**
 * Created by andresrodriguez on 12/5/17.
 */
class FeedsAdapter(context: Context, items: List<RssFeedModel>, active:Int ): RecyclerView.Adapter<FeedsAdapter.FeedsViewHolder>(){

    var rssItems: List<RssFeedModel>? = null
        get() {
            if (field == null) {
                this.rssItems = ArrayList<RssFeedModel>()
            }
            return field
        }
    var adapterContext: Context?=null
    var listener:IconClickListener?=null
    var lastPositionSelected = 0
    var nextIdSelected = -1
    var nextPosition = -1

    class FeedsViewHolder(v: View) : RecyclerView.ViewHolder(v) {

        var feedIcon: CircleImage? = null
        var categoryName: TextView? = null

        init {
            feedIcon = v.findViewById<CircleImage>(R.id.imgFriend) as CircleImage
            categoryName = v.findViewById<TextView>(R.id.categoryName) as TextView
        }
    }

    init {
        Log.v("BlacklistSize", items.size.toString())
        rssItems = items
        adapterContext = context
        nextIdSelected = active
    }

    fun setItems(items:List<RssFeedModel>){
        rssItems = items
    }

    fun clear(){
        rssItems = ArrayList<RssFeedModel>()
    }

    fun setActivePosition(position:Int){
        nextIdSelected = position
    }

    fun setCustomItemClickListener(listener:IconClickListener){
        this.listener = listener
    }

    interface IconClickListener {
        fun onItemClickListener(position:Int,feed:RssFeedModel)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeedsAdapter.FeedsViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.feed_item_icon, parent, false)
        return FeedsViewHolder(v)
    }

    override fun onBindViewHolder(holder: FeedsAdapter.FeedsViewHolder, position: Int) {
        if (rssItems != null) {
            if (rssItems!!.isNotEmpty()) {
                if (lastPositionSelected>=0 ){
                    Log.v("SelectedCheck","VALID LAST POSITION")
                    if (lastPositionSelected!=position){
                        Log.v("SelectedCheck","Position: "+position+", Last checked: "+lastPositionSelected)
//                        holder.feedIcon!!.isSelected = false
                        holder.feedIcon!!.borderColor = Color.WHITE
                    }else{
                        Log.v("SelectedCheck","CHECKED - Position: "+position+", Last checked: "+lastPositionSelected)
                        Log.v("SelectedCheck","CHECKED - Item: "+rssItems!![position].toString())
//                        holder.feedIcon!!.isSelected = true
                        holder.feedIcon!!.borderColor = Color.RED
                    }
                }else if (nextIdSelected>0){
                    Log.v("SelectedCheck","SAME ID")
                    if (nextIdSelected == rssItems!![position].idMobile){
                        nextIdSelected = -1
                        lastPositionSelected = position
                        Log.v("SelectedCheck","CHECKED - Next Id: "+nextIdSelected+", Item Id: "+rssItems!![position].idMobile)
                        Log.v("SelectedCheck","CHECKED - Item: "+rssItems!![position].toString())
                        holder.feedIcon!!.isSelected = true
                        holder.feedIcon!!.borderColor = Color.RED
                    }else{
                        Log.v("SelectedCheck","Next id: "+nextIdSelected+", id: "+rssItems!![position].idMobile)
                        holder.feedIcon!!.isSelected = false
                        holder.feedIcon!!.borderColor = Color.WHITE
                    }
                }
                try{
                    try{
                        holder.feedIcon?.setImageDrawable(null)
                        holder.feedIcon?.setImageResource(0)
                        holder.feedIcon?.setImageURI(null)
                        holder.feedIcon?.setImageBitmap(null)
                    }catch (e:Exception){
                        Log.e("CleanImageException",e.toString())
                    }
                    Log.e("FeedLoadingImage","Keyword: "+rssItems!![position].keyword+", Image: "+rssItems!![position].thumbnailImg)
                    try{
                        if (rssItems!![position].isFriends){
                            val user = UserFriendsDao().getFriendByFacebookId(rssItems!![position].friendsId)
                            Glide
                                    .with(adapterContext!!)
                                    .load(user!!.profilePicture?.replace("large","small"))
                                    .into(holder.feedIcon!!)
                        }else if(rssItems!![position].isSuggestion || rssItems!![position].isTrend){
                            Glide
                                    .with(adapterContext!!)
                                    .load(R.mipmap.drew_new_circle2)
                                    .into(holder.feedIcon!!)
                        }else{
                            Glide
                                    .with(adapterContext!!)
                                    .load(rssItems!![position].thumbnailImg)
                                    .into(holder.feedIcon!!)
                        }

                    }catch (e:Exception){
                        Log.e("RssImageException",e.toString())
                    }
                    holder.categoryName?.setText(rssItems!![position].keyword)
                    holder.feedIcon?.setOnClickListener(object : View.OnClickListener{
                        override fun onClick(p0: View?) {
//                            if (holder.feedIcon!!.isSelected){
//                                lastPositionSelected = -1
//                                holder.feedIcon!!.isSelected = false
//                                holder.feedIcon!!.borderColor = Color.WHITE
//                                Log.e("RssItemClick","Unselected")
//                            }else{
//                                Log.v("ItemSelected","Item Selected: "+position+", Last selected: "+lastPositionSelected)
//                                holder.feedIcon!!.isSelected = true
//                                holder.feedIcon!!.borderColor = Color.RED
//                                Log.e("RssItemClick","Selected")
//                                lastPositionSelected = 0
//                            }
                            if (lastPositionSelected==0){
                                lastPositionSelected = -1
//                                holder.feedIcon!!.isSelected = false
//                                holder.feedIcon!!.borderColor = Color.WHITE
                                Log.e("RssItemClick","Unselected")
                            }else{
                                Log.v("ItemSelected","Item Selected: "+position+", Last selected: "+lastPositionSelected)
//                                holder.feedIcon!!.isSelected = true
//                                holder.feedIcon!!.borderColor = Color.RED
                                Log.e("RssItemClick","Selected")
                                lastPositionSelected = 0
                            }
                            if (listener!=null){
                                notifyDataSetChanged()
                                listener?.onItemClickListener(lastPositionSelected,rssItems!![position])
                            }
                            else
                                Log.e("RssItemClick","Listener is null")
                        }
                    })
                }catch (e:Exception){
                    Log.e("RssIconException",e.toString())
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return rssItems!!.size
    }

}