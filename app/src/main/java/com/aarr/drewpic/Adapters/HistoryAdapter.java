package com.aarr.drewpic.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.aarr.drewpic.Database.Model.History;
import com.aarr.drewpic.R;
import com.aarr.drewpic.Views.MainActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andresrodriguez on 11/21/16.
 */
public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder> {

    private Context adapterContext;
    private List<History> history = null;
    private Dialog dialog = null;
    private MainActivity activity = null;
    private int type;

    public List<History> getHistory() {
        if (history==null)
            history = new ArrayList<History>();
        return history;
    }

    public void setHistory(List<History> history) {
        this.history = history;
    }

    public static class HistoryViewHolder extends RecyclerView.ViewHolder {

//        public TextView idField;
        public TextView url;
        public TextView title;
        public LinearLayout linearLayout;
        public Spinner protocolSpinner;

        public HistoryViewHolder(View v) {
            super(v);
//            idField = (TextView) v.findViewById(R.id.idField);
            url = (TextView) v.findViewById(R.id.url);
            title = (TextView) v.findViewById(R.id.title);
            linearLayout = (LinearLayout) v.findViewById(R.id.layout);

        }
    }

    public HistoryAdapter(List<History> items, Context context) {
        setHistory(items);
        Log.v("HistorySize", String.valueOf(getHistory().size()));
        adapterContext = context;
    }

    @Override
    public HistoryAdapter.HistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_history, parent, false);
        activity = (MainActivity) adapterContext;
        return new HistoryViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final HistoryAdapter.HistoryViewHolder holder, final int position) {
        if (getHistory()!=null){
            if (getHistory().size()>0){
//                holder.idField.setText("ID: " + String.valueOf(getHistory().get(position).getId()));
                holder.title.setText(getHistory().get(position).getTitle().trim());
                holder.url.setText(getHistory().get(position).getUrl().trim());
//                holder.creationDate.setText("DATE: " + getHistory().get(position).getCreationDate().toString());

                holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (activity!=null){
                            activity.loadUrlFromAdapter(getHistory().get(position).getUrl().trim());
                        }
                    }
                });
            }
        }

    }

    public String getCleanUrl(String url){
        url = url.replaceFirst("^(http://www\\.|http://|www\\.https://www\\.|https://|www\\.http://m\\.|http://|m\\.https://m\\.|https://|m\\.www\\.)","");
        return url;
    }

    @Override
    public int getItemCount() {
        return getHistory().size();
    }
}
