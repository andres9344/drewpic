package com.aarr.drewpic.Database.Model

import com.aarr.drewpic.Database.Helper.DatabaseHelper
import com.j256.ormlite.dao.RuntimeExceptionDao
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import java.io.Serializable


/**
 * Created by andresrodriguez on 9/16/17.
 */
@DatabaseTable(tableName = "sub_categories")
data class SubCategories(
        @DatabaseField(generatedId = true, columnName = "id_mobile_subcategory")
        var id:Int = -1,
        @DatabaseField(columnName = "id_category")
        var cat: Int? = null,
        @DatabaseField(columnName = "sub_cat")
        var subCat: String? = null
):Serializable{
    constructor() : this(-1,-1,"")

        fun getRepository(): RuntimeExceptionDao<SubCategories, Int> {
                return DatabaseHelper.DaoGet.get(SubCategories::class.java)
        }
}
