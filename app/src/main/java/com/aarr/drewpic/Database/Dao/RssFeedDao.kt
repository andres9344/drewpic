package com.aarr.drewpic.Database.Dao

import android.util.Log
import com.aarr.drewpic.Database.Helper.DatabaseHelper
import com.aarr.drewpic.Database.Model.*
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by andresrodriguez on 9/4/17.
 */

class RssFeedDao {

    private fun insert(concepto: RssFeedModel): Int {
        val dao = DatabaseHelper.helper.getRuntimeExceptionDao(RssFeedModel::class.java)
        return dao.create(concepto)
    }

    private fun update(cocepto: RssFeedModel): Int {
        val dao = DatabaseHelper.helper.getRuntimeExceptionDao(RssFeedModel::class.java)
        return dao.update(cocepto)
    }

    fun findRssFeedByKeyword(keyword: String):RssFeedModel?{
        var lstFilters:RssFeedModel? = null

        val qB = RssFeedModel().getRepository().queryBuilder()

        try {
            qB.where().eq("keyword",keyword)
            qB.orderBy("publish_date",false)
            lstFilters = qB.query()[0]
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun findRssFeedModelByTitle(title:String):RssFeedModel?{
        var lstFilters:RssFeedModel? = null

        val qB = RssFeedModel().getRepository().queryBuilder()

        try {
            qB.where().eq("title",title)
            lstFilters = qB.query()[0]
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun findRssFeedModelByTitleAndSynced(title:String):RssFeedModel?{
        var lstFilters:RssFeedModel? = null

        val qB = RssFeedModel().getRepository().queryBuilder()

        try {
            qB.where()
                    .eq("title",title)
                    .and()
                    .eq("is_friends",false)
//                    .and()
//                    .ne("id_server_feed",-1)
            lstFilters = qB.query()[0]
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun checkWholeFeeds(title:String):List<RssFeedModel>?{
        var lstFilters:List<RssFeedModel>? = null

        val qB = RssFeedModel().getRepository().queryBuilder()

        try {
            qB.where()
                    .eq("title",title)
                    .and()
                    .eq("is_friends",false)
//                    .and()
//                    .ne("id_server_feed",-1)
            lstFilters = qB.query()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun findRssFeedModelByDescription(description:String):RssFeedModel?{
        var lstFilters:RssFeedModel? = null

        val qB = RssFeedModel().getRepository().queryBuilder()

        try {
            qB.where().eq("description",description)
            lstFilters = qB.query()[0]
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun findUnviewedFeeds():MutableList<RssFeedModel>?{
        var lstFilters:MutableList<RssFeedModel>? = null

        val qB = RssFeedModel().getRepository().queryBuilder()

        try {
            qB.where().eq("viewed",false)
            qB.orderBy("id_mobile_feed",true)
            lstFilters = qB.query()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun findUnviewedFeedsByKeyword(keyword:String):MutableList<RssFeedModel>?{
        var lstFilters:MutableList<RssFeedModel>? = null

        val qB = RssFeedModel().getRepository().queryBuilder()

        try {
            qB.where()
                    .eq("keyword",keyword)
                    .and()
                    .eq("viewed",false)

//            qB.orderBy("id_mobile_feed",true)
            qB.orderBy("publish_date",false)
            lstFilters = qB.query()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun findUnviewedFeedsBySearch(keyword:String,search:String):MutableList<RssFeedModel>?{
        var lstFilters:MutableList<RssFeedModel>? = null

        val qB = RssFeedModel().getRepository().queryBuilder()
        val w = qB.where()
        try {
            w
                .like("description","%"+search+"%")
                .and(
                    w.eq("keyword",keyword),
                    w.eq("viewed",false)
                )

//            qB.orderBy("id_mobile_feed",true)
            qB.orderBy("publish_date",false)
            lstFilters = qB.query()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun findAllUnviewedFeedsBySearch(search:String?):MutableList<RssFeedModel>?{
        var lstFilters:MutableList<RssFeedModel>? = null

        val qB = RssFeedModel().getRepository().queryBuilder()
        val w = qB.where()
        try {
            if (!search.isNullOrEmpty()){
                w
                        .or(
                                w.like("description","%"+search+"%"),
                                w.like("keyword","%"+search+"%")
                        ).and()
            }
            w.eq("viewed",false)
            qB.orderBy("publish_date",false)
//            qB.orderBy("publish_date",false)
            lstFilters = qB.query()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun findAllUnviewedFeedsWithFriends(search:String?,addTrend:Boolean):MutableList<RssFeedModel>?{
        var lstFeeds:MutableList<RssFeedModel>? = findAllUnviewedFeedsBySearchIcons(search)
//        var lstFriendFeeds:MutableList<RssFeedModel>? = findAllUnviewedFeedsBySearchFriends(search)
        var lstFinal:MutableList<RssFeedModel>? = ArrayList<RssFeedModel>()
        var feedSuggestions:RssFeedModel?=null
        var friendsPosition = 0
        var counter = 0
        var trendCounter = 0
        Log.e("FeedsCount","Feeds: "+lstFeeds?.size)
//        Log.e("FeedsCount","Friends: "+lstFriendFeeds?.size)
        if (lstFeeds!=null && lstFeeds.isNotEmpty()){
            for (feed in lstFeeds){
//                if (lstFriendFeeds!=null && lstFeeds.isNotEmpty()){
//                    if (counter==3){
//                        counter = 0
//                        if (friendsPosition<lstFriendFeeds.size){
//                            lstFinal!!.add(lstFriendFeeds[friendsPosition])
//                            friendsPosition++
//                        }
//                        if (!search.isNullOrEmpty() && feedSuggestions==null){
//                            Log.e("Suggestion","Adding Suggestion Item")
//                            feedSuggestions = RssFeedModel(
//                                    -12345,
//                                    -1,
//                                    search!!,
//                                    "",
//                                    "",
//                                    "",
//                                    0,
//                                    "",
//                                    "",
//                                    false,
//                                    "",
//                                    "",
//                                    0,
//                                    0,
//                                    false,
//                                    "",
//                                    true,
//                                    false)
//                            lstFinal!!.add(feedSuggestions)
//                        }
//                    }else
//                        if (trendCounter==7){
//                        trendCounter = 0
//                        var searchText = ""
//                        if (!search.isNullOrEmpty()) {
//                            searchText = search!!
//                        }
//                            Log.e("Suggestion","Adding Trend Item")
//                            val trenItem = RssFeedModel(
//                                    -12345,
//                                    -1,
//                                    searchText!!,
//                                    "",
//                                    "",
//                                    "",
//                                    0,
//                                    "",
//                                    "",
//                                    false,
//                                    "",
//                                    "",
//                                    0,
//                                    0,
//                                    false,
//                                    "",
//                                    false,
//                                    true)
//                            lstFinal!!.add(trenItem)
//                    }else{
//                        if (!feed.viewed)
//                            lstFinal!!.add(feed)
//                    }
//                }else
                if (trendCounter==3 && addTrend){
                    trendCounter = 0
                    var searchText = ""
                    if (!search.isNullOrEmpty()) {
                        searchText = search!!
                    }
                    Log.e("Suggestion","Adding Trend Item")
                    val trenItem = RssFeedModel(
                            -12345,
                            -1,
                            searchText!!,
                            "",
                            "",
                            "",
                            0,
                            "",
                            "",
                            false,
                            "",
                            "",
                            0,
                            0,
                            false,
                            "",
                            false,
                            true)
                    lstFinal!!.add(trenItem)
                }else{
                    if (!feed.viewed)
                        lstFinal!!.add(feed)
                }
                counter++
                trendCounter++
            }
        }
        Log.e("FeedsCount","Final: "+lstFinal?.size)
        return lstFinal
    }

    fun findAllUnviewedFeedsBySearchIcons(search:String?):MutableList<RssFeedModel>?{
        var lstFilters:MutableList<RssFeedModel>? = ArrayList<RssFeedModel>()

        val qB = RssFeedModel().getRepository().queryBuilder()
        val w = qB.where()
        try {
            if (!search.isNullOrEmpty()){
                w
                        .or(
                                w.like("description","%"+search+"%"),
                                w.like("keyword","%"+search+"%"),
                                w.like("title","%"+search+"%")
                        )
                        .and()
            }
            w
                    .eq("viewed",false)
                    .and()
                    .eq("is_friends",false)
                    .and()
                    .eq("id_server_feed",-1)
                    .and()
                    .eq("caption","")
            qB.orderBy("publish_date",false)
            qB.limit(5)
//            qB.orderBy("publish_date",false)
            lstFilters = qB.query()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun findAllUnviewedFeedsBySearchIconsAll(search:String?):MutableList<RssFeedModel>?{
        var lstFilters:MutableList<RssFeedModel>? = ArrayList<RssFeedModel>()

        val qB = RssFeedModel().getRepository().queryBuilder()
        val w = qB.where()
        try {
            if (!search.isNullOrEmpty()){
                w
                        .or(
                                w.like("description","%"+search+"%"),
                                w.like("keyword","%"+search+"%"),
                                w.like("title","%"+search+"%")
                        )
                        .and()
            }
            w
                    .eq("viewed",false)
                    .and()
                    .eq("is_friends",false)
                    .and()
                    .eq("id_server_feed",-1)
                    .and()
                    .eq("caption","")
            qB.orderBy("publish_date",false)
//            qB.limit(5)
//            qB.orderBy("publish_date",false)
            lstFilters = qB.query()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun findAllUnviewedFeedsBySearchFriends(search:String?):MutableList<RssFeedModel>?{
        var lstFilters:MutableList<RssFeedModel>? = ArrayList<RssFeedModel>()

        val qB = RssFeedModel().getRepository().queryBuilder()
        val w = qB.where()
        try {
            if (!search.isNullOrEmpty()){
                w
                        .or(
                                w.like("description","%"+search+"%"),
                                w.like("keyword","%"+search+"%")
                        )
                        .and()
            }
            w.eq("viewed",false)
            w.and()
            w.eq("is_friends",true)
            qB.orderBy("publish_date",false)
            qB.limit(5)
//            qB.orderBy("publish_date",false)
            lstFilters = qB.query()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun findLastFeedsByKeyword():MutableList<RssFeedModel>?{
        val lstFilters:MutableList<RssFeedModel>? = ArrayList<RssFeedModel>()

        val qB = RssFeedModel().getRepository().queryBuilder()
        val likes = UserDao().findCurrentUser()?.likes?.split(",")
        if (likes!=null && likes.isNotEmpty())
            for (i in likes){
                try {
                    qB.where()
                            .eq("keyword",i)
                            .and()
                            .eq("viewed",false)
                    qB.orderBy("publish_date",false)
//                    qB.groupBy("keyword")
                    val item = qB.query()[0]
                    if (item!=null)
                        lstFilters?.add(item)
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
        else
            Log.e("LastFeeds","Likes are empty")

        return lstFilters
    }

    fun findLastFeedsBySearch(search:String):MutableList<RssFeedModel>?{
        var lstFilters:MutableList<RssFeedModel>? = ArrayList<RssFeedModel>()

        val qB = RssFeedModel().getRepository().queryBuilder()
        val w = qB.where()
        try {
            w.or(
                    w.like("keyword","%"+search+"%"),
                    w.like("description","%"+search+"%")
            )
                    .and()
                    .eq("viewed",false)
            qB.orderBy("publish_date",false)
//            qB.groupBy("keyword")
            lstFilters = qB.query()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        return lstFilters
    }

    fun findRssFeedModelByMobileId(id:Int):RssFeedModel?{
        var lstFilters:RssFeedModel? = null

        val qB = RssFeedModel().getRepository().queryBuilder()

        try {
            qB.where().eq("id_mobile_feed",id)
            qB.orderBy("id_mobile_feed",true)
            lstFilters = qB.query()[0]
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun findRssFeedModelByServerId(id:Int):RssFeedModel?{
        var lstFilters:RssFeedModel? = null

        val qB = RssFeedModel().getRepository().queryBuilder()

        try {
            qB.where().eq("id_server_feed",id)
            qB.orderBy("id_server_feed",true)
            lstFilters = qB.query()[0]
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun findRssFeedModelByServerIdOrTitle(id:Int,title:String,facebookId:String):RssFeedModel?{
        var lstFilters:RssFeedModel? = null

        val qB = RssFeedModel().getRepository().queryBuilder()
        val w = qB.where()
        try {
            w
                    .or(
                        w.eq("id_server_feed",id),
                        w.eq("title",title)
                    )
                    .and()
                    .eq("friends_id",facebookId)
            qB.orderBy("id_server_feed",true)
            lstFilters = qB.query()[0]
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun updateOrCreate(cat:RssFeedModel?):Int{
        var i = -1
        Log.v("CardSwiped","Updating: "+cat.toString())
        if (cat!!.idMobile>0){
            i = update(cat)
            if (i==1)
                i = cat!!.idMobile
            Log.v("CardSwiped","Updated id: "+i)
        }else{
            insert(cat)
            i = cat.idMobile
            Log.v("CardSwiped","Insert id: "+i)
        }
        return i
    }


}
