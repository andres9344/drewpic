package com.aarr.drewpic.Database.Dao

import com.aarr.drewpic.Database.Model.Bookmarks

/**
 * Created by andresrodriguez on 9/4/17.
 */

class BookmarkDao{

    fun findBookmarkByUrl(query:String?):List<Bookmarks>?{
        var lstFilters:List<Bookmarks>? = null

        val qB = Bookmarks.getRepository().queryBuilder()
        val w = qB.where()

        if (query!=null && !query.equals("")){
            try {
                w.or(
                        w.eq("url",query),
                        w.like("url","%"+query+"%"))
                lstFilters = qB.query()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
        return lstFilters
    }

    fun findBookmarksActive():List<Bookmarks>?{
        var lstFilters:List<Bookmarks>? = null

        val qB = Bookmarks.getRepository().queryBuilder()

        try {
            qB.where().eq("active",true)
            qB.orderBy("createdDate",false)
            lstFilters = qB.query()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun findBookmarks():List<Bookmarks>?{
        var lstFilters:List<Bookmarks>? = null

        val qB = Bookmarks.getRepository().queryBuilder()

        try {
            qB.orderBy("createdDate",false)
            lstFilters = qB.query()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }
}
