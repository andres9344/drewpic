package com.aarr.drewpic.Database.Model;

import com.aarr.drewpic.Database.Helper.DatabaseHelper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by andresrodriguez on 11/20/16.
 */
@DatabaseTable(tableName = "browser_settings")

public class BrowserSettingsModel {

    @DatabaseField(generatedId = true) int id;
    @DatabaseField String homePage;
    @DatabaseField boolean showAdvice;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHomePage() {
        return homePage;
    }

    public void setHomePage(String homePage) {
        this.homePage = homePage;
    }

    public boolean isShowAdvice() {
        return showAdvice;
    }

    public void setShowAdvice(boolean showAdvice) {
        this.showAdvice = showAdvice;
    }

    public static RuntimeExceptionDao<BrowserSettingsModel, Integer> getRepository()
    {
        return DatabaseHelper.DaoGet.Companion.get(BrowserSettingsModel.class);
    }
}
