package com.aarr.drewpic.Database.Model

import android.os.Parcel
import android.os.Parcelable
import com.aarr.drewpic.Database.Helper.DatabaseHelper
import com.j256.ormlite.dao.RuntimeExceptionDao
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import java.io.Serializable
import java.util.*


/**
 * Created by andresrodriguez on 9/16/17.
 */
@DatabaseTable(tableName = "feed")
data class RssFeedModel(

        @DatabaseField(generatedId = true, columnName = "id_mobile_feed")
        var idMobile: Int,

        @DatabaseField(columnName = "id_server_feed")
        var idServer: Int,

        @DatabaseField(columnName = "keyword")
        var keyword: String,

        @DatabaseField(columnName = "title")
        var title: String,

        @DatabaseField(columnName = "link")
        var link: String,

        @DatabaseField(columnName = "image")
        var image: String = "",

        @DatabaseField(columnName = "publish_date")
        var publishDate: Long,

        @DatabaseField(columnName = "thumbnail")
        var thumbnailImg: String = "",

        @DatabaseField(columnName = "description")
        var description: String,

        @DatabaseField(columnName = "viewed")
        var viewed: Boolean,

        @DatabaseField(columnName = "type")
        var type: String,

        @DatabaseField(columnName = "caption")
        var caption: String,

        @DatabaseField(columnName = "count_agree")
        var countAgree: Int,

        @DatabaseField(columnName = "count_disagree")
        var countDisagree: Int,

        @DatabaseField(columnName = "is_friends")
        var isFriends: Boolean,

        @DatabaseField(columnName = "friends_id")
        var friendsId: String,

        @DatabaseField(columnName = "is_suggestions")
        var isSuggestion: Boolean,

        @DatabaseField(columnName = "is_trend")
        var isTrend: Boolean


):Serializable{


    constructor() : this(
            -1,
            -1,
            "",
            "",
            "",
            "",
            -1,
            "",
            "",
            false,
            "",
            "",
            0,
            0,
            false,
            "",
            false,
            false)

        fun getRepository(): RuntimeExceptionDao<RssFeedModel, Int> {
                return DatabaseHelper.DaoGet.get(RssFeedModel::class.java)
        }


}
