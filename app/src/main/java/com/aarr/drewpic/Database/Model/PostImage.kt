package com.aarr.drewpic.Database.Model

import com.aarr.drewpic.Database.Helper.DatabaseHelper
import com.j256.ormlite.dao.RuntimeExceptionDao
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import java.io.Serializable


/**
 * Created by andresrodriguez on 9/16/17.
 */
@DatabaseTable(tableName = "post_image")
data class PostImage(
        @DatabaseField(generatedId = true, columnName = "id_post_image")
        var id:Int = -1,
        @DatabaseField(columnName = "id_mobile_feed")
        var idMobile: Int? = null,
        @DatabaseField(columnName = "image")
        var image: String? = null

):Serializable
{
constructor() : this(-1,
                        -1,
                        "")

        fun getRepository(): RuntimeExceptionDao<PostImage, Int> {
                return DatabaseHelper.DaoGet.get(PostImage::class.java)
        }

}