package com.aarr.drewpic.Database.Model

import com.aarr.drewpic.Database.Helper.DatabaseHelper
import com.j256.ormlite.dao.RuntimeExceptionDao
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import java.io.Serializable


/**
 * Created by andresrodriguez on 9/16/17.
 */
@DatabaseTable(tableName = "settings")
data class Settings(
        @DatabaseField(generatedId = true, columnName = "id_mobile_user")
        var id:Int = -1,
        @DatabaseField(columnName = "language")
        var language: String? = null,
        @DatabaseField(columnName = "language_short")
        var languageShort: String? = null,
        @DatabaseField(columnName = "country")
        var country: String? = null,
        @DatabaseField(columnName = "dislike_bubble")
        var showDislike: Boolean = true,
        @DatabaseField(columnName = "like_bubble")
        var showLike: Boolean = true

):Serializable
{
constructor() : this(
        -1,
        "",
        "",
        "",
        true,
        true
)

        fun getRepository(): RuntimeExceptionDao<Settings, Int> {
                return DatabaseHelper.DaoGet.get(Settings::class.java)
        }

}