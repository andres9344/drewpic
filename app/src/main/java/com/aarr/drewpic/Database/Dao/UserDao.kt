package com.aarr.drewpic.Database.Dao

import com.aarr.drewpic.Database.Model.TabsContent
import com.aarr.drewpic.Database.Model.User

/**
 * Created by andresrodriguez on 9/4/17.
 */

class UserDao{

    fun findCurrentUser(): User?{
        var lstFilters:User? = null

        try {
            lstFilters = User().getRepository().queryForAll().get(0)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun updateBubbleShow(){
        var user: User? = findCurrentUser()
        if (user!!.fullPageBubble){
            user!!.fullPageBubble = false
            User().getRepository().update(user)
        }
    }
}
