package com.aarr.drewpic.Database.Dao

import android.util.Log
import com.aarr.drewpic.Database.Helper.DatabaseHelper
import com.aarr.drewpic.Database.Model.RssFeedModel
import com.aarr.drewpic.Database.Model.TabsContent
import com.aarr.drewpic.Database.Model.User
import com.aarr.drewpic.Database.Model.UserFriends

/**
 * Created by andresrodriguez on 9/4/17.
 */

class UserFriendsDao {

    private fun insert(concepto: UserFriends): Int {
        val dao = DatabaseHelper.helper.getRuntimeExceptionDao(UserFriends::class.java)
        return dao.create(concepto)
    }

    private fun update(cocepto: UserFriends): Int {
        val dao = DatabaseHelper.helper.getRuntimeExceptionDao(UserFriends::class.java)
        return dao.update(cocepto)
    }

    fun findAllCurrentFriends(): MutableList<UserFriends>?{
        var lstFilters:MutableList<UserFriends>? = null

        try {
            lstFilters = UserFriends().getRepository().queryForAll()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun getFriendByFacebookId(id:String?):UserFriends?{
        var lstFilters:UserFriends? = null

        val qB = UserFriends().getRepository().queryBuilder()

        try {
            qB.where().eq("id_facebook",id)
            lstFilters = qB.query()[0]
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun getFriendByMobileId(id:String):UserFriends?{
        var lstFilters:UserFriends? = null

        val qB = UserFriends().getRepository().queryBuilder()

        try {
            qB.where().eq("id_mobile_user",id)
            lstFilters = qB.query()[0]
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun getFriendsCount():Int{
        var size = 0
        try{
            size = findAllCurrentFriends()?.size!!
        }catch (e:Exception){
            Log.e("FriendsCount",e.toString())
        }
        return size
    }

    fun updateOrCreate(friend: UserFriends?):Int{
        var i = -1
        Log.v("UpdateFriend","Updating: "+friend.toString())
        if (friend!!.id>0){
            i = update(friend)
            if (i==1)
                i = friend!!.id
            Log.v("UpdateFriend","Updated id: "+i)
        }else{
            insert(friend)
            i = friend.id
            Log.v("UpdateFriend","Insert id: "+i)
        }
        return i
    }
}
