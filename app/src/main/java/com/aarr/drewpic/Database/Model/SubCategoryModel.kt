package com.aarr.drewpic.Database.Model

import java.io.Serializable

/**
 * Created by andresrodriguez on 9/18/17.
 */
data class SubCategoryModel(var id:Int, var name:String):Serializable{
    constructor() : this(-1,"")
}