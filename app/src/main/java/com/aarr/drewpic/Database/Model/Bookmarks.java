package com.aarr.drewpic.Database.Model;

import com.aarr.drewpic.Database.Helper.DatabaseHelper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by andresrodriguez on 11/20/16.
 */
@DatabaseTable(tableName = "bookmarks")

public class Bookmarks {

    @DatabaseField(generatedId = true) int id;
    @DatabaseField String url;
    @DatabaseField String title;
    @DatabaseField int idUsuario;
    @DatabaseField Date createdDate;
    @DatabaseField boolean active;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public static RuntimeExceptionDao<Bookmarks, Integer> getRepository()
    {
        return DatabaseHelper.DaoGet.Companion.get(Bookmarks.class);
    }
}
