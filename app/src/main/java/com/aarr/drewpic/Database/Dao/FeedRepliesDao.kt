package com.aarr.drewpic.Database.Dao

import android.util.Log
import com.aarr.drewpic.Database.Helper.DatabaseHelper
import com.aarr.drewpic.Database.Model.*
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by andresrodriguez on 9/4/17.
 */

class FeedRepliesDao {

    private fun insert(concepto: FeedReplies): Int {
        val dao = DatabaseHelper.helper.getRuntimeExceptionDao(FeedReplies::class.java)
        return dao.create(concepto)
    }

    private fun update(cocepto: FeedReplies): Int {
        val dao = DatabaseHelper.helper.getRuntimeExceptionDao(FeedReplies::class.java)
        return dao.update(cocepto)
    }

    fun getReplies(ascending:Boolean,status:String?):MutableList<FeedReplies>?{
        var lstFilters:MutableList<FeedReplies>? = ArrayList<FeedReplies>()

        val qB = FeedReplies().getRepository().queryBuilder()

        try {
            if (!status.isNullOrEmpty()){
                qB.where().eq("status",status)
            }
            qB.orderBy("created_at",ascending)
            lstFilters = qB.query()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun findReplyByMobileId(id:Int):FeedReplies?{
        var lstFilters:FeedReplies? = null

        val qB = FeedReplies().getRepository().queryBuilder()

        try {
            qB.where().eq("id_mobile_reply",id)
            qB.orderBy("id_mobile_reply",true)
            lstFilters = qB.query()[0]
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun findMyReplyByPostId(id:Int):FeedReplies?{
        var lstFilters:FeedReplies? = null

        val qB = FeedReplies().getRepository().queryBuilder()
        val user = UserDao().findCurrentUser()
        try {
            qB.where()
                    .eq("server_post_id",id)
                    .and()
                    .eq("friends_facebook_id",user!!.idFacebook)
            qB.orderBy("id_mobile_reply",true)
            lstFilters = qB.query()[0]
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun findReplyByServerId(id:Int):FeedReplies?{
        var lstFilters:FeedReplies? = null

        val qB = FeedReplies().getRepository().queryBuilder()

        try {
            qB.where().eq("id_server_reply",id)
            qB.orderBy("id_server_reply",true)
            lstFilters = qB.query()[0]
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun getLikesCountByPost(id:Int):Int{
        var count = 0
        val qB = FeedReplies().getRepository().queryBuilder()

        try {
            qB.where()
                    .eq("type","Right")
                    .and()
                    .eq("server_post_id",id)
            count = qB.query().size
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return count
    }

    fun getDislikesCountByPost(id:Int):Int{
        var count = 0
        val qB = FeedReplies().getRepository().queryBuilder()

        try {
            qB.where()
                    .eq("type","Left")
                    .and()
                    .eq("server_post_id",id)
            count = qB.query().size
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return count
    }

    fun updateOrCreate(cat:FeedReplies?):Int{
        var i = -1
        Log.v("CardSwiped","Updating: "+cat.toString())
        if (cat!!.idMobile>0){
            i = update(cat)
            if (i==1)
                i = cat!!.idMobile
            Log.v("CardSwiped","Updated id: "+i)
        }else{
            insert(cat)
            i = cat.idMobile
            Log.v("CardSwiped","Insert id: "+i)
        }
        return i
    }


}
