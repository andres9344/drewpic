package com.aarr.drewpic.Database.Model

import android.os.Parcel
import android.os.Parcelable
import com.aarr.drewpic.Database.Helper.DatabaseHelper
import com.j256.ormlite.dao.RuntimeExceptionDao
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import java.io.Serializable
import java.util.*


/**
 * Created by andresrodriguez on 9/16/17.
 */
@DatabaseTable(tableName = "feed_replies")
data class FeedReplies(

        @DatabaseField(generatedId = true, columnName = "id_mobile_reply")
        var idMobile: Int,

        @DatabaseField(columnName = "id_server_reply")
        var idServer: Int,

        @DatabaseField(columnName = "caption")
        var caption: String,

        @DatabaseField(columnName = "friends_facebook_id")
        var friendFacebookId: String,

        @DatabaseField(columnName = "server_post_id")
        var serverPostId: Int,

        @DatabaseField(columnName = "created_at")
        var createdAt: Long,

        @DatabaseField(columnName = "type")
        var type: String,

        @DatabaseField(columnName = "status")
        var status: String

):Serializable{


    constructor() : this(
            -1,
            -1,
            "",
            "",
            -1,
            0,
            "",
            "")

        fun getRepository(): RuntimeExceptionDao<FeedReplies, Int> {
                return DatabaseHelper.DaoGet.get(FeedReplies::class.java)
        }


}
