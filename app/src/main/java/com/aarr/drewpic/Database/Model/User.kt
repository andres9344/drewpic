package com.aarr.drewpic.Database.Model

import com.aarr.drewpic.Database.Helper.DatabaseHelper
import com.j256.ormlite.dao.RuntimeExceptionDao
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import java.io.Serializable


/**
 * Created by andresrodriguez on 9/16/17.
 */
@DatabaseTable(tableName = "user")
data class User(
        @DatabaseField(generatedId = true, columnName = "id_mobile_user")
        var id:Int = -1,
        @DatabaseField(columnName = "id_user")
        var userId: Int? = null,
        @DatabaseField(columnName = "email")
        var email: String? = null,
        @DatabaseField(columnName = "name")
        var name: String? = null,
        @DatabaseField(columnName = "profile_picture")
        var profilePicture: String? = null,
        @DatabaseField(columnName = "sub_category")
        var subCategory: String? = null,
        @DatabaseField(columnName = "full_page_bubble")
        var fullPageBubble: Boolean = false,
        @DatabaseField(columnName = "likes")
        var likes: String? = null,
        @DatabaseField(columnName = "show_tutorial")
        var showTutorial: Boolean? = null,
        @DatabaseField(columnName = "id_facebook")
        var idFacebook: String? = null,
        @DatabaseField(columnName = "token")
        var token: String? = null

):Serializable
{
constructor() : this(-1,
                        -1,
                        "",
                        "",
                        "",
                        "",
                        false,
                        "",
                        true,
                        "",
                        "")

        fun getRepository(): RuntimeExceptionDao<User, Int> {
                return DatabaseHelper.DaoGet.get(User::class.java)
        }

}