package com.aarr.drewpic.Database.Dao

import android.util.Log
import com.aarr.drewpic.Database.Helper.DatabaseHelper
import com.aarr.drewpic.Database.Model.Categories
import com.aarr.drewpic.Database.Model.History
import com.aarr.drewpic.Database.Model.SubCategoryModel
import com.aarr.drewpic.Database.Model.User
import java.util.*

/**
 * Created by andresrodriguez on 9/4/17.
 */

class CategoriesDao{

    private fun insert(concepto: Categories): Int {
        val dao = DatabaseHelper.helper.getRuntimeExceptionDao(Categories::class.java)
        return dao.create(concepto)
    }

    private fun update(cocepto: Categories): Int {
        val dao = DatabaseHelper.helper.getRuntimeExceptionDao(Categories::class.java)
        return dao.update(cocepto)
    }

    fun findCategories():MutableList<Categories>?{
        var lstFilters:MutableList<Categories>? = null

        val qB = Categories().getRepository().queryBuilder()

        try {
            qB.orderBy("category",true)
            lstFilters = qB.query()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun updateOrCreate(cat:Categories?):Int{
        var i = -1
        if (cat!!.id>0){
            i = update(cat)
        }else{
            insert(cat)
            i = cat.id
            Log.v("CreateCategory","Id: "+i)
        }
        return i
    }


}
