package com.aarr.drewpic.Database.Model

import com.aarr.drewpic.Database.Helper.DatabaseHelper
import com.j256.ormlite.dao.RuntimeExceptionDao
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import java.io.Serializable


/**
 * Created by andresrodriguez on 9/16/17.
 */
@DatabaseTable(tableName = "categories")
data class Categories(
        @DatabaseField(generatedId = true, columnName = "id_mobile_category")
        var id:Int = -1,
        @DatabaseField(columnName = "category")
        var cat: String? = null,
        @DatabaseField(columnName = "category_img")
        var image: String? = null
):Serializable{
    constructor() : this(-1,"","")

        fun getRepository(): RuntimeExceptionDao<Categories, Int> {
                return DatabaseHelper.DaoGet.get(Categories::class.java)
        }
}
