package com.aarr.drewpic.Database.Dao

import com.aarr.drewpic.Database.Model.BlacklistFilter

/**
 * Created by andresrodriguez on 9/4/17.
 */

class BlackListFilterDao{

    fun findBlacklistByUrl(query:String?, type:Int?):List<BlacklistFilter>?{
        var lstFilters:List<BlacklistFilter>? = null

        val qB = BlacklistFilter.getRepository().queryBuilder()
        val w = qB.where()

        if (query!=null && !query.equals("")){
            try {
                w.eq("type",type).and(
                        w.or(
                                w.eq("url",query),
                                w.like("url","%"+query+"%"))
                        ,null
                )
                lstFilters = qB.query()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }else{
            try {
                w.eq("type",type)
                lstFilters = qB.query()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
        return lstFilters
    }
}
