package com.aarr.drewpic.Database.Dao

import android.util.Log
import com.aarr.drewpic.Database.Model.*
import java.util.*

/**
 * Created by andresrodriguez on 9/4/17.
 */

class SubCategoriesDao{

    fun findSubCategories():List<SubCategories>?{
        var lstFilters:List<SubCategories>? = null

        val qB = SubCategories().getRepository().queryBuilder()

        try {
            qB.orderBy("id_category",true)
            lstFilters = qB.query()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun findSubCategoriesByCategory(id:Int):List<SubCategories>?{
        var lstFilters:List<SubCategories>? = null

        val qB = SubCategories().getRepository().queryBuilder()

        try {
            qB.where().eq("id_category",id)
            lstFilters = qB.query()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun updateOrCreate(cat:Categories?):Int{
        if (cat!!.id>0){
            Categories().getRepository().update(cat)
            return cat.id
        }else{
            var i = Categories().getRepository().create(cat)
            Log.v("CreateCategory","Id: "+i)
            return i
        }
    }

    fun getRandomKeyword():String?{
        var keyword: String? = null
        var lstData = CategoriesDao().findCategories()
        var userData = UserDao().findCurrentUser()
        if (userData!=null){
            Log.v("Keyword","USER DATA NOT NULL")
            val currentCats = userData!!.subCategory!!.split(",")
            val selectedCat = currentCats[Random().nextInt((currentCats.size-1) - 0 + 1) + 0]
            if (lstData!=null && lstData!!.isNotEmpty()){
                Log.v("Keyword","LIST DATA IS NOT EMPTY")
                for (item in lstData!!){
                    Log.v("Keyword","CATEGORY ID: "+item.id)
                    var lstSubs = findSubCategoriesByCategory(item.id)
                    if (lstSubs!=null){
                        Log.v("Keyword","LIST SUB CATEGOIRES IS NOT NULL: "+lstSubs.size)
                    }else{
                        Log.v("Keyword","LIST SUB CATEGOIRES IS NULL")
                    }
                    for (item2 in lstSubs!!){
                        val sub2 = item2!!.subCat!!.split("#")
                        if (sub2[0] == selectedCat){
                            Log.v("Keyword","FOUND SELECTED CATEGORY")
                            keyword = sub2[1]
                        }else{
                            Log.v("Keyword","SELECTED CATEGORY NOT FOUND")
                        }
                    }
                }
            }else{
                Log.v("Keyword","LIST DATA IS EMPTY")
            }
        }else{
            Log.v("Keyword","USER DATA IS NULL")
        }
        return keyword
    }

}
