package com.aarr.drewpic.Database.Dao

import android.util.Log
import com.aarr.drewpic.Database.Helper.DatabaseHelper
import com.aarr.drewpic.Database.Model.*

/**
 * Created by andresrodriguez on 9/4/17.
 */

class SettingsDao {

    private fun insert(concepto: Settings): Int {
        val dao = DatabaseHelper.helper.getRuntimeExceptionDao(Settings::class.java)
        return dao.create(concepto)
    }

    private fun update(cocepto: Settings): Int {
        val dao = DatabaseHelper.helper.getRuntimeExceptionDao(Settings::class.java)
        return dao.update(cocepto)
    }

    fun getSettings(): Settings?{
        var settings:Settings? = null

        try {
            settings = Settings().getRepository().queryForAll()[0]
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return settings
    }

    fun updateOrCreate(settings: Settings?):Int{
        var i = -1
        Log.v("UpdateFriend","Updating: "+settings.toString())
        if (settings!!.id>0){
            i = update(settings)
            if (i==1)
                i = settings!!.id
            Log.v("UpdateFriend","Updated id: "+i)
        }else{
            insert(settings)
            i = settings.id
            Log.v("UpdateFriend","Insert id: "+i)
        }
        return i
    }
}
