package com.aarr.drewpic.Database.Dao

import com.aarr.drewpic.Database.Model.TabsContent

/**
 * Created by andresrodriguez on 9/4/17.
 */

class TabsDao{

    fun findTabActive():TabsContent?{
        var lstFilters:TabsContent? = null

        val qB = TabsContent.getRepository().queryBuilder()

        try {
            qB.where().eq("active",true)
            qB.orderBy("id",false)
            lstFilters = qB.query().get(0)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun findTabs():List<TabsContent>?{
        var lstFilters:List<TabsContent>? = null

        val qB = TabsContent.getRepository().queryBuilder()

        try {
            qB.orderBy("id",false)
            lstFilters = qB.query()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }
}
