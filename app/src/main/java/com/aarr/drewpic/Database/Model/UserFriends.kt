package com.aarr.drewpic.Database.Model

import com.aarr.drewpic.Database.Helper.DatabaseHelper
import com.j256.ormlite.dao.RuntimeExceptionDao
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import java.io.Serializable


/**
 * Created by andresrodriguez on 9/16/17.
 */
@DatabaseTable(tableName = "user_friends")
data class UserFriends(
        @DatabaseField(generatedId = true, columnName = "id_mobile_user")
        var id:Int = -1,
        @DatabaseField(columnName = "email")
        var email: String? = null,
        @DatabaseField(columnName = "name")
        var name: String? = null,
        @DatabaseField(columnName = "gender")
        var gender: String? = null,
        @DatabaseField(columnName = "profile_picture")
        var profilePicture: String? = null,
        @DatabaseField(columnName = "id_facebook")
        var idFacebook: String? = null

):Serializable
{
constructor() : this(-1,
                        "",
                        "",
                        "",
                        "",
                        "")

        fun getRepository(): RuntimeExceptionDao<UserFriends, Int> {
                return DatabaseHelper.DaoGet.get(UserFriends::class.java)
        }

}