package com.aarr.drewpic.Database.Dao

import android.util.Log
import com.aarr.drewpic.Database.Helper.DatabaseHelper
import com.aarr.drewpic.Database.Model.*
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by andresrodriguez on 9/4/17.
 */

class PostImagesDao {

    private fun insert(concepto: PostImage): Int {
        val dao = DatabaseHelper.helper.getRuntimeExceptionDao(PostImage::class.java)
        return dao.create(concepto)
    }

    private fun update(cocepto: PostImage): Int {
        val dao = DatabaseHelper.helper.getRuntimeExceptionDao(PostImage::class.java)
        return dao.update(cocepto)
    }

    fun findRssFeedByKeyword(keyword: String):PostImage?{
        var lstFilters:PostImage? = null

        val qB = PostImage().getRepository().queryBuilder()

        try {
            qB.where().eq("keyword",keyword)
            qB.orderBy("publish_date",false)
            lstFilters = qB.query()[0]
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun findPostImageByTitle(title:String):PostImage?{
        var lstFilters:PostImage? = null

        val qB = PostImage().getRepository().queryBuilder()

        try {
            qB.where().eq("title",title)
            lstFilters = qB.query()[0]
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun findPostImageByDescription(description:String):PostImage?{
        var lstFilters:PostImage? = null

        val qB = PostImage().getRepository().queryBuilder()

        try {
            qB.where().eq("description",description)
            lstFilters = qB.query()[0]
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun findUnviewedFeeds():MutableList<PostImage>?{
        var lstFilters:MutableList<PostImage>? = null

        val qB = PostImage().getRepository().queryBuilder()

        try {
            qB.where().eq("viewed",false)
            qB.orderBy("id_mobile_feed",true)
            lstFilters = qB.query()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun findUnviewedFeedsByKeyword(keyword:String):MutableList<PostImage>?{
        var lstFilters:MutableList<PostImage>? = null

        val qB = PostImage().getRepository().queryBuilder()

        try {
            qB.where()
                    .eq("keyword",keyword)
                    .and()
                    .eq("viewed",false)

//            qB.orderBy("id_mobile_feed",true)
            qB.orderBy("publish_date",false)
            lstFilters = qB.query()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun findUnviewedFeedsBySearch(keyword:String,search:String):MutableList<PostImage>?{
        var lstFilters:MutableList<PostImage>? = null

        val qB = PostImage().getRepository().queryBuilder()
        val w = qB.where()
        try {
            w
                .like("description","%"+search+"%")
                .and(
                    w.eq("keyword",keyword),
                    w.eq("viewed",false)
                )

//            qB.orderBy("id_mobile_feed",true)
            qB.orderBy("publish_date",false)
            lstFilters = qB.query()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun findAllUnviewedFeedsBySearch(search:String?):MutableList<PostImage>?{
        var lstFilters:MutableList<PostImage>? = null

        val qB = PostImage().getRepository().queryBuilder()
        val w = qB.where()
        try {
            if (!search.isNullOrEmpty()){
                w
                        .or(
                                w.like("description","%"+search+"%"),
                                w.like("keyword","%"+search+"%")
                        )
                        .and()
                        .eq("viewed",false)
            }
            w.eq("viewed",false)
            qB.orderBy("id_mobile_feed",true)
//            qB.orderBy("publish_date",false)
            lstFilters = qB.query()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun findAllUnviewedFeedsBySearchIcons(search:String?):MutableList<PostImage>?{
        var lstFilters:MutableList<PostImage>? = null

        val qB = PostImage().getRepository().queryBuilder()
        val w = qB.where()
        try {
            if (!search.isNullOrEmpty()){
                w
                        .or(
                                w.like("description","%"+search+"%"),
                                w.like("keyword","%"+search+"%")
                        )
                        .and()
                        .eq("viewed",false)
            }
            w.eq("viewed",false)
            qB.orderBy("id_mobile_feed",false)
//            qB.orderBy("publish_date",false)
            lstFilters = qB.query()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun findLastFeedsByKeyword():MutableList<PostImage>?{
        val lstFilters:MutableList<PostImage>? = ArrayList<PostImage>()

        val qB = PostImage().getRepository().queryBuilder()
        val likes = UserDao().findCurrentUser()?.likes?.split(",")
        if (likes!=null && likes.isNotEmpty())
            for (i in likes){
                try {
                    qB.where()
                            .eq("keyword",i)
                            .and()
                            .eq("viewed",false)
                    qB.orderBy("publish_date",false)
                    qB.groupBy("keyword")
                    val item = qB.query()[0]
                    if (item!=null)
                        lstFilters?.add(item)
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
        else
            Log.e("LastFeeds","Likes are empty")

        return lstFilters
    }

    fun findLastFeedsBySearch(search:String):MutableList<PostImage>?{
        var lstFilters:MutableList<PostImage>? = ArrayList<PostImage>()

        val qB = PostImage().getRepository().queryBuilder()
        val w = qB.where()
        try {
            w.or(
                    w.like("keyword","%"+search+"%"),
                    w.like("description","%"+search+"%")
            )
                    .and()
                    .eq("viewed",false)
            qB.orderBy("publish_date",false)
            qB.groupBy("keyword")
            lstFilters = qB.query()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        return lstFilters
    }

    fun findPostImageByMobileId(id:Int):PostImage?{
        var lstFilters:PostImage? = null

        val qB = PostImage().getRepository().queryBuilder()

        try {
            qB.where().eq("id_mobile_feed",id)
            qB.orderBy("id_mobile_feed",true)
            lstFilters = qB.query()[0]
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun findPostImageByPostId(id:Int):PostImage?{
        var lstFilters:PostImage? = null

        val qB = PostImage().getRepository().queryBuilder()

        try {
            qB.where().eq("id_mobile_feed",id)
            lstFilters = qB.query()[0]
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }

    fun updateOrCreate(cat:PostImage?):Int{
        var i = -1
        Log.v("CardSwiped","Updating: "+cat.toString())
        if (cat!!.id>0){
            i = update(cat)
            if (i==1)
                i = cat!!.id
            Log.v("CardSwiped","Updated id: "+i)
        }else{
            insert(cat)
            i = cat.id
            Log.v("CardSwiped","Insert id: "+i)
        }
        return i
    }


}
