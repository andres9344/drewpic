package com.aarr.drewpic.Services.Response.Categories

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by andresrodriguez on 9/16/17.
 */
data class CategoriesResponse(
        @SerializedName("status")
        @Expose
        var status: String? = null,
        @SerializedName("data")
        @Expose
        var data: List<Data>? = null,
        @SerializedName("msg")
        @Expose
        var msg: String? = null
):Serializable{
    constructor() : this("", emptyList(),"")
}