package com.aarr.drewpic.Services

import android.content.Context
import android.util.Log
import com.aarr.drewpic.Database.Dao.RssFeedDao
import com.aarr.drewpic.Database.Dao.UserDao
import com.aarr.drewpic.Database.Model.RssFeedModel
import com.aarr.drewpic.Services.Response.GetBingNews.GetBingNews
import com.aarr.drewpic.Services.Response.GetFriendsPosts.GetFriendsPosts
import com.aarr.drewpic.Services.Response.GetFriendsPosts.GetPostsHistory
import com.aarr.drewpic.Services.Response.PostPhoto.PostPhotoResponse
import com.aarr.drewpic.Util.DateManage
import com.aarr.drewpic.Util.JsonInterceptor
import com.aarr.drewpic.Views.Main2Activity
import com.aarr.drewpic.Views.ProfileActivity
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory



/**
 * Created by andresrodriguez on 2/18/18.
 */
class GetPostsHistoryService {

    var context: Context?=null
    var service: ServiceAPI?=null
    var activity: Main2Activity?=null

    constructor(context: Context){
        this.context = context
        activity = context as Main2Activity
    }

    fun getHistory(facebookId:String){

        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(JsonInterceptor())

        val retrofit = Retrofit.Builder()
                .baseUrl("http://www.drewpic.net/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build()
        Log.v("Service","Url:"+retrofit.baseUrl().toString())
        service = retrofit.create(ServiceAPI::class.java)

        val response: Call<GetPostsHistory>? = service!!.getPostsHistory(facebookId)
            response!!.enqueue(object : Callback<GetPostsHistory> {
                override fun onResponse(call: Call<GetPostsHistory>?, response: retrofit2.Response<GetPostsHistory>?) {
                    val resp = response!!.body()
                    Log.v("PostResponse","Data: "+resp.toString())
                    try{
                        val resp = response!!.body()
                        Log.v("PostResponse","Data: "+resp.toString())
                        if (resp.replies!=null && resp.replies.isNotEmpty()){
                            activity!!.startAsyncLoadFriendReplies(resp.replies,context!!)
                        }else{
                            activity!!.startAsyncLoadFriendReplies(emptyList(),context!!)
                        }
                    }catch (e:Exception){
                        Log.e("PostResponse","Exception: "+e.toString())
                    }
                }
                override fun onFailure(call: Call<GetPostsHistory>?, t: Throwable?) {
                    activity!!.startAsyncLoadFriendReplies(emptyList(),context!!)
                    Log.e("PostResponse","Failure: "+t.toString())
                }
            })
    }

    fun Boolean.toInt() = if (this) 1 else 0
}