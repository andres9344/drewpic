package com.aarr.drewpic.Services

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Service
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.PixelFormat
import android.os.Environment
import android.os.IBinder
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import cn.pedant.SweetAlert.SweetAlertDialog
import com.aarr.drewpic.Adapters.FeedsAdapter
import com.aarr.drewpic.Adapters.RssFeedAdapter
import com.aarr.drewpic.Database.Dao.RssFeedDao
import com.aarr.drewpic.Database.Dao.UserDao
import com.aarr.drewpic.Database.Model.RssFeedModel
import com.aarr.drewpic.Database.Model.User
import com.aarr.drewpic.R
import com.aarr.drewpic.Services.Response.RSS.RssItemModel
import com.aarr.drewpic.Services.Response.UserFBLikes.UserLikesResponse
import com.aarr.drewpic.Views.Main2Activity
import com.aarr.drewpic.Views.PostActivity
import com.aarr.drewpic.Views.ProfileActivity
import com.aarr.drewpic.Views.SignupActivity
import com.alexzh.circleimageview.CircleImageView
import com.alexzh.circleimageview.ItemSelectedListener
import com.bumptech.glide.Glide
import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.facebook.GraphResponse
import com.facebook.HttpMethod
import com.facebook.share.model.SharePhoto
import com.facebook.share.model.SharePhotoContent
import com.facebook.share.widget.ShareDialog
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.yuyakaido.android.cardstackview.CardStackView
import com.yuyakaido.android.cardstackview.SwipeDirection
import me.toptas.rssconverter.RssItem
import java.io.*
import java.util.*
import kotlinx.android.synthetic.main.layout_floating_widget.*

/**
 * Created by andresrodriguez on 11/12/17.
 */
class FloatingViewService() : Service(), FeedsAdapter.IconClickListener, RssFeedAdapter.ShareButtonListener {

    private var mWindowManager: WindowManager? = null
    private var mFloatingView: View? = null
//    private var imgCategory: CircleImageView? = null
//    private var imgCategory2: CircleImageView? = null
    private var feedLst: MutableList<RssFeedModel>? = ArrayList<RssFeedModel>()
    private var feedIconLst: MutableList<RssFeedModel>? = ArrayList<RssFeedModel>()
    private var recycler: RecyclerView? = null
    private var cardStackView: CardStackView? = null
    private var adapterCard: RssFeedAdapter? = null
    private var adapter: FeedsAdapter? = null
    private var rssDetails: RelativeLayout? = null
    private var btnOpenDetails: ImageView? = null
    private var feedContainer: ConstraintLayout? = null
    private var currentPosition: Int = -1
    private var collapsedView: RelativeLayout? = null
    private var expandedView: LinearLayout? = null
    var pDialog: SweetAlertDialog? = null
    private var facebookToken: String?=null
    private var searchView: RelativeLayout?=null
    private var txtSearch: EditText?=null


    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        var onSuper = super.onStartCommand(intent, flags, startId)

//        fillAndResetRecycler(null)
        if (intent!=null){
            if (intent.hasExtra("Token")){
                facebookToken = intent.getStringExtra("Token")
            }
//            if (intent.hasExtra("FeedList")){
//                fillAndResetCardStack()
//                feedLst = intent.getParcelableArrayListExtra("FeedList")
//
//            }
//            if (intent.hasExtra("FeedIconList")){
//                feedIconLst = intent.getParcelableArrayListExtra("FeedIconList")
//                if (feedIconLst!=null && feedIconLst!!.size>0){
//                    Log.v("StartCommand","Feed Icon List is not null: "+feedIconLst!!.size)
//                }else{
//                    Log.e("StartCommand","Feed Icon List is null")
//                }
//            }

        }

        return onSuper
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate() {
        super.onCreate()


        //Inflate the floating view layout we created
        mFloatingView = LayoutInflater.from(this).inflate(R.layout.layout_floating_widget, null)

        feedContainer = mFloatingView!!.findViewById(R.id.feedContainer)

        rssDetails = mFloatingView!!.findViewById(R.id.rssDetails)
        btnOpenDetails = mFloatingView!!.findViewById(R.id.btnOpenDetails)
//        btnOpenDetails?.setOnClickListener {
//
//
//        }

        recycler = mFloatingView!!.findViewById<RecyclerView>(R.id.recycler) as RecyclerView
        cardStackView = mFloatingView!!.findViewById<CardStackView>(R.id.card_stack_view) as CardStackView

//        fillAndResetRecycler(null)
//        fillAndResetCardStack()

//        imgCategory = mFloatingView!!.findViewById<CircleImageView>(R.id.imgCategory) as CircleImageView
//        imgCategory!!.setOnItemSelectedClickListener(object: ItemSelectedListener {
//            override fun onSelected(view: View?) {
//                val intent = Intent(this@FloatingViewService, SignupActivity::class.java)
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                startActivity(intent)
//
//
//                //close the service and remove view from the view hierarchy
//                stopSelf()
//            }
//
//            override fun onUnselected(view: View?) {
//                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//            }
//        })
//
//        imgCategory2 = mFloatingView!!.findViewById<CircleImageView>(R.id.imgCategory2) as CircleImageView
//        imgCategory2!!.setOnItemSelectedClickListener(object: ItemSelectedListener {
//            override fun onSelected(view: View?) {
//                val intent = Intent(this@FloatingViewService, SignupActivity::class.java)
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                startActivity(intent)
//
//
//                //close the service and remove view from the view hierarchy
//                stopSelf()
//            }
//
//            override fun onUnselected(view: View?) {
//                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//            }
//        })

        //Add the view to the window.
        val params = WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT)

        //Specify the view position
        params.gravity = Gravity.TOP or Gravity.CENTER_HORIZONTAL        //Initially view will be added to center
        params.y = 0

        //Add the view to the window
        mWindowManager = getSystemService(WINDOW_SERVICE) as WindowManager
        mWindowManager!!.addView(mFloatingView, params)

        //The root element of the collapsed view layout
        collapsedView = mFloatingView!!.findViewById<RelativeLayout>(R.id.collapse_view)
//        val mainButton = mFloatingView!!.findViewById<CircleImageView>(R.id.collapsed_iv)
//        mainButton.setOnItemSelectedClickListener(object: ItemSelectedListener{
//            override fun onSelected(view: View?) {
//
//            }
//
//            override fun onUnselected(view: View?) {
//
//            }
//
//        })
//The root element of the expanded view layout
        expandedView = mFloatingView!!.findViewById<LinearLayout>(R.id.expandedContainer)


//Set the close button
        val closeButtonCollapsed = mFloatingView!!.findViewById<ImageView>(R.id.close_btn) as ImageView
        closeButtonCollapsed.setOnClickListener(View.OnClickListener {
            //close the service and remove the from from the window
            stopSelf()
        })


//Set the view while floating view is expanded.
//Set the play button.
//        val btnAlert = mFloatingView!!.findViewById(R.id.btnAlert) as ImageView
//        btnAlert.setOnClickListener{ Toast.makeText(this@FloatingViewService, "Sending alert", Toast.LENGTH_LONG).show() }


//Set the next button.
        val btnClose = mFloatingView!!.findViewById<ImageView>(R.id.btnClose) as ImageView
        btnClose.setOnClickListener{
            collapsedView?.visibility = View.VISIBLE
            expandedView?.visibility = View.GONE
            rssDetails?.visibility = View.GONE
            val imm = getSystemService(Service.INPUT_METHOD_SERVICE) as InputMethodManager?
            imm!!.hideSoftInputFromInputMethod(txtSearch?.windowToken, InputMethodManager.HIDE_IMPLICIT_ONLY)
        }

        val btnHome:ImageView = mFloatingView!!.findViewById(R.id.btnHome)
        btnHome.setOnClickListener {
            if (searchView!=null){
                when (searchView?.visibility){
                    View.VISIBLE -> searchView?.visibility = View.GONE
                }
            }
            val imm = getSystemService(Service.INPUT_METHOD_SERVICE) as InputMethodManager?
            imm!!.hideSoftInputFromInputMethod(txtSearch?.windowToken, InputMethodManager.HIDE_IMPLICIT_ONLY)
        }
        val btnSearch:ImageView = mFloatingView!!.findViewById(R.id.btnSearch)
        btnSearch.setOnClickListener {

            if (searchView!=null){
                when (searchView?.visibility){
                    View.VISIBLE -> searchView?.visibility = View.GONE
                    View.GONE -> searchView?.visibility = View.VISIBLE
                }
            }
            val imm = getSystemService(Service.INPUT_METHOD_SERVICE) as InputMethodManager?
            imm!!.hideSoftInputFromInputMethod(txtSearch?.windowToken, InputMethodManager.HIDE_IMPLICIT_ONLY)
        }
        val btnNotification:ImageView = mFloatingView!!.findViewById(R.id.btnNotification)
        btnNotification.setOnClickListener {
            val imm = getSystemService(Service.INPUT_METHOD_SERVICE) as InputMethodManager?
            imm!!.hideSoftInputFromInputMethod(txtSearch?.windowToken, InputMethodManager.HIDE_IMPLICIT_ONLY)
        }

        searchView = mFloatingView!!.findViewById(R.id.searchView)
        txtSearch = mFloatingView!!.findViewById(R.id.txtSearch)
        txtSearch?.onFocusChangeListener = object: View.OnFocusChangeListener{
            override fun onFocusChange(p0: View?, p1: Boolean) {
                Log.v("EditTextFocus","Focused")
                if (p1){
                    val imm = getSystemService(Service.INPUT_METHOD_SERVICE) as InputMethodManager?
                    imm!!.showSoftInputFromInputMethod(mFloatingView?.windowToken, InputMethodManager.SHOW_FORCED)
                }else{
                    val imm = getSystemService(Service.INPUT_METHOD_SERVICE) as InputMethodManager?
                    imm!!.hideSoftInputFromInputMethod(mFloatingView?.windowToken, 0)
                }
            }
        }
        txtSearch?.setOnEditorActionListener(object: TextView.OnEditorActionListener{
            override fun onEditorAction(p0: TextView?, p1: Int, p2: KeyEvent?): Boolean {
                if (p1 == EditorInfo.IME_ACTION_DONE) {
                    val imm = getSystemService(Service.INPUT_METHOD_SERVICE) as InputMethodManager?
                    imm!!.hideSoftInputFromInputMethod(mFloatingView?.windowToken, 0)
                    val keyword = p0?.text.toString()
                    if (!keyword.isNullOrEmpty()){
                        Log.v("DetailsStatus","Keyword: "+keyword)
                        fillAndResetRecycler(keyword)
                    }else{
                        fillAndResetRecycler(null)
                    }
                    return true
                }
                return false
            }
        })

        val imgPages: ImageView = mFloatingView!!.findViewById(R.id.imgPages)

        try{
//            Glide.with(this).load(R.mipmap.widget_home).into(btnHome)
//            Glide.with(this).load(R.mipmap.widget_search).into(btnSearch)
//            Glide.with(this).load(R.mipmap.widget_notification).into(btnNotification)
//            Glide.with(this).load(R.mipmap.widget_collapse).into(btnClose)
            Glide.with(this).load(R.mipmap.pages).into(imgPages)
        }catch (e:Exception){
            Log.e("LoadImageException",e.toString())
        }



//Open the application on thi button click
//        val openButton = mFloatingView!!.findViewById(R.id.open_button) as ImageView
//        openButton.setOnClickListener{
//            //Open the application  click.
//            val intent = Intent(this@FloatingViewService, MainActivity::class.java)
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//            startActivity(intent)
//
//
//            //close the service and remove view from the view hierarchy
//            stopSelf()
//        }

        //Drag and move floating view using user's touch action.
        mFloatingView!!.findViewById<RelativeLayout>(R.id.root_container).setOnTouchListener(object : View.OnTouchListener {
            private var initialX: Int = 0
            private var initialY: Int = 0
            private var initialTouchX: Float = 0.toFloat()
            private var initialTouchY: Float = 0.toFloat()

            override fun onTouch(v: View, event: MotionEvent): Boolean {
                when (event.action) {
                    MotionEvent.ACTION_UP -> {
                        var Xdiff: Int = (event.getRawX() - initialTouchX).toInt()
                        var Ydiff: Int = (event.getRawY() - initialTouchY).toInt()


                        //The check for Xdiff <10 && YDiff< 10 because sometime elements moves a little while clicking.
                        //So that is click event.
                        if (Xdiff < 10 && Ydiff < 10) {
                            if (isViewCollapsed()) {
                                //When user clicks on the image view of the collapsed layout,
                                //visibility of the collapsed layout will be changed to "View.GONE"
                                //and expanded view will become visible.
                                stopSelf()
//                                collapsedView?.setVisibility(View.GONE)
//                                expandedView?.setVisibility(View.VISIBLE)
                                val dialogIntent = Intent(this@FloatingViewService, Main2Activity::class.java)
                                dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                startActivity(dialogIntent)
                            }
                        }
                        return true
                    }
                    MotionEvent.ACTION_DOWN -> {


                        //remember the initial position.
                        initialX = params.x
                        initialY = params.y


                        //get the touch location
                        initialTouchX = event.rawX
                        initialTouchY = event.rawY
                        return true
                    }
                    MotionEvent.ACTION_MOVE -> {
                        //Calculate the X and Y coordinates of the view.
                        /*params.x = initialX + (event.rawX - initialTouchX).toInt()
                        params.y = initialY + (event.rawY - initialTouchY).toInt()


                        //Update the layout with new X & Y coordinate
                        mWindowManager!!.updateViewLayout(mFloatingView, params)*/
                        return true
                    }
                }
                return false
            }
        })
    }

    fun isDetailsOpen():Boolean{
        if (rssDetails!!.visibility == View.VISIBLE)
            return true
        return false
    }

    private fun fillAndResetRecycler(search:String?){
        Log.v("FeedIconList","Reseting Recycler")
        recycler?.adapter = null

        feedIconLst = if (search.isNullOrEmpty()){
            RssFeedDao().findLastFeedsByKeyword()
        }else{
            RssFeedDao().findLastFeedsBySearch(search!!)
        }

        adapter = FeedsAdapter(this, feedIconLst!!,-1)
        adapter?.setCustomItemClickListener(this)
        recycler?.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true)
        recycler?.adapter = adapter
        if (feedIconLst!!.isEmpty()){
            Log.v("FeedIconList","Size: Empty")
        }else{
            try{
                val size = feedIconLst!!.size - 1
                recycler?.smoothScrollToPosition(0)
                recycler?.smoothScrollToPosition(size)
                recycler?.smoothScrollToPosition(0)
                recycler?.smoothScrollToPosition(size)
            }catch (e:Exception){
                Log.e("RecyclerScroll",e.toString())
            }
            Log.v("FeedIconList","Size: "+feedIconLst?.size)
        }

    }

    override fun onItemClickListener(position: Int, feed: RssFeedModel) {
        if (currentPosition == position){
            if (isDetailsOpen()){
                Log.v("DetailsStatus","Closed")
//            btnOpenDetails!!.setImageResource(R.mipmap.collapse32)
                rssDetails!!.visibility = View.GONE
            }else{
                Log.v("DetailsStatus","Keyword: "+feed.keyword)
                feedLst = RssFeedDao().findUnviewedFeedsByKeyword(feed.keyword)
//                if (feedLst!=null && feedLst!!.size>0){
                    if (adapterCard==null)
                        fillAndResetCardStack()
                    adapterCard!!.setAdapterType(false)
                    adapterCard!!.clear()
                    adapterCard!!.addAll(feedLst)
                    adapterCard!!.notifyDataSetChanged()
                    cardStackView?.visibility = View.VISIBLE
                    cardStackView?.setCardEventListener(object: CardStackView.CardEventListener{
                        override fun onCardDragging(percentX: Float, percentY: Float) {}

                        override fun onCardSwiped(direction: SwipeDirection?) {
                            Log.v("CardSwiped",direction?.name)
                            feed.viewed = true
                            feed.type = direction?.name!!
                            val id = RssFeedDao().updateOrCreate(feed)
                            Log.v("CardSwiped","Updated id: "+id)
                        }

                        override fun onCardReversed() {}

                        override fun onCardMovedToOrigin() {}

                        override fun onCardClicked(index: Int) {
//
                        }

                    })
                    Log.v("StartCommand","Feed List is not null: "+feedLst!!.size)
//                }else{
//                    Log.e("StartCommand","Feed List is null")
//                }
                currentPosition = position
                Log.v("DetailsStatus","Opened")
//            btnOpenDetails!!.setImageResource(R.mipmap.expand32)
                rssDetails!!.visibility = View.VISIBLE
            }
        }else{
            currentPosition = position
            Log.v("DetailsStatus","Opened")
            Log.v("DetailsStatus","Keyword: "+feed.keyword)
            feedLst = RssFeedDao().findUnviewedFeedsByKeyword(feed.keyword)
            if (feedLst!=null && feedLst!!.isNotEmpty()){
                for (item in feedLst!!){
                    Log.v("CardSelected",item.toString())
                }
            }
//            if (feedLst!=null && feedLst!!.size>0){
                if (adapterCard==null)
                    fillAndResetCardStack()
                adapterCard!!.setAdapterType(false)
                adapterCard!!.clear()
                adapterCard!!.addAll(feedLst)
                adapterCard!!.notifyDataSetChanged()
                cardStackView?.visibility = View.VISIBLE
                cardStackView?.setCardEventListener(object: CardStackView.CardEventListener{
                    override fun onCardDragging(percentX: Float, percentY: Float) {}

                    override fun onCardSwiped(direction: SwipeDirection?) {
                        Log.v("CardSwiped",direction?.name)
                        feed.viewed = true
                        feed.type = direction?.name!!
                        val id = RssFeedDao().updateOrCreate(feed)
                        Log.v("CardSwiped","Updated id: "+id)
                    }

                    override fun onCardReversed() {}

                    override fun onCardMovedToOrigin() {}

                    override fun onCardClicked(index: Int) {
                    }

                })
                Log.v("StartCommand","Feed List is not null: "+feedLst!!.size)
//            }else{
//                Log.e("StartCommand","Feed List is null")
//            }
            rssDetails!!.visibility = View.VISIBLE
        }
    }

    override fun onShareButtonClickListener(img:Bitmap, position: Int, feed: RssFeedModel) {
        if (img!=null){
            val encoded = encodeToBase64(img,Bitmap.CompressFormat.PNG,100)
            Log.v("ImageBase64",encoded)
//            val url = saveImage(img)
//            val imgFile = File(url)
//            if (imgFile.exists()){
//                Log.v("ImageFile","Exist")
//            }
            showMessageDialog("Post feed to facebook?",encoded)
        }else{
            Log.v("ImageBase64","Image is null")
        }
    }

    fun encodeToBase64(image: Bitmap, compressFormat: Bitmap.CompressFormat, quality: Int): String {
        val byteArrayOS = ByteArrayOutputStream()
        image.compress(compressFormat, quality, byteArrayOS)
        return android.util.Base64.encodeToString(byteArrayOS.toByteArray(), android.util.Base64.DEFAULT)
    }

    fun saveImage(img:Bitmap): String {
        var fOut: OutputStream? = null
        var outputFileUri = ""
        try {
            val root = File(Environment.getExternalStorageDirectory().toString()
                    + File.separator + "TabsPreview" + File.separator)
            root.mkdirs()
            val sdImageMainDirectory = File(root, Date().time.toString())
            outputFileUri = sdImageMainDirectory.absolutePath
            fOut = FileOutputStream(sdImageMainDirectory)
        } catch (e: Exception) {
            Toast.makeText(this, "Error saving preview: "+e.toString(),
                    Toast.LENGTH_SHORT).show()
        }

        try {
            img!!.compress(Bitmap.CompressFormat.PNG, 100, fOut)
            fOut!!.flush()
            fOut.close()
        } catch (e: Exception) {
            Log.v("CreatePreview", "Exception imagen: " + e.toString())
        }

        Log.v("CreatePreview", "Image Path: " + outputFileUri)
        return outputFileUri
    }

    fun showProgressDialog(msg:String?){
        if (msg!=null){
            pDialog = SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
            pDialog?.getProgressHelper()?.setBarColor(R.color.drewpicColor)
            pDialog?.setTitleText(msg)
            pDialog?.setCancelable(false)
            pDialog?.show()
        }
    }

    fun showMessageDialog(msg:String?, imgUrl:String?){
        if (msg!=null){
            pDialog = SweetAlertDialog(this, SweetAlertDialog.NORMAL_TYPE)
            pDialog?.setTitleText(msg)
            pDialog?.window!!.setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT)
            pDialog?.setCancelable(false)
            pDialog?.confirmText = "Yes"
            pDialog?.cancelText = "No"
            val feed: RssFeedModel = RssFeedModel()
            pDialog?.setCancelClickListener { dismissDialog() }
            pDialog?.setConfirmClickListener {
                dismissDialog()
//                PostPhotoFacebookService(this).postPhoto(facebookToken!!,"Drewpic photo upload",imgUrl!!,feed,true)
//                collapsedView?.visibility = View.VISIBLE
//                expandedView?.visibility = View.GONE
//
//                val dialogIntent = Intent(this, PostActivity::class.java)
//                dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                dialogIntent.putExtra("ImgUrl",imgUrl)
//                startActivity(dialogIntent)
            }
            pDialog?.show()
        }
    }

    fun dismissDialog(){
        if (pDialog!=null && pDialog!!.isShowing){
            pDialog!!.dismiss()
        }
    }

    private fun fillAndResetCardStack(){
        adapterCard = RssFeedAdapter(this,null)
        adapterCard?.setShareButtonListener(this)
        cardStackView?.setAdapter(adapterCard)
        cardStackView?.visibility = View.VISIBLE
    }

    private fun isViewCollapsed(): Boolean {
        return mFloatingView == null || mFloatingView!!.findViewById<RelativeLayout>(R.id.collapse_view).visibility === View.VISIBLE
    }


    override fun onDestroy() {
        super.onDestroy()
        if (mFloatingView != null) mWindowManager!!.removeView(mFloatingView)
    }

}