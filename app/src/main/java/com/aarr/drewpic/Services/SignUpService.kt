package com.aarr.drewpic.Services

import android.content.Context
import android.util.Log
import com.aarr.drewpic.Services.Response.SignUp.SignUpResponse
import com.aarr.drewpic.Views.SignupActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import org.json.JSONException
import org.json.JSONObject
import okhttp3.OkHttpClient
import okhttp3.HttpUrl





/**
 * Created by andresrodriguez on 9/16/17.
 */
class SignUpService{

    private var context: Context? = null
    private var service:ServiceAPI? = null
    private var activity: SignupActivity? = null

    constructor(context: Context){
        this.context = context
        if (context is SignupActivity){
            activity = context as SignupActivity
        }
    }

    fun SignUp(email:String){

        val retrofit = Retrofit.Builder()
                .baseUrl("http://www.drewpic.com/api/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        Log.v("Service","Url:"+retrofit.baseUrl().toString())
        service = retrofit.create(ServiceAPI::class.java)

        try {
            var response: Call<SignUpResponse>? = service!!.SignUpEmail(email)
            response!!.enqueue(object : Callback<SignUpResponse> {
                override fun onResponse(call: Call<SignUpResponse>?, response: retrofit2.Response<SignUpResponse>?) {
                    Log.v("Service","Success")
                    Log.v("Service","Data:"+response!!.body().toString())
                    activity!!.SignUpSuccessResponse(response!!.body())
                }
                override fun onFailure(call: Call<SignUpResponse>?, t: Throwable?) {
                    Log.v("Service","Failed:"+t.toString())
                }
            })

        } catch (e: JSONException) {
            Log.v("Service","Exception:"+e.toString())
        }
    }
}