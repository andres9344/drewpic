package com.aarr.drewpic.Services.Response.RSS

import android.os.Parcel
import android.os.Parcelable
import java.io.Serializable

/**
 * Created by andresrodriguez on 12/9/17.
 */
data class RssItemModel(
        var categoryName: String,
        var title: String,
        var link: String,
        var image: String,
        var publishDate: String,
        var thumbnailImg: String,
        var description: String
):Serializable,Parcelable{

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    constructor():this(
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(categoryName)
        parcel.writeString(title)
        parcel.writeString(link)
        parcel.writeString(image)
        parcel.writeString(publishDate)
        parcel.writeString(thumbnailImg)
        parcel.writeString(description)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RssItemModel> {
        override fun createFromParcel(parcel: Parcel): RssItemModel {
            return RssItemModel(parcel)
        }

        override fun newArray(size: Int): Array<RssItemModel?> {
            return arrayOfNulls(size)
        }
    }
}