package com.aarr.drewpic.Services

import android.content.Context
import android.util.Log
import com.aarr.drewpic.Services.Response.GetBingImages.GetBingImagesResponse
import com.aarr.drewpic.Services.Response.GetBingNews.GetBingNews
import com.aarr.drewpic.Services.Response.GetImages.GetImagesResponse
import com.aarr.drewpic.Services.Response.RSS.RssItemModel
import com.aarr.drewpic.Views.ProfileActivity
import com.aarr.drewpic.Views.SignupActivity
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*

/**
 * Created by andresrodriguez on 11/12/17.
 */
class GetBingNewsService {

    private var context: Context? = null
    private var service:ServiceAPI? = null
    private var activity: ProfileActivity? = null
    private val GETTY_API_KEY = "8bf823e50dca4cc18973578e659edc0a"
    private val BASE_URL = "https://api.cognitive.microsoft.com/bing/v7.0/news/"
    private val SEARCH_URL = "search?q="

    constructor(context: Context){
        this.context = context
        if (context is ProfileActivity){
            activity = context as ProfileActivity
        }
    }

    fun getRSS(keyword:String){

        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(object : Interceptor {
            override fun intercept(chain: Interceptor.Chain?): Response {
                val original = chain!!.request()

                // Request customization: add request headers
                val requestBuilder = original.newBuilder()
                        .header("Ocp-Apim-Subscription-Key", GETTY_API_KEY) // <-- this is the important line

                val request = requestBuilder.build()
                return chain!!.proceed(request)
            }

        })

        val client = httpClient.build()

        val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        Log.v("Service","Url:"+retrofit.baseUrl().toString())
        service = retrofit.create(ServiceAPI::class.java)

        try {
            var response: Call<GetBingNews>? = service!!.GetBingNewsByKeyword(SEARCH_URL+keyword.replace(" ","+"))
            response!!.enqueue(object : Callback<GetBingNews> {
                override fun onResponse(call: Call<GetBingNews>?, response: retrofit2.Response<GetBingNews>?) {
                    if (response!=null){
                        try{
                            if (response.body().value!=null && response.body().value.isNotEmpty()){
                                Log.v("RssValue","Keyword: "+keyword)
                                Log.v("RssValue","Size: "+response.body().value.size)
                                var item = response.body().value
                                Log.v("RssValue",item.toString())
                                if (activity!=null){
                                    Log.v("FloatingService","Adding new element")
//                                    activity?.loadRssFeed(item,keyword)
                                }else{
                                    Log.e("FloatingService","Activity is null")
                                }
                            }else{
                                if (activity!=null){
                                    Log.v("FloatingService","Adding new element")
                                    activity?.loadRssFeed(emptyList(),keyword)
                                }else{
                                    Log.e("FloatingService","Activity is null")
                                }
                                Log.e("RssException","No rss found with keyword: "+keyword)
                            }
                        }catch (e:Exception){
                            if (activity!=null){
                                Log.v("FloatingService","Adding new element")
                                activity?.loadRssFeed(emptyList(),keyword)
                            }else{
                                Log.e("FloatingService","Activity is null")
                            }
                            Log.e("RssException","Exception: "+e.toString())
                        }
                    }
                }
                override fun onFailure(call: Call<GetBingNews>?, t: Throwable?) {
                    if (activity!=null){
                        Log.v("FloatingService","Adding new element")
                        activity?.loadRssFeed(emptyList(),keyword)
                    }else{
                        Log.e("FloatingService","Activity is null")
                    }
                    Log.e("RssException","Failure: "+t.toString())
                }
            })

        } catch (e: JSONException) {
            Log.v("Service","Exception:"+e.toString())
        }
    }

    fun getRandomNumber(max:Int):Int{
        val r = Random()
        val i1 = r.nextInt(max.toInt() - 1) + 1
        return i1
    }
}