package com.aarr.drewpic.Services.Asyncs

import android.content.Context
import android.os.AsyncTask
import android.util.Log
import com.aarr.drewpic.Database.Dao.RssFeedDao
import com.aarr.drewpic.Database.Dao.UserDao
import com.aarr.drewpic.Database.Model.RssFeedModel
import com.aarr.drewpic.Util.DateManage
import com.aarr.drewpic.Views.Main2Activity
import me.toptas.rssconverter.RssItem

/**
 * Created by andresrodriguez on 2/28/18.
 */
class AsyncLoadFeed(items: List<RssItem>?, keyword: String, context: Context): AsyncTask<String, Void, String>() {

    private var activity: Main2Activity?=null
    private var rssItems: List<RssItem>?=null
    private var keywordValue: String? = null

    init {
        if (context!=null){
            activity = context as Main2Activity
        }
        rssItems = items
        keywordValue = keyword
    }

    override fun doInBackground(vararg p0: String?): String {
        if (activity!=null){
            loadRssFeed(rssItems,keywordValue!!)
        }
        return ""
    }

    override fun onPostExecute(result: String?) {
        super.onPostExecute(result)
        Log.v("RequestPosition","Likes: "+activity?.currentPosition+", Requests: "+activity?.currentRequestPosition)
        activity?.checkFeedCounter()
    }

    fun loadRssFeed(items: List<RssItem>?, keyword: String){
        activity!!.currentRequestPosition++
        if (items!=null && items.isNotEmpty()){
            val user = UserDao().findCurrentUser()
            for (item in items){
                Log.v("AddItem","Adding: "+keyword)
                Log.v("AddItemValue","Value: "+item.toString())
                val parsedTitle = item.title.replace("'","&#39;")
                val parsedDescription = checkTitleContent(parsedTitle,item.description)
                var feed : RssFeedModel? = RssFeedDao().findRssFeedModelByTitleAndSynced(parsedTitle)
                if (feed==null && !parsedDescription.isNullOrEmpty()){
                    var actualImg = ""
                    val time = DateManage().parseDateFeedToMillis(item.publishDate)
                    if (item.image!=null && time>0){
                        val timeOld = DateManage().parseMillistoTimeInt(time)
                        if (!activity!!.apiCallSearch){

                            if (timeOld<=48){
                                actualImg = item.image

                                var description = parsedDescription?.replace("...","")
//                                var lastDot = item.description.lastIndexOf(".",0,false)
//                                Log.v("ItemDescription","Description: "+description)
//                                Log.v("ItemDescription","Length: "+description.length)
//                                Log.v("ItemDescription","Date: "+item.publishDate)
//                    Log.v("ItemDescription","Last dot: "+lastDot)

//                    Log.v("ItemDescription","Last dot Index: "+description.get(lastDot-1))
//
//                    Log.v("ItemDescription","TimeMillis: "+DateManage().parseDateFeedToMillis(item.publishDate))

                                var rssIconItem = RssFeedModel(
                                        -1,
                                        -1,
                                        keyword,
                                        parsedTitle,
                                        item.link,
                                        actualImg,
                                        time,
                                        actualImg,
                                        parsedDescription!!,
                                        false,
                                        "",
                                        "",
                                        0,
                                        0,
                                        false,
                                        user!!.idFacebook!!,
                                        false,
                                        false)

                                Log.v("ItemDescription","InsertingData: "+rssIconItem.toString())

                                RssFeedDao().updateOrCreate(rssIconItem)
                            }else{
                                Log.e("ItemDescription","Feed to Old: "+timeOld+" hours ago")
                            }
                        }else{
                            actualImg = item.image

                            var description = parsedDescription?.replace("...","")
//                            var lastDot = item.description.lastIndexOf(".",0,false)
//                            Log.v("ItemDescription","Description: "+description)
//                            Log.v("ItemDescription","Length: "+description.length)
//                            Log.v("ItemDescription","Date: "+item.publishDate)
//                    Log.v("ItemDescription","Last dot: "+lastDot)

//                    Log.v("ItemDescription","Last dot Index: "+description.get(lastDot-1))
//
//                    Log.v("ItemDescription","TimeMillis: "+DateManage().parseDateFeedToMillis(item.publishDate))

                            var rssIconItem = RssFeedModel(
                                    -1,
                                    -1,
                                    keyword,
                                    parsedTitle,
                                    item.link,
                                    actualImg,
                                    time,
                                    actualImg,
                                    parsedDescription!!,
                                    false,
                                    "",
                                    "",
                                    0,
                                    0,
                                    false,
                                    user!!.idFacebook!!,
                                    false,
                                    false)

                            Log.v("ItemDescription","InsertingData: "+rssIconItem.toString())

                            RssFeedDao().updateOrCreate(rssIconItem)
                        }
                    }else{
                        Log.v("AddItem","Item Image is null")
                    }
                }else{
                    Log.v("AddItem","Feed Already Exists")
                    Log.v("AddItem","Value: "+feed.toString())
                }
            }
        }
    }

    fun checkTitleContent(title:String,description:String):String?{
        if (description.toLowerCase().contains(title.toLowerCase())){
            Log.v("CheckDescription","Description contains title")
            var finalDescription = removeHtmlTags(description)
//            var finalDescription = description
            Log.v("CheckDescription","Title: "+title+", Description: "+finalDescription)
//            finalDescription = finalDescription.toLowerCase().replace(title.toLowerCase(),"")
            Log.v("CheckDescription","Final Description: "+finalDescription)
            if (finalDescription.length>=50){
                Log.v("CheckDescription","Description length > 5")
                return finalDescription
            }
        }else{
            Log.v("CheckDescription","Description dont contains title")
        }
        return null
    }

    fun removeHtmlTags(text: String): String {
        val tagsRegex = Regex("<[^>]*>")
        val strongTagRegex = Regex("<strong>(.+?)<\\/strong>")
        val fontTagRegex = Regex("<font.*?>([^<]*?)<\\/font>")
        var parsedText = text
                .replace(strongTagRegex,"")
                .replace(fontTagRegex,"")
                .replace(tagsRegex,"")
                .replace("&nbsp;"," ")
                .replace("Full coverage","")
        parsedText = parsedText.replace("'","&#39;")
        Log.v("RssItemValue",parsedText.toString())
        return parsedText
    }
}