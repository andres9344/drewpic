package com.aarr.drewpic.Services

import com.aarr.drewpic.Services.Response.Categories.CategoriesResponse
import com.aarr.drewpic.Services.Response.GetBingImages.GetBingImagesResponse
import com.aarr.drewpic.Services.Response.GetBingNews.GetBingNews
import com.aarr.drewpic.Services.Response.GetFriendsPosts.GetFriendsPosts
import com.aarr.drewpic.Services.Response.GetFriendsPosts.GetPostsHistory
import com.aarr.drewpic.Services.Response.GetImages.GetImagesResponse
import com.aarr.drewpic.Services.Response.PostPhoto.PostPhotoResponse
import com.aarr.drewpic.Services.Response.QwantResponse.News.GetQwantNewsResponse
import com.aarr.drewpic.Services.Response.RegisterCategories.RegisterCategoriesResponse
import com.aarr.drewpic.Services.Response.SearchAutocomplete.SearchAutocomplete
import com.aarr.drewpic.Services.Response.SignUp.SignUpResponse
import retrofit2.Call
import retrofit2.http.*
import retrofit2.http.Url
import me.toptas.rssconverter.RssFeed
import retrofit2.http.GET



/**
 * Created by andresrodriguez on 9/16/17.
 */
interface ServiceAPI {

    @FormUrlEncoded
    @POST("register-user.php")
    fun SignUpEmail(@Field("email") email: String): Call<SignUpResponse>

    @POST("get-all-categories.php")
    fun GetAllCategories(): Call<CategoriesResponse>

    @FormUrlEncoded
    @POST("register-categories.php")
    fun RegisterCategories(@Field("email") email: String, @Field("cat") cat: String): Call<RegisterCategoriesResponse>

    @GET
    fun GetImagesByKeyword(@Url url: String): Call<GetImagesResponse>

    @GET
    fun GetBingImagesByKeyword(@Url url: String): Call<GetBingImagesResponse>

    @GET
    fun GetBingNewsByKeyword(@Url url: String): Call<GetBingNews>

    @GET
    fun getRss(@Url url: String): Call<RssFeed>

    @GET
    fun getQwantNews(@Url url: String): Call<GetQwantNewsResponse>

    @FormUrlEncoded
    @POST("facebook/photo.php")
    fun postPhotoFacebook(
            @Field("token") token: String,
            @Field("img") img: String,
            @Field("message") message: String,
            @Field("keyword") keyword: String,
            @Field("title") title: String,
            @Field("link") link: String,
            @Field("image") image: String,
            @Field("publish_date") publishDate: Long,
            @Field("thumbnail") thumbnail: String,
            @Field("description") description: String,
            @Field("viewed") viewed: Int,
            @Field("type") type: String,
            @Field("facebook_id") facebookId: String,
            @Field("caption") caption: String,
            @Field("id_server") idServer: Int,
            @Field("status") status: String
    ) : Call<PostPhotoResponse>

    @FormUrlEncoded
    @POST("apis/savePost.php")
    fun savePost(
            @Field("keyword") keyword: String,
            @Field("title") title: String,
            @Field("link") link: String,
            @Field("image") image: String,
            @Field("publish_date") publishDate: Long,
            @Field("thumbnail") thumbnail: String,
            @Field("description") description: String,
            @Field("viewed") viewed: Int,
            @Field("type") type: String,
            @Field("facebook_id") facebookId: String,
            @Field("caption") caption: String,
            @Field("id_server") idServer: Int,
            @Field("status") status: String
    ) : Call<PostPhotoResponse>

    @FormUrlEncoded
    @POST("apis/saveReply.php")
    fun saveReply(
            @Field("type") type: String,
            @Field("facebook_id") facebookId: String,
            @Field("caption") caption: String,
            @Field("id_server") idServer: Int,
            @Field("status") status: String
    ) : Call<PostPhotoResponse>

    @FormUrlEncoded
    @POST("apis/getPosts.php")
    fun getFriendsPosts(
            @Field("facebook_id") facebookId: String
    ) : Call<GetFriendsPosts>

    @FormUrlEncoded
    @POST("apis/getHistory.php")
    fun getPostsHistory(
            @Field("facebook_id") facebookId: String
    ) : Call<GetPostsHistory>

    @FormUrlEncoded
    @POST("apis/autocomplete.php")
    fun getAutocomplete(
            @Field("searchTerm") searchTerm: String
    ) : Call<SearchAutocomplete>
}