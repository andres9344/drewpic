package com.aarr.drewpic.Services.Response.SearchAutocomplete

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by andresrodriguez on 3/21/18.
 */
data class SearchAutocomplete(

        @SerializedName("error")
        @Expose
        var error:Boolean,
        @SerializedName("error_msg")
        @Expose
        var errorMsg:String,
        @SerializedName("keyword")
        @Expose
        var keyword:String,
        @SerializedName("results")
        @Expose
        var results: List<String>?

):Serializable{
    constructor():this(false,"","", emptyList())
}