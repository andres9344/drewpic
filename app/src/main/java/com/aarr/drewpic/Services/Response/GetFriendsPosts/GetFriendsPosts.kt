package com.aarr.drewpic.Services.Response.GetFriendsPosts

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by andresrodriguez on 2/28/18.
 */
data class GetFriendsPosts(

        @SerializedName("error")
        @Expose
        var error:String,
@SerializedName("error_msg")
@Expose
var errorMsg:String,
@SerializedName("posts")
@Expose
var posts:List<Posts>
):Serializable{
    constructor():this("","", emptyList())
}