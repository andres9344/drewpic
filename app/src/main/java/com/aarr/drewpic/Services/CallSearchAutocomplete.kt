package com.aarr.drewpic.Services

import android.content.Context
import android.util.Log
import com.aarr.drewpic.Services.Response.Categories.CategoriesResponse
import com.aarr.drewpic.Services.Response.SearchAutocomplete.SearchAutocomplete
import com.aarr.drewpic.Views.Main2Activity
import com.aarr.drewpic.Views.SignupActivity
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by andresrodriguez on 3/21/18.
 */
class CallSearchAutocomplete{
    private var service:ServiceAPI? = null
    private var activity: Main2Activity? = null
    private var context: Context? = null

    constructor(context: Context){
        this.context = context
        if (context is Main2Activity){
            activity = context as Main2Activity
        }
    }

    fun getAutocomplete(search:String){
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(logging)

        val client = httpClient.build()

        Log.v("Autocomplete","Keyword: "+search)
        val retrofit = Retrofit.Builder()
                .baseUrl("http://www.drewpic.net/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()

        service = retrofit.create(ServiceAPI::class.java)

        try {

            var response: Call<SearchAutocomplete>? = service!!.getAutocomplete(search)

            response!!.enqueue(object : Callback<SearchAutocomplete> {
                override fun onResponse(call: Call<SearchAutocomplete>?, response: retrofit2.Response<SearchAutocomplete>?) {
                    Log.v("Autocomplete","Success")
                    val resp = response!!.body()
                    try{
                        Log.v("Autocomplete","Data:"+resp.toString())
                    }catch (e:Exception){
                        Log.v("Autocomplete","Exception: "+e.toString())
                    }
                    if (!resp.error)
                        activity!!.resetAutocomplete(response.body().results!!)
                    else
                        Log.e("Autocomplete","Response is error")

                }
                override fun onFailure(call: Call<SearchAutocomplete>?, t: Throwable?) {
                    Log.v("Autocomplete","Failed:"+t.toString())
                }
            })

        } catch (e: JSONException) {
            Log.v("Autocomplete","Exception:"+e.toString())
        }
    }
}