package com.aarr.drewpic.Services.Asyncs

import android.content.Context
import android.os.AsyncTask
import android.util.Log
import com.aarr.drewpic.Database.Dao.RssFeedDao
import com.aarr.drewpic.Database.Dao.UserDao
import com.aarr.drewpic.Database.Model.RssFeedModel
import com.aarr.drewpic.Services.Response.GetFriendsPosts.Posts
import com.aarr.drewpic.Util.DateManage
import com.aarr.drewpic.Views.Main2Activity

/**
 * Created by andresrodriguez on 2/28/18.
 */
class AsyncLoadFriendFeed(items: List<Posts>?, context: Context, friends:Boolean): AsyncTask<String, Void, String>() {

    private var activity: Main2Activity?=null
    private var rssItems: List<Posts>?=null
    private var isFriends=false

    init {
        if (context!=null){
            activity = context as Main2Activity
        }
        rssItems = items
        isFriends = friends
    }

    override fun doInBackground(vararg p0: String?): String {
        if (activity!=null){
            loadRssFeed(rssItems)
        }
        return ""
    }

    override fun onPostExecute(result: String?) {
        super.onPostExecute(result)
        Log.v("RequestPosition","Likes: "+activity?.currentPosition+", Requests: "+activity?.currentRequestPosition)
        if (isFriends)
            activity?.requestFriendsPosts()
    }

    fun loadRssFeed(items: List<Posts>?){
        activity!!.currentRequestPosition++
        if (items!=null && items.isNotEmpty()){
            for (item in items){
                Log.v("FriendsItem","Value: "+item.toString())
                val myId = UserDao().findCurrentUser()!!.idFacebook
                var idUser = item.idUser
                var isViewed = false
                if (!isFriends){
                    isViewed = true
                    idUser = myId!!
                    Log.v("FriendsItem","Is My Post")
                }else{
                    Log.v("FriendsItem","Is Friend's Post")
                }

                var updatedFeed = RssFeedDao().findRssFeedModelByServerIdOrTitle(item.id,item.title,idUser)

                if (updatedFeed==null){
                    val time = DateManage().getMillisFromServerTime(item.createdAt)
                    val timeOld = DateManage().parseMillistoTimeInt(time)
                    if (timeOld<=48) {

                        updatedFeed = RssFeedModel(
                                -1,
                                item.id,
                                item.keyword,
                                item.title,
                                item.link,
                                item.image,
                                time,
                                item.thumbnail,
                                item.description,
                                isViewed,
                                "",
                                item.caption,
                                0,
                                0,
                                isFriends,
                                idUser,
                                false,
                                false)
                        RssFeedDao().updateOrCreate(updatedFeed)
                        Log.e("FriendsItem","Inserted: "+updatedFeed)

                    }else{
                        Log.e("FriendsItem","Friend Feed to Old: "+timeOld+" hours ago")
                    }
                }
            }
        }
    }

    fun removeHtmlTags(text: String): String {
        val tagsRegex = Regex("<[^>]*>")
        var parsedText = text
                .replace(tagsRegex,"")
                .replace("&nbsp;"," ")
                .replace("Full coverage","")
        parsedText = parsedText.replace("'","&#39;")
        Log.v("RssItemValue",parsedText.toString())
        return parsedText
    }
}