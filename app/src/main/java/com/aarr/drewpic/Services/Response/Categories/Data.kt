package com.aarr.drewpic.Services.Response.Categories

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


/**
 * Created by andresrodriguez on 9/16/17.
 */

data class Data(
        @SerializedName("cat")
        @Expose
        var cat: String? = null,
        @SerializedName("image")
        @Expose
        var image: String? = null,
        @SerializedName("sub_cat")
        @Expose
        var subCat: List<String>? = null
):Serializable{
    constructor() : this("","", emptyList())
}
