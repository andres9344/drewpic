package com.aarr.drewpic.Services.Response.RegisterCategories

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


/**
 * Created by andresrodriguez on 9/16/17.
 */
data class Data(
        @SerializedName("email")
        @Expose
        var email: String? = null
):Serializable
{
constructor() : this("")

}