package com.aarr.drewpic.Services.Response.GetFriendsPosts

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by andresrodriguez on 2/28/18.
 */
data class Replies(
        @SerializedName("id")
        @Expose
        var id:Int,
        @SerializedName("caption")
        @Expose
        var caption:String,
        @SerializedName("friends_facebook_id")
        @Expose
        var friendFacebookId:String,
        @SerializedName("post_id")
        @Expose
        var postServerId:Int,
        @SerializedName("created_at")
        @Expose
        var createdAt:String,
        @SerializedName("type")
        @Expose
        var type:String,
        @SerializedName("status")
        @Expose
        var status:String
):Serializable{
    constructor():this(
            -1,
            "",
            "",
            -1,
            "",
            "",
            "")
}