package com.aarr.drewpic.Services

import android.content.Context
import android.util.Log
import com.aarr.drewpic.Services.Response.GetImages.GetImagesResponse
import com.aarr.drewpic.Views.SignupActivity
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by andresrodriguez on 11/12/17.
 */
class GetImagesService{

    private var context: Context? = null
    private var service:ServiceAPI? = null
    private var activity: SignupActivity? = null
    private val GETTY_API_KEY = "bnvn3vzssqhsv2erdu8j2gsn"

    constructor(context: Context){
        this.context = context
        if (context is SignupActivity){
            activity = context as SignupActivity
        }
    }

    fun getImages(keyword:String){

        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(object : Interceptor {
            override fun intercept(chain: Interceptor.Chain?): Response {
                val original = chain!!.request()

                // Request customization: add request headers
                val requestBuilder = original.newBuilder()
                        .header("Api-Key", GETTY_API_KEY) // <-- this is the important line

                val request = requestBuilder.build()
                return chain!!.proceed(request)
            }

        })

        val client = httpClient.build()

        val retrofit = Retrofit.Builder()
                .baseUrl("https://api.gettyimages.com/v3/search/images/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        Log.v("Service","Url:"+retrofit.baseUrl().toString())
        service = retrofit.create(ServiceAPI::class.java)

        try {
            var response: Call<GetImagesResponse>? = service!!.GetImagesByKeyword("creative?phrase="+keyword)
            response!!.enqueue(object : Callback<GetImagesResponse> {
                override fun onResponse(call: Call<GetImagesResponse>?, response: retrofit2.Response<GetImagesResponse>?) {
                    Log.v("Service","Success")
                    Log.v("Service","Data:"+response!!.body().toString())
                }
                override fun onFailure(call: Call<GetImagesResponse>?, t: Throwable?) {
                    Log.v("Service","Failed:"+t.toString())
                }
            })

        } catch (e: JSONException) {
            Log.v("Service","Exception:"+e.toString())
        }
    }
}