package com.aarr.drewpic.Services.Response.TweetsNews

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by andresrodriguez on 2/16/18.
 */
data class Tweet(
        @SerializedName("time")
        @Expose
        var time:String?=null,
        @SerializedName("text")
        @Expose
        var description:String?=null,
        @SerializedName("image")
        @Expose
        var image:String?=null
):Serializable{
    constructor():this(
            "",
            "",
            ""
    )
}