package com.aarr.drewpic.Services.Response.GetImages;

/**
 * Created by andresrodriguez on 10/5/17.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DisplaySize {

    @SerializedName("is_watermarked")
    @Expose
    private Boolean isWatermarked;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("uri")
    @Expose
    private String uri;

    public Boolean getIsWatermarked() {
        return isWatermarked;
    }

    public void setIsWatermarked(Boolean isWatermarked) {
        this.isWatermarked = isWatermarked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    @Override
    public String toString() {
        return "DisplaySize{" +
                "isWatermarked=" + isWatermarked +
                ", name='" + name + '\'' +
                ", uri='" + uri + '\'' +
                '}';
    }
}