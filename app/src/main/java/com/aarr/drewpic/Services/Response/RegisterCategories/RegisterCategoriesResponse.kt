package com.aarr.drewpic.Services.Response.RegisterCategories

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


/**
 * Created by andresrodriguez on 9/16/17.
 */
data class RegisterCategoriesResponse(
        @SerializedName("status")
        @Expose
        var status: String? = null,
        @SerializedName("data")
        @Expose
        var data: Data? = null,
        @SerializedName("msg")
        @Expose
        var msg: String? = null
):Serializable{

constructor() : this("",
                        null,
                        "")


}