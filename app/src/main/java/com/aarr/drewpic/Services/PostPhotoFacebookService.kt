package com.aarr.drewpic.Services

import android.content.Context
import android.util.Log
import com.aarr.drewpic.Database.Dao.RssFeedDao
import com.aarr.drewpic.Database.Dao.UserDao
import com.aarr.drewpic.Database.Model.RssFeedModel
import com.aarr.drewpic.Services.Response.GetBingNews.GetBingNews
import com.aarr.drewpic.Services.Response.PostPhoto.PostPhotoResponse
import com.aarr.drewpic.Util.JsonInterceptor
import com.aarr.drewpic.Views.ProfileActivity
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory



/**
 * Created by andresrodriguez on 2/18/18.
 */
class PostPhotoFacebookService{

    var context: Context?=null
    var service: ServiceAPI?=null

    constructor(context: Context){
        this.context = context
    }

    fun postPhoto(token:String,msg:String,imgString:String,feed:RssFeedModel,postToFacebook:Boolean,status:String){

        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(JsonInterceptor())

        val retrofit = Retrofit.Builder()
                .baseUrl("http://www.drewpic.net/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build()
        Log.v("Service","Url:"+retrofit.baseUrl().toString())
        service = retrofit.create(ServiceAPI::class.java)

        val user = UserDao().findCurrentUser()

        var response: Call<PostPhotoResponse>? = service!!.postPhotoFacebook(token,imgString,msg,feed.keyword,feed.title,feed.link,feed.image,feed.publishDate,feed.thumbnailImg,feed.description,feed.viewed.toInt(),feed.type,user?.idFacebook!!,feed.caption,feed.idServer,status)
        try{
            response!!.enqueue(object : Callback<PostPhotoResponse> {
                override fun onResponse(call: Call<PostPhotoResponse>?, response: retrofit2.Response<PostPhotoResponse>?) {
                    try{
                        val resp = response!!.body()
                        Log.v("PostResponse","Data: "+resp.toString())
                        if (!feed.isFriends){
                            if (resp.idInsert!!>0){
                                var updatedFeed = RssFeedDao().findRssFeedModelByMobileId(feed.idMobile)
                                updatedFeed?.idServer = resp.idInsert!!
                                RssFeedDao().updateOrCreate(updatedFeed)
                            }
                        }
                    }catch (e:Exception){
                        Log.e("PostResponse","Exception: "+e.toString())
                    }
                }

                override fun onFailure(call: Call<PostPhotoResponse>?, t: Throwable?) {
                    Log.e("PostResponse","Failure: "+t.toString())
                }
            })
        }catch(e:Exception){
            Log.e("PostResponse","Exception: "+e.toString())
        }
    }

    fun Boolean.toInt() = if (this) 1 else 0
}