package com.aarr.drewpic.Services

import android.content.Context
import android.util.Log
import com.aarr.drewpic.Services.Response.Categories.CategoriesResponse
import com.aarr.drewpic.Services.Response.SignUp.SignUpResponse
import com.aarr.drewpic.Views.SignupActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import org.json.JSONException
import org.json.JSONObject



/**
 * Created by andresrodriguez on 9/16/17.
 */
class GetAllCategoriesService{

    private var service:ServiceAPI? = null
    private var activity: SignupActivity? = null
    private var context: Context? = null

    constructor(context: Context){
        this.context = context
        if (context is SignupActivity){
            activity = context as SignupActivity
        }
    }

    fun getCategories(){
        val retrofit = Retrofit.Builder()
                .baseUrl("http://www.drewpic.com/api/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        service = retrofit.create(ServiceAPI::class.java)

        try {

            var response: Call<CategoriesResponse>? = service!!.GetAllCategories()

            response!!.enqueue(object : Callback<CategoriesResponse> {
                override fun onResponse(call: Call<CategoriesResponse>?, response: retrofit2.Response<CategoriesResponse>?) {
                    Log.v("Service","Success")
                    Log.v("Service","Data:"+response!!.body().toString())
                    activity!!.CategoriesSuccessResponse(response!!.body(),false)
                }
                override fun onFailure(call: Call<CategoriesResponse>?, t: Throwable?) {
                    Log.v("Service","Failed:"+t.toString())
                }
            })

        } catch (e: JSONException) {
            Log.v("Service","Exception:"+e.toString())
        }
    }
}