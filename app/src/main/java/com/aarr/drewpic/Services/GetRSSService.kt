package com.aarr.drewpic.Services

import android.content.Context
import android.util.Log
import com.aarr.drewpic.Database.Dao.SettingsDao
import com.aarr.drewpic.Views.Main2Activity
import com.aarr.drewpic.Views.ProfileActivity
import com.aarr.drewpic.Views.SignupActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import me.toptas.rssconverter.RssFeed
import me.toptas.rssconverter.RssConverterFactory



/**
 * Created by andresrodriguez on 11/12/17.
 */
class GetRSSService {

    private var context: Context? = null
    private var service: ServiceAPI? = null
    private var activity: ProfileActivity? = null
    private var activityMain: Main2Activity? = null
    private var floatingService: FloatingViewService? = null

    constructor(context: Context){
        this.context = context
        if (context is ProfileActivity){
            activity = context as ProfileActivity
        }else if (context is FloatingViewService)
            this.floatingService = context as FloatingViewService
        else if (context is Main2Activity)
            activityMain = context as Main2Activity
    }

    fun getKeywordWithLanguage(keyword:String):String?{
        val settings = SettingsDao().getSettings()
        when(settings?.language){
            "English"->return keyword+" news"
            "Español"->return keyword+" noticias"
            "Italiano"->return keyword+" notizie"
            "Français"->return keyword+" informations"
            "Deutsche"->return keyword+" Nachrichten"
            "Português"->return keyword+" notícias"
            "Română"->return keyword+" știri"
        }
        return null
    }

    fun getRSS(keyword:String):Call<RssFeed>?{

        val retrofit = Retrofit.Builder()
                .baseUrl("https://github.com")
                .addConverterFactory(RssConverterFactory.create())
                .build()

        val service = retrofit.create<ServiceAPI>(ServiceAPI::class.java)
//        service.getRss("https://news.google.com/news?q="+keyword+"&output=rss")
        if (keyword!=null && !keyword.equals("")){
            val searchKeyword = getKeywordWithLanguage(keyword)
            Log.v("RssService","Searching: "+searchKeyword)
            val apiCall = service.getRss("https://news.google.com/news/rss/search/section/q/"+searchKeyword+"/"+searchKeyword+"?gl=419&ned=us&hl=en&lt;/font&gt;&lt;/b&gt;&lt;/font&gt;&lt;br&gt;&lt;font size=&quot;-1&quot;&gt;")

            apiCall.enqueue(object : Callback<RssFeed> {
                override fun onResponse(call: Call<RssFeed>?, response: retrofit2.Response<RssFeed>?) {
                    if (response!=null){
                        try{
                            if (response.body().items!=null && response.body().items.isNotEmpty()){
                                Log.v("RssValue","Keyword: "+keyword)
                                Log.v("RssValue","Size: "+response.body().items.size)
                                var item = response.body().items
                                Log.v("RssValue",item.toString())
                                if (activityMain!=null){
                                    Log.v("FloatingService","Adding new element")
//                                            activityMain?.loadRssFeed(item,keyword)
                                    activityMain?.startAsyncLoadFeed(item,keyword,context!!)
                                }else{
                                    Log.e("FloatingService","Activity is null")
                                }
                            }else{
                                if (activityMain!=null){
                                    Log.v("FloatingService","Adding new element")
//                                            activityMain?.loadRssFeed(emptyList(),keyword)
                                    activityMain?.startAsyncLoadFeed(emptyList(),keyword,context!!)
                                }else{
                                    Log.e("FloatingService","Activity is null")
                                }
                                Log.e("RssException","No rss found with keyword: "+keyword)
                            }
                        }catch (e:Exception){
                            if (activityMain!=null){
                                Log.v("FloatingService","Adding new element")
//                                        activityMain?.loadRssFeed(emptyList(),keyword)
                                activityMain?.startAsyncLoadFeed(emptyList(),keyword,context!!)
                            }else{
                                Log.e("FloatingService","Activity is null")
                            }
                            Log.e("RssException","Exception: "+e.toString())
                        }
                    }
                }
                override fun onFailure(call: Call<RssFeed>, t: Throwable) {
                    if (activityMain!=null){
                        Log.v("FloatingService","Adding new element")
//                                activityMain?.loadRssFeed(emptyList(),keyword)
                        activityMain?.startAsyncLoadFeed(emptyList(),keyword,context!!)
                    }else{
                        Log.e("FloatingService","Activity is null")
                    }
                    Log.e("RssException","Failure: "+t.toString())
                }
            })
            return apiCall
        }
        return null
    }
}