package com.aarr.drewpic.Services

import android.content.Context
import android.util.Log
import com.aarr.drewpic.Database.Model.RssFeedModel
import com.aarr.drewpic.Services.Response.GetBingImages.GetBingImagesResponse
import com.aarr.drewpic.Services.Response.GetBingImages.Value
import com.aarr.drewpic.Services.Response.GetImages.GetImagesResponse
import com.aarr.drewpic.Services.Response.RSS.RssItemModel
import com.aarr.drewpic.Views.ProfileActivity
import com.aarr.drewpic.Views.SignupActivity
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*

/**
 * Created by andresrodriguez on 11/12/17.
 */
class GetBingImagesService {

    private var context: Context? = null
    private var service:ServiceAPI? = null
    private var activity: ProfileActivity? = null
    private val GETTY_API_KEY = "8bf823e50dca4cc18973578e659edc0a"
    private val BASE_URL = "https://api.cognitive.microsoft.com/bing/v7.0/images/"
    private val SEARCH_URL = "search?q="

    constructor(context: Context){
        this.context = context
        if (context is ProfileActivity){
            activity = context as ProfileActivity
        }
    }

    fun getImages(item: RssFeedModel, keyword:String){

        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)

        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(object : Interceptor {
            override fun intercept(chain: Interceptor.Chain?): Response {
                val original = chain!!.request()

                // Request customization: add request headers
                val requestBuilder = original.newBuilder()
                        .header("Ocp-Apim-Subscription-Key", GETTY_API_KEY) // <-- this is the important line

                val request = requestBuilder.build()

                return chain!!.proceed(request)
            }

        })
        httpClient.addInterceptor(logging)

        val client = httpClient.build()

        val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        Log.v("Service","Url:"+retrofit.baseUrl().toString())
        service = retrofit.create(ServiceAPI::class.java)

        try {
            var response: Call<GetBingImagesResponse>? = service!!.GetBingImagesByKeyword(SEARCH_URL+keyword.replace(" ","+"))
            response!!.enqueue(object : Callback<GetBingImagesResponse> {
                override fun onResponse(call: Call<GetBingImagesResponse>?, response: retrofit2.Response<GetBingImagesResponse>?) {
                    Log.v("BingService","Keyword: "+keyword)
//                    Log.v("BingService","Success, Matches: "+response!!.body().totalEstimatedMatches)
                    if (response!=null && response!!.body()!=null){
                        if (response!!.body().totalEstimatedMatches!=null && response!!.body().totalEstimatedMatches>0){
                            val imagesValues: MutableList<Value> = response!!.body().value
                            val image = imagesValues[0]
                            item.thumbnailImg = image.thumbnailUrl
                            item.image = image.contentUrl
                            Log.v("BingService","Thumbnail: "+image.thumbnailUrl)
                            Log.v("BingService","Image: "+image.contentUrl)
                            if (activity!=null)
                                activity?.bingImageResult(item)
                        }else{
                            Log.v("BingService","NO RESULTS")
                            if (activity!=null)
                                activity?.bingImageResult(null)
                        }
                    }else{
                        if (activity!=null)
                            activity?.bingImageResult(null)
                    }
                }
                override fun onFailure(call: Call<GetBingImagesResponse>?, t: Throwable?) {
                    if (activity!=null)
                        activity?.bingImageResult(null)
                    Log.v("Service","Failed:"+t.toString())
                }
            })

        } catch (e: JSONException) {
            Log.v("Service","Exception:"+e.toString())
        }
    }

    fun getRandomNumber(max:Int):Int{
        val r = Random()
        val i1 = r.nextInt(max.toInt() - 1) + 1
        return i1
    }
}