package com.aarr.drewpic.Services.Asyncs

import android.content.Context
import android.os.AsyncTask
import android.util.Log
import com.aarr.drewpic.Database.Dao.FeedRepliesDao
import com.aarr.drewpic.Database.Dao.RssFeedDao
import com.aarr.drewpic.Database.Model.FeedReplies
import com.aarr.drewpic.Database.Model.RssFeedModel
import com.aarr.drewpic.Services.Response.GetFriendsPosts.Posts
import com.aarr.drewpic.Services.Response.GetFriendsPosts.Replies
import com.aarr.drewpic.Util.DateManage
import com.aarr.drewpic.Views.Main2Activity

/**
 * Created by andresrodriguez on 2/28/18.
 */
class AsyncLoadFriendReplies(items: List<Replies>?, context: Context): AsyncTask<String, Void, String>() {

    private var activity: Main2Activity?=null
    private var rssItems: List<Replies>?=null

    init {
        if (context!=null){
            activity = context as Main2Activity
        }
        rssItems = items
    }

    override fun doInBackground(vararg p0: String?): String {
        if (activity!=null){
            loadRssFeed(rssItems)
        }
        return ""
    }

    override fun onPostExecute(result: String?) {
        super.onPostExecute(result)
        Log.v("RequestPosition","Likes: "+activity?.currentPosition+", Requests: "+activity?.currentRequestPosition)
        activity?.fillAndResetNotificationRecycler(false,"Like")
    }

    fun loadRssFeed(items: List<Replies>?){
        activity!!.currentRequestPosition++
        if (items!=null && items.isNotEmpty()){
            for (item in items){
                Log.v("FriendsItem","Value: "+item.toString())
                var updatedFeed = FeedRepliesDao().findReplyByServerId(item.id)
                if (updatedFeed==null){
                    val time = DateManage().getMillisFromServerTime(item.createdAt)
                    val timeOld = DateManage().parseMillistoTimeInt(time)
                    updatedFeed = FeedReplies(
                            -1,
                            item.id,
                            item.caption,
                            item.friendFacebookId,
                            item.postServerId,
                            time,
                            item.type,
                            item.status)
                    FeedRepliesDao().updateOrCreate(updatedFeed)
                    Log.e("FriendsItem","Inserted: "+updatedFeed)
                }
            }
        }
    }

    fun removeHtmlTags(text: String): String {
        val tagsRegex = Regex("<[^>]*>")
        var parsedText = text
                .replace(tagsRegex,"")
                .replace("&nbsp;"," ")
                .replace("Full coverage","")
        parsedText = parsedText.replace("'","&#39;")
        Log.v("RssItemValue",parsedText.toString())
        return parsedText
    }
}