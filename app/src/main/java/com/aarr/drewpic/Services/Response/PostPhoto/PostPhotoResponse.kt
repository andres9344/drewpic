package com.aarr.drewpic.Services.Response.PostPhoto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by andresrodriguez on 2/18/18.
 */
data class PostPhotoResponse(
        @SerializedName("error")
        @Expose
        var error: String? = null,
        @SerializedName("error_msg")
        @Expose
        var errorMsg: String? = null,
        @SerializedName("id_insert")
        @Expose
        var idInsert: Int? = null
):Serializable{
    constructor():this("","",-1)
}