package com.aarr.drewpic.Services.Response.GetFriendsPosts

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by andresrodriguez on 2/28/18.
 */
data class GetPostsHistory(

        @SerializedName("error")
        @Expose
        var error:String,
        @SerializedName("error_msg")
        @Expose
        var errorMsg:String,
        @SerializedName("replies")
        @Expose
        var replies:List<Replies>
):Serializable{
    constructor():this("","", emptyList())
}