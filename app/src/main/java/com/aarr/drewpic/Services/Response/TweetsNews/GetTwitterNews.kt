package com.aarr.drewpic.Services.Response.TweetsNews

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by andresrodriguez on 2/16/18.
 */
data class GetTwitterNews(
        @SerializedName("tweets")
        @Expose
        var tweets:List<Tweet>?=null,
        @SerializedName("relatedKeywords")
        @Expose
        var relatedKeywords:List<String>?=null
):Serializable{
    constructor():this(
            emptyList(),
            emptyList()
    )

}