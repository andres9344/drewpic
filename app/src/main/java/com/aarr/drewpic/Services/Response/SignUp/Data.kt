package com.aarr.drewpic.Services.Response.SignUp

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


/**
 * Created by andresrodriguez on 9/16/17.
 */
data class Data(
        @SerializedName("user_id")
        @Expose
        var userId: Int? = null,
        @SerializedName("email")
        @Expose
        var email: String? = null,
        @SerializedName("sub_category")
        @Expose
        var subCategory: String? = null
):Serializable
{
constructor() : this(-1,
                        "",
                        "")

}