package com.aarr.drewpic.Services.Response.GetFriendsPosts

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by andresrodriguez on 2/28/18.
 */
data class Posts(
        @SerializedName("id")
        @Expose
        var id:Int,
        @SerializedName("keyword")
        @Expose
        var keyword:String,
        @SerializedName("title")
        @Expose
        var title:String,
        @SerializedName("link")
        @Expose
        var link:String,
        @SerializedName("image")
        @Expose
        var image:String,
        @SerializedName("publish_date")
        @Expose
        var publishDate:Long,
        @SerializedName("thumbnail")
        @Expose
        var thumbnail:String,
        @SerializedName("description")
        @Expose
        var description:String,
        @SerializedName("viewed")
        @Expose
        var viewed:Int,
        @SerializedName("type")
        @Expose
        var type:String,
        @SerializedName("caption")
        @Expose
        var caption:String,
        @SerializedName("post_image")
        @Expose
        var postImage:String,
        @SerializedName("id_user")
        @Expose
        var idUser:String,
        @SerializedName("created_at")
        @Expose
        var createdAt:String
):Serializable{
    constructor():this(
            -1,
            "",
            "",
            "",
            "",
            0,
            "",
            "",
            0,
            "",
            "",
            "",
            "",
            "")
}