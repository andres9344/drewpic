package com.aarr.drewpic

import android.app.Application
import android.content.Context

/**
 * Created by andresrodriguez on 12/9/17.
 */
class MainApplication : Application() {

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
//        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        MainApplication.appContext = applicationContext
    }

    companion object {
        var appContext: Context? = null
            private set
    }

}